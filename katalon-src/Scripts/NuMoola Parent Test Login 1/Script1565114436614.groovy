import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.app_path, false)

Mobile.waitForElementPresent(findTestObject('Login/XCUIElementTypeImage - Logo'), 0)

Mobile.verifyElementExist(findTestObject('Login/XCUIElementTypeTextField - username'), 0)

Mobile.verifyElementExist(findTestObject('Login/XCUIElementTypeSecureTextField - password'), 0)

Mobile.verifyElementExist(findTestObject('Login/XCUIElementTypeButton - SignIn'), 0)

Mobile.verifyElementExist(findTestObject('Login/XCUIElementTypeStaticText - reset'), 0)

Mobile.verifyElementExist(findTestObject('Login/XCUIElementTypeButton - SignUp'), 0)

Mobile.verifyElementExist(findTestObject('Login/XCUIElementTypeStaticText - privacy'), 0)

Mobile.tap(findTestObject('Login/XCUIElementTypeTextField - username'), 0)

Mobile.setText(findTestObject('Login/XCUIElementTypeTextField - username'), GlobalVariable.parent_login, 0)

Mobile.tap(findTestObject('Login/XCUIElementTypeSecureTextField - password'), 0)

Mobile.setText(findTestObject('Login/XCUIElementTypeSecureTextField - password'), GlobalVariable.parent_password, 0)

Mobile.tap(findTestObject('Login/XCUIElementTypeButton - SignIn'), 0)

Mobile.waitForElementPresent(findTestObject('Parent Welcome/Welcome/XCUIElementTypeButton - Skip'), 6)

Mobile.tap(findTestObject('Parent Welcome/Welcome/XCUIElementTypeButton - Skip'), 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Parent Home/Household/XCUIElementTypeButton - Settings'), 0)

Mobile.tap(findTestObject('Parent Home/Household/XCUIElementTypeButton - Settings'), 0)

Mobile.tap(findTestObject('Parent Settings/XCUIElementTypeButton - Sign Out'), 0)

Mobile.closeApplication()

