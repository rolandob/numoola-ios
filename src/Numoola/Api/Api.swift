//
//  Api.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/24/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import AWSCore
import AWSMobileClient
import AWSCognitoIdentityProvider
import SwiftyJSON
import CoreData
import UIKit
import RxSwift

public class RequestOptions {
    let fetchOptions: String
    let select: String
    let filter: String
    let orderBy: String
    let skip: Int
    let top: Int
    let distinct: Bool
    let count: Bool
    let profiler: Bool
    let tenant:String
    
    init(fetch fetchOptions: String = "", select selectOptions: String = "", filter filterOptions: String = "", orderBy orderByOptions: String = "", skip skipOptions:Int = 0, top topOptions:Int = 0, distinct distinctOptions:Bool = false,
         count countOptions: Bool = false, profiler profilerOptions: Bool = false, tenant tenantId: String = "") {
        self.fetchOptions = fetchOptions
        self.select = selectOptions
        self.filter = filterOptions
        self.orderBy = orderByOptions
        self.skip = skipOptions
        self.top = topOptions
        self.distinct = distinctOptions
        self.count = countOptions
        self.profiler = profilerOptions
        self.tenant = tenantId
    }
}

public class Api {
    
    static var _instance: Api = Api()
    
    private var schema: String = ""
    private var host: String = ""
    private var numoolaVersion: String = ""
    private var cbeVersion: String = ""
    
    static var shared: AppDelegate? {
        if Thread.isMainThread {
            return UIApplication.shared.delegate as? AppDelegate
        }
        
        var appDelegate: AppDelegate?
        DispatchQueue.main.sync {
            appDelegate = UIApplication.shared.delegate as? AppDelegate
        }
        return appDelegate
    }
    
    init() {
        if let settings = NumoolaSettings.get() {
            if let jsonResult = settings as? Dictionary<String, AnyObject> {
                // do stuff
                let baseUrl = (jsonResult["BaseUrl"] as? Dictionary<String, String>)!
                schema = baseUrl["schema"]!
                host = baseUrl["host"]!
                numoolaVersion = (jsonResult["NumoolaVersion"] as? String)!
                cbeVersion = (jsonResult["CbeVersion"] as? String)!
            }
        }
    }
    
    private func encodeQueryValue(with value: String) -> String {
        var encoded = value.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        encoded = encoded?.replacingOccurrences(of: "/", with: "%2F")
        encoded = encoded?.replacingOccurrences(of: "'", with: "%27")
        encoded = encoded?.replacingOccurrences(of: "(", with: "%28")
        encoded = encoded?.replacingOccurrences(of: ")", with: "%29")
        encoded = encoded?.replacingOccurrences(of: ")", with: "%29")
        encoded = encoded?.replacingOccurrences(of: ",", with: "%2C")
        encoded = encoded?.replacingOccurrences(of: "$", with: "%24")
        return encoded!
    }
    
    private func getUrl(to path: String, with options: RequestOptions? = nil, using api: String, withVersion version: String, useEncode encode: Bool = false, apiVersionSeparator separator: String = "/") -> URL! {
        let urlComponents = NSURLComponents()
        urlComponents.scheme = schema;
        urlComponents.host = host;
        
        if path.starts(with: "/") {
            urlComponents.path = "/\(api)\(separator)\(version)\(path)";
        } else {
            urlComponents.path = "/\(api)\(separator)\(version)/\(path)";
        }
        if let options = options {
            var queryItems = [URLQueryItem]()
            var percentEncodedQueryItems = [URLQueryItem]()
            if options.count {
                queryItems.append(URLQueryItem(name: "count", value: "true"))
            }
            if options.distinct {
                queryItems.append(URLQueryItem(name: "distinct", value: "true"))
            }
            if !options.orderBy.isEmpty {
                percentEncodedQueryItems.append(URLQueryItem(name: "orderby", value: encodeQueryValue(with: options.orderBy)))
            }
            if !options.select.isEmpty {
                percentEncodedQueryItems.append(URLQueryItem(name: "select", value: encodeQueryValue(with: options.select)))
            }
            if !options.filter.isEmpty {
                percentEncodedQueryItems.append(URLQueryItem(name: "filter", value: encodeQueryValue(with: options.filter)))
            }
            if options.skip != 0 {
                queryItems.append(URLQueryItem(name: "skip", value: String(options.skip)))
            }
            if options.top != 0 {
                queryItems.append(URLQueryItem(name: "top", value: String(options.top)))
            }
            if queryItems.count > 0 {
                urlComponents.queryItems = queryItems
            }
            if percentEncodedQueryItems.count > 0 {
                urlComponents.percentEncodedQueryItems = percentEncodedQueryItems
            }
        }
        return urlComponents.url
    }
    
    private func getUrl(to path: String) -> URL! {
        let urlComponents = NSURLComponents()
        urlComponents.scheme = schema;
        urlComponents.host = host;
        
        if path.starts(with: "/") {
            urlComponents.path = "\(path)";
        } else {
            urlComponents.path = "/\(path)";
        }
        return urlComponents.url
    }
    
    public func downloadPhoto(for entity: JSON?, withField photo: String, completionHandler handler: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        if entity![photo].exists() {
            let key = entity![photo]["$modifiedOn"].exists() ? "\(entity![photo]["id"].string!)\(entity![photo]["$modifiedOn"].string!)" : "\(entity![photo]["id"].string!)"
            if !key.isEmpty {
                let request = UserPhoto.fetchRequest() as NSFetchRequest<UserPhoto>
                request.predicate = NSPredicate(format: "key == %@", key)
                request.sortDescriptors = []
                do {
                    let context = Api.shared!.persistentContainer.viewContext
                    let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                    privateMOC.parent = context
                    
                    let results = NSFetchedResultsController(fetchRequest: request, managedObjectContext: privateMOC, sectionNameKeyPath: nil, cacheName: nil)
                    try results.performFetch()
                    if results.fetchedObjects!.count > 1 {
                        // get user photo
                        let userPhoto = results.fetchedObjects!.first
                        // create data from base64 string
                        let data = userPhoto!.content!.data(using: .utf8)
                        // call handler
                        handler(data, nil)
                    } else {
                        downloadPhoto(to: "/entity-3.0/entity/\(entity!["$class"].string!)/\(entity!["id"].string!)/\(photo)/url", completionHandler: {
                            data, error in
                            if let data = data {
                                let userPhoto = UserPhoto(entity: UserPhoto.entity(), insertInto: privateMOC)
                                userPhoto.key = key
                                userPhoto.content = String(data: data, encoding: .utf8)
                                do {
                                    try privateMOC.save()
                                    context.performAndWait {
                                        do {
                                            try context.save()
                                            // save it
                                            handler(data, error)
                                        } catch {
                                            handler(nil, nil)
                                        }
                                    }
                                } catch {
                                    handler(nil, nil)
                                }
                            }
                        })
                    }
                    
                } catch let error as NSError {
                    handler(nil, error)
                }
            } else {
                downloadPhoto(to: "/entity-3.0/entity/\(entity!["$class"].string!)/\(entity!["id"].string!)/\(photo)/url", completionHandler: { data, error in
                    if let data = data {
                        let context = Api.shared!.persistentContainer.viewContext
                        let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                        privateMOC.parent = context
                        let userPhoto = UserPhoto(entity: UserPhoto.entity(), insertInto: privateMOC)
                        userPhoto.key = "\(entity![photo]["id"].string!)"
                        userPhoto.content = String(data: data, encoding: .utf8)
                        do {
                            try privateMOC.save()
                            context.performAndWait {
                                do {
                                    try context.save()
                                    // save it
                                    handler(data, error)
                                } catch {
                                    handler(nil, nil)
                                }
                            }
                        } catch {
                            handler(nil, nil)
                        }
                    }
                })
            }
        } else {
            handler(nil, nil)
        }
    }
    
    public func downloadPhoto(to path: String, completionHandler handler: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        // generate url
        let url = getUrl(to: path)!
        // get credentials provider
        let credentialsProvider = AWSMobileClient.sharedInstance().getCredentialsProvider() as AWSCognitoCredentialsProvider
        let serviceInfo = AWSInfo.default().defaultServiceInfo("CognitoUserPool")
        // create endpoint
        let endpoint = AWSEndpoint(region: serviceInfo!.region, service: .APIGateway, url: url)
        // init signer
        let signer = AWSSignatureV4Signer(credentialsProvider: credentialsProvider, endpoint: endpoint)
        // url request
        let request = NSMutableURLRequest(url: url)
        // generate date header
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "yyyyMMdd'T'HHmmss'Z'"
        request.setValue(formatter.string(from: date), forHTTPHeaderField: "X-Amz-Date")
        // set method
        request.httpMethod = "Get"
        signer?.interceptRequest(request)?.continueOnSuccessWith(block: {
            _ in
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: request as URLRequest) {
                (data, response, error) in
                if let res = response as? HTTPURLResponse {
                    if res.statusCode == 200 {
                        if let dataObject = data {
                            do {
                                let json = try JSON(data: dataObject)
                                self.downloadPhotoContent(to: json.array![0]["url"].string!, completionHandler: handler)
                            }
                            catch let err as NSError{
                                handler(nil, err)
                            }
                        } else if let error = error {
                            handler(nil, error)
                        }
                    } else {
                        handler(nil, NSError(domain: "", code: res.statusCode, userInfo: ["message": "An error occurred"]))
                    }
                }
            }
            task.resume()
            return nil
        })
    }
    
    private func downloadPhotoContent(to url: String, completionHandler handler: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        let urlObject = URL(string: url)
        // url request
        let request = NSMutableURLRequest(url: urlObject!)
        // set method
        request.httpMethod = "Get"
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            if let res = response as? HTTPURLResponse {
                if res.statusCode == 200 {
                    if let dataObject = data {
                        handler(dataObject, nil)
                    } else if let error = error {
                        handler(nil, error)
                    }
                } else {
                    handler(nil, NSError(domain: "", code: res.statusCode, userInfo: ["message": "An error occurred"]))
                }
            }
        }
        task.resume()
    }
    
    public func rxUploadFile(content uploadData: Data?, type contentType: String) -> Observable<Result<JSON?, Error>> {
        return self.requestUploadUrl().concatMap({ [self] result -> Observable<JSON?> in
            switch result {
            case .success(let data):
                if let info = data  {
                    return self.uploadContent(using: info, with: uploadData, type: contentType)
                }
            case .failure(let error as NSError):
                print(error.localizedDescription)
            }
            return Observable.just(nil)
        }).concatMap({ [self] info -> Observable<Result<JSON?, Error>> in
            guard let info = info else {
                return Observable.just(.success(nil))
            }
            do {
                return try self.rxInvoke(using: "Post", to: "document/upload/content", payload: info.rawData())
            } catch(let error as NSError) {
                print(error.localizedDescription)
                return Observable.just(.success(nil))
            }
        })
    }
    
    public func requestUploadUrl() -> Observable<Result<JSON?,Error>> {
        return self.rxInvoke(using: "Get", to: "document/upload/url")
    }
    
    public func uploadContent(using info: JSON, with uploadData: Data?, type contentType: String) -> Observable<JSON?> {
        return Observable.create({ observer in
            
            let url = URL(string: info["url"].stringValue)!
            var request = URLRequest(url: url)
            request.httpMethod = info["method"].stringValue
            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            request.setValue("attachment; filename=\(NSUUID().uuidString)", forHTTPHeaderField: "Content-Disposition")
            
            let uploadTask = URLSession.shared.uploadTask(with: request, from: uploadData) { data, response, error in
                if let error = error {
                    print ("error: \(error)")
                    observer.on(.next(nil))
                    observer.on(.completed)
                    return
                }
                
                guard let response = response as? HTTPURLResponse,
                    (200...299).contains(response.statusCode) else {
                        print ("server error")
                        observer.on(.next(nil))
                        observer.on(.completed)
                        return
                }
                observer.on(.next(info))
                observer.on(.completed)
            }
            uploadTask.resume()
            
            return Disposables.create()
        })
    }
    
    public func rxInvoke(using method: String, to path: String, payload body: Data? = nil, api apiValue: String = "numoola", with options: RequestOptions? = nil) -> Observable<Result<JSON?, Error>> {
        return rxInvoke(using: method, to: path, payload: body, api: apiValue, with: options, withVersion: numoolaVersion)
    }
    
    public func rxInvokeCbe(using method: String, to path: String, payload body: Data? = nil, api apiValue: String, with options: RequestOptions? = nil) -> Observable<Result<JSON?, Error>> {
        return rxInvoke(using: method, to: path, payload: body, api: apiValue, with: options, withVersion: cbeVersion)
    }
    
    public func rxInvokeCbeEntity(using method: String, to path: String, payload body: Data? = nil, with options: RequestOptions? = nil) -> Observable<Result<JSON?, Error>> {
        return rxInvoke(using: method, to: path, payload: body, api: "entity", with: options, withVersion: "3.0", apiVersionSeparator: "-")
    }
    
    private func rxInvoke(using method: String, to path: String, payload body: Data? = nil, api apiValue: String = "numoola", with options: RequestOptions? = nil, withVersion version: String, apiVersionSeparator separator: String = "/") -> Observable<Result<JSON?, Error>> {
        return Observable.create({[weak self] observer in
            // generate url
            let url = self!.getUrl(to: path, with: options, using: apiValue, withVersion: version, apiVersionSeparator: separator)!
            // get credentials provider
            let credentialsProvider = AWSMobileClient.sharedInstance().getCredentialsProvider() as AWSCognitoCredentialsProvider
            let serviceInfo = AWSInfo.default().defaultServiceInfo("CognitoUserPool")
            // create endpoint
            let endpoint = AWSEndpoint(region: serviceInfo!.region, service: .APIGateway, url: url)
            // init signer
            let signer = AWSSignatureV4Signer(credentialsProvider: credentialsProvider, endpoint: endpoint)
            // url request
            let request = NSMutableURLRequest(url: url)
            // generate date header
            let date = Date()
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.dateFormat = "yyyyMMdd'T'HHmmss'Z'"
            request.setValue(formatter.string(from: date), forHTTPHeaderField: "X-Amz-Date")
            // set method
            request.httpMethod = method
            // set body
            if let payload = body {
                request.httpBody = payload
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("\(payload.count)", forHTTPHeaderField: "Content-Length")
            }
            if let options = options {
                if !options.fetchOptions.isEmpty {
                    request.setValue(options.fetchOptions, forHTTPHeaderField: "X-Fetch-Options")
                }
                if !options.tenant.isEmpty {
                    request.setValue(options.tenant, forHTTPHeaderField: "X-Tenant-Id")
                }
                if options.profiler == true {
                    request.setValue("true", forHTTPHeaderField: "X-Profiler")
                }
            }
            
            signer?.interceptRequest(request)?.continueOnSuccessWith(block: {
                _ in
                let session = URLSession(configuration: .default)
                request.url = self!.getUrl(to: path, with: options, using: apiValue, withVersion: version, useEncode: true, apiVersionSeparator: separator)!
                let task = session.dataTask(with: request as URLRequest) {
                    (data, response, error) in
                    if let res = response as? HTTPURLResponse {
                        do {
                            var jsonData: JSON!
                            if let data = data {
                                jsonData = try JSON(data: data)
                            }
                            if res.statusCode == 200 || res.statusCode == 204 {
                                if let jsonData = jsonData {
                                    observer.on(.next(.success(jsonData)))
                                    observer.on(.completed)
                                } else if let error = error {
                                    observer.on(.next(.failure(error)))
                                    observer.on(.completed)
                                }
                            } else {
                                if let jsonData = jsonData {
                                    observer.on(.next(.failure(NSError(domain: "", code: res.statusCode, userInfo: ["message": jsonData["message"].string ?? "An error occurred"]))))
                                    observer.on(.completed)
                                } else {
                                    observer.on(.next(.failure(NSError(domain: "", code: res.statusCode, userInfo: ["message": "An error occurred"]))))
                                    observer.on(.completed)
                                }
                            }
                        }
                        catch let err as SwiftyJSONError {
                            observer.on(.next(.failure(err)))
                            observer.on(.completed)
                        }
                        catch {
                            observer.on(.next(.failure(error)))
                            observer.on(.completed)
                        }
                    }
                }
                task.resume()
                return nil
            })
            
            return Disposables.create()
        })
    }
    
    public func registerLoginEvent() -> Observable<Result<JSON?, Error>> {
        return Observable.create({ [self] observer in
            
            var disposable:Disposable? = nil
            AWSMobileClient.sharedInstance().getTokens({ tokens, error in
                if let tokens = tokens {
                    
                    let serviceInfo = AWSInfo.default().defaultServiceInfo("CognitoUserPool")
                    let appClientId = serviceInfo?.infoDictionary["AppClientId"] as? String
                    
                    var payload = JSON(parseJSON: "{}")
                    payload["jwt"].string = tokens.idToken!.tokenString
                    payload["clientId"].string = appClientId
                    do {
                        disposable = try self.rxInvokeCbe(using: "Post", to: "userLoginEvent", payload: payload.rawData(), api: "iam").subscribe(onNext: { result in
                            switch result {
                            case .success(let data):
                                observer.on(.next(.success(data)))
                                observer.on(.completed)
                            case .failure(let error):
                                observer.on(.next(.failure(error)))
                                observer.on(.completed)
                            }
                        })
                    } catch let error as NSError {
                        observer.on(.error(error))
                        observer.on(.completed)
                    }
                } else {
                    observer.on(.error(error!))
                    observer.on(.completed)
                }
            })
            return disposable ?? Disposables.create()
        })
    }
    
    public func loginUser(_ username: String, _ password: String, _ fetchOptions: String) -> Observable<Result<JSON?, Error>> {
        if(AWSMobileClient.sharedInstance().isSignedIn){
            print("Signing out user \(AWSMobileClient.sharedInstance().username!)")
            AWSMobileClient.sharedInstance().signOut()
        }
        return Observable.create({ [self] observer in
            var disposable:Disposable? = nil
            
            AWSMobileClient.sharedInstance().signIn(username: username, password: password, completionHandler: {
                (result, error) in
                if let error = error {
                    observer.on(.next(.failure(error)))
                    observer.on(.completed)
                } else {
                    
                    AWSMobileClient.sharedInstance().getUserAttributes { (attributes, error) in
                        if error == nil {
                            let verified = attributes?.first(where: { (item) -> Bool in
                                return item.key == "phone_number_verified"
                            })?.value
                            DataHub.instance().publishData(for: "userPhoneVerified", with: JSON(dictionaryLiteral: ("verified", verified == "true" ? true : false)))
                        }
                    }
                    
                    disposable = self.registerLoginEvent().map({ loginEventResult -> Bool in
                        switch loginEventResult {
                        case .success(let loginEventResultData):
                            if let json = loginEventResultData {
                                return json["provisioned"].boolValue
                            }
                            return false
                        case .failure:
                            return false
                        }
                    }).flatMap({ [self] provisioned -> Observable<Result<JSON?, Error>> in
                        if provisioned == true {
                            return Api.instance().rxInvoke(using: "Get", to: "userAccount/self", with: RequestOptions(fetch: fetchOptions))
                        } else {
                            return self.checkUserProvisioned(fetchOptions)
                        }
                    }).subscribe(onNext: { dataResult in
                        observer.on(.next(dataResult))
                        observer.on(.completed)
                    })
                }
            })
            
            return disposable ?? Disposables.create()
        })
    }
    
    public func checkUserProvisioned(_ fetchOptions: String) -> Observable<Result<JSON?, Error>> {
        print("Checking user is provisioned: Trying to get user information...")
        return Api.instance().rxInvoke(using: "Get", to: "userAccount/self", with: RequestOptions(fetch: fetchOptions))
            .flatMap({ result -> Observable<Result<JSON?, Error>> in
                switch result {
                case .success:
                    return Observable<Result<JSON?, Error>>.just(result)
                case .failure:
                    print("Retrying in 500 milliseconds")
                    return Observable.just(true).delay(RxTimeInterval.milliseconds(500), scheduler: ConcurrentMainScheduler.instance).flatMap({ _ in
                        return self.checkUserProvisioned(fetchOptions)
                    })
                }
            })
    }
    
    public class func instance() -> Api {
        return _instance
    }
}
