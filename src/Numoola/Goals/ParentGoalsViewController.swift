//
//  ParentGoalsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/17/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class ParentGoalsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var kidsView: UIView!
    @IBOutlet var goalsTableView: UITableView!
    @IBOutlet var filterGoalsButton: NMLBarButtonItem!
    @IBOutlet var noChildStackView: UIStackView!
    @IBOutlet var summaryView: UIView!
    weak var titleView: PageTitleView!
    //fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let kid:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let goals:BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    let filters:BehaviorRelay<FilterOptions?> = BehaviorRelay(value: FilterOptions(
        [
            FilterOptionGroup(
                in: 0,
                identifier: "status",
                label: "Status",
                items: [
                    FilterOption(identifier: "Any", label: "Any", selected: true),
                    FilterOption(identifier: "Progress", label: "In Progress"),
                    FilterOption(identifier: "ToApprove", label: "Pending Approval"),
                    FilterOption(identifier: "Declined", label: "Declined"),
                    FilterOption(identifier: "Ready", label: "Ready to Complete"),
                    FilterOption(identifier: "Purchased", label: "Purchased")
                ]
            ),
            FilterOptionGroup(
                in: 1,
                identifier: "type",
                label: "Type",
                items: [
                    FilterOption(identifier: "Any", label: "Any", selected: true),
                    FilterOption(identifier: "Spending", label: "Spending Goal"),
                    FilterOption(identifier: "Saving", label: "Saving Goal"),
                    FilterOption(identifier: "Charity", label: "Charity Goal")
                ]
            )
        ]))
    var avatarListView: AvatarListView!
}

extension ParentGoalsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.goalsTableView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:  #selector(getGoalsForCurrentUser), for: .valueChanged)
        self.goalsTableView.refreshControl = refreshControl
        
        parentSubject
            .subscribe(onNext:{ parent in
                guard let parent = parent else { return }
                if self.titleView != nil {
                    self.titleView.removeFromSuperview()
                }
                self.titleView = UINib(nibName: "PageTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PageTitleView
                self.titleView.frame.size.width = self.summaryView.frame.size.width
                self.titleView.frame.size.height = self.summaryView.frame.size.height
                self.summaryView.addSubview(self.titleView)
                
                if self.avatarListView != nil {
                    self.avatarListView.removeFromSuperview()
                }
                
                self.avatarListView = UINib(nibName: "AvatarListView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? AvatarListView
                self.avatarListView.frame.size.width = self.kidsView.frame.size.width
                self.avatarListView.frame.size.height = self.kidsView.frame.size.height
                
                self.avatarListView.handleAvatarTap = self.handleAvatarTap
                self.avatarListView.kids = parent["kids"]
                if self.kid.value != nil {
                    self.avatarListView.selectedKid = self.kid.value
                }
                self.kidsView.addSubview(self.avatarListView)
            }).disposed(by: disposeBag)
        
        kid.subscribe(onNext: { [self] kid in
            guard let kid = kid else { return }
            self.getGoals(for: kid)
            self.titleView.user.accept(kid)
            self.titleView.title.accept("\(kid["firstName"].stringValue)'s Goals")
        }).disposed(by: disposeBag)
        
        filters.subscribe(onNext: { [self] filters in
            guard let _ = filters, let kid = self.kid.value else { return }
            self.getGoals(for: kid)
        }).disposed(by: disposeBag)
        
        // bind goals to table view
        goals.bind(to: goalsTableView.rx.items(cellIdentifier: "GoalItem")) { [self] (row, goal, cell) in
            if let cell = cell as? GoalTableViewCell {
                cell.goalView.goal.accept(goal)
                self.getGoalBalance(for: goal, at: cell)
            }
            }.disposed(by: disposeBag)
        
        goalsTableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // set default background when no goals are available
        goals.asObservable()
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [unowned self] missions in
                if missions.count == 0 {
                    let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.goalsTableView.bounds.size.width, height: self.goalsTableView.bounds.size.height))
                    noDataLabel.text = "No goals available"
                    noDataLabel.textColor = ThemePrimaryColor
                    noDataLabel.textAlignment = NSTextAlignment.center
                    self.goalsTableView.backgroundView = noDataLabel
                    self.goalsTableView.separatorStyle = .none
                } else {
                    self.goalsTableView.backgroundView = nil
                    self.goalsTableView.separatorStyle = .singleLine
                }
            }).disposed(by: disposeBag)
        
        let noChildren: Observable<Bool> = parentSubject.map { parent -> Bool in parent!["kids"].arrayValue.count == 0 }.share(replay: 1)
        
        noChildren.bind(to: kidsView.rx.isHidden).disposed(by: disposeBag)
        noChildren.bind(to: summaryView.rx.isHidden).disposed(by: disposeBag)
        noChildren.bind(to: goalsTableView.rx.isHidden).disposed(by: disposeBag)
        noChildren.map{ value in !value }.bind(to: filterGoalsButton.rx.isEnabled).disposed(by: disposeBag)
        noChildren.map{ value in !value }.bind(to: noChildStackView.rx.isHidden).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    private func handleAvatarTap(user: JSON!) -> Void {
        if (kid.value != nil && kid.value!["id"].stringValue != user!["id"].stringValue) || kid.value == nil {
            kid.accept(user)
        }
    }
    
    private func getGoalBalance(for goal: JSON, at row: GoalTableViewCell) {
        if let kid = self.kid.value {
            Api.instance().rxInvoke(
                using: "Get",
                to: "userAccount/self/kids/\(kid["id"].stringValue)/goals/\(goal["id"].stringValue)/balance",
                api: "numoola-q2"
                ).subscribe(onNext: {result in
                    switch result {
                    case .success(let data):
                        if let json = data {
                            row.goalView.balance.accept(json["availableBalance"].doubleValue)
                        }
                    case .failure(let error as NSError):
                        print(error.localizedDescription)
                    }
                }).disposed(by: disposeBag)
        }
    }
    
    @objc func getGoalsForCurrentUser() {
        if let kid = kid.value {
            self.getGoals(for: kid)
        }
    }
    
    private func getGoals(for kid: JSON) {
        let statusOption = filters.value!.getSelectedOption(inGroup: "status")
        
        var statusFilter:String?
        
        switch statusOption?.key {
        case "Progress":
            statusFilter = "status/value eq 'Approved'"
        case "ToApprove":
            statusFilter = "status/value eq 'Pending'"
        case "Declined":
            statusFilter = "status/value eq 'Declined'"
        case "Ready":
            statusFilter = "status/value eq 'Ready'"
        case "Purchased":
            statusFilter = "status/value eq 'Done'"
        default:
            statusFilter = nil
        }
        
        let typeOption = filters.value!.getSelectedOption(inGroup: "type")
        
        var typeFilter:String?
  
        switch typeOption?.key {
        case "Spending":
            typeFilter = "type/value eq 'Spending'"
        case "Saving":
            typeFilter = "type/value eq 'Saving'"
        case "Charity":
            typeFilter = "type/value eq 'Charity'"
        default:
            typeFilter = nil
        }
        
        var filter = statusFilter != nil ? "(\(statusFilter!))" : ""
        if typeFilter != nil {
            filter = filter.isEmpty ? "(\(typeFilter!))" : "\(filter) and (\(typeFilter!))"
        }
        
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/kids/\(kid["id"].stringValue)/goals",
            with: RequestOptions(
                select: "id,name,description,cost,status/value as 'status',status/description as 'statusDescription',type/value as 'type',type/description as 'typeDescription', photo/id as 'photoId', photo/$modifiedOn as 'photoModifiedOn'",
                filter: filter
            ))
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: {[self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.goals.accept(json["value"].arrayValue)
                    }
                case .failure(let error):
                    Notification.showError(using: error)
                }
                self.goalsTableView.refreshControl?.endRefreshing()
            }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FilterGoalsSegue" {
            if let controller = segue.destination as? FilterViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "AddChildSegue" {
            if let controller = segue.destination as? SignupChildViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentGoalsViewController: UITableViewDelegate {
    
    func contextualApproveAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Approve", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let goal = self.goals.value[indexPath.row]
            do {
                let data = try JSON(parseJSON: "{}").rawData()
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/kids/\(self.kid.value!["id"].stringValue)/goals/\(goal["id"].stringValue)/approve", payload: data)
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success:
                            Notification.show(title: "Goal Approved", message: "Goal \"\(goal["name"].stringValue)\" has been approved for \(self.kid.value!["firstName"].stringValue)", type: .Success)
                            self.getGoals(for: self.kid.value!)
                            completionHandler(true)
                        case .failure(let error as NSError):
                            print(error.localizedDescription)
                            completionHandler(false)
                        }
                    }).disposed(by: self.disposeBag)
            } catch {
                completionHandler(false)
            }
        })
        action.backgroundColor = ThemeAccentColor1
        return action
    }
    func contextualCompleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Complete", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let goal = self.goals.value[indexPath.row]
            do {
                let data = try JSON(parseJSON: "{}").rawData()
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/kids/\(self.kid.value!["id"].stringValue)/goals/\(goal["id"].stringValue)/complete", payload: data)
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success:
                            Notification.show(title: "Goal Completed", message: "Goal \"\(goal["name"].stringValue)\" has been completed for \(self.kid.value!["firstName"].stringValue)", type: .Success)
                            self.getGoals(for: self.kid.value!)
                            completionHandler(true)
                        case .failure(let error as NSError):
                            print(error.localizedDescription)
                            completionHandler(false)
                        }
                    }).disposed(by: self.disposeBag)
            } catch {
                completionHandler(false)
            }
        })
        action.backgroundColor = ThemeAccentColor1
        return action
    }
    func contextualRejectAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Reject", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let goal = self.goals.value[indexPath.row]
            do {
                // TODO: capture comments
                let data = try JSON(parseJSON: "{}").rawData()
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/kids/\(self.kid.value!["id"].stringValue)/goals/\(goal["id"].stringValue)/decline", payload: data)
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success:
                            Notification.show(title: "Goal Rejected", message: "Goal \"\(goal["name"].stringValue)\" has been marked as rejected for \(self.kid.value!["firstName"].stringValue)", type: .Success)
                            self.getGoals(for: self.kid.value!)
                            completionHandler(true)
                        case .failure(let error as NSError):
                            print(error.localizedDescription)
                            completionHandler(false)
                        }
                    }).disposed(by: self.disposeBag)
            } catch {
                completionHandler(false)
            }
        })
        action.backgroundColor = ThemeErrorColor
        return action
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actions:[UIContextualAction] = []
        let goal = self.goals.value[indexPath.row]
        let status = goal["status"].stringValue
        switch status {
        case "Pending":
            actions.append(self.contextualRejectAction(forRowAtIndexPath: indexPath))
            actions.append(self.contextualApproveAction(forRowAtIndexPath: indexPath))
        case "Ready":
            actions.append(self.contextualCompleteAction(forRowAtIndexPath: indexPath))
        default:
            print("Unknow status")
        }
        let swipeConfig = UISwipeActionsConfiguration(actions: actions)
        swipeConfig.performsFirstActionWithFullSwipe = false
        return swipeConfig
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension ParentGoalsViewController: FilterViewControllerDelegate {
    
    func filterViewControllerWillShow(_ controller: FilterViewController) {
        UIController.instance().hideTabBar()
    }
    
    func filterViewControllerDidCancel(_ controller: FilterViewController) {
        UIController.instance().showTabBar()
    }
    
    func filterViewController(_ controller: FilterViewController, didFilter filters: FilterOptions!) {
        UIController.instance().showTabBar()
        self.filters.accept(filters)
    }
    
    func filterViewControllerFiltersToShow(_ controller: FilterViewController) -> FilterOptions! {
        return filters.value!.copy() as? FilterOptions
    }
    
    func filterViewControllerTitle(_ controller: FilterViewController) -> String {
        return "Filter Goals"
    }
}

extension ParentGoalsViewController: SignupChildViewControllerDelegate {
    func signupChildViewControllerDidCancel(_ controller: SignupChildViewController) {
        UIController.instance().showTabBar()
    }
    
    func signupChildViewController(_ controller: SignupChildViewController, didFinishAdding kid: JSON) {
        UIController.instance().showTabBar()
    }
    
    func signupChildViewControllerWillShow(_ controller: SignupChildViewController) {
        UIController.instance().hideTabBar()
    }
}
