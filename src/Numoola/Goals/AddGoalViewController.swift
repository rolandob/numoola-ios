//
//  AddGoalViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/26/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import BetterSegmentedControl

class GoalModel {
    var name: String
    var description: String
    var cost: Double
    var type: String
    var photo: UIImage?
    
    init(_ name: String, _ description: String, _ cost: Double, _ type: String, _ photo: UIImage?) {
        self.name = name
        self.description = description
        self.cost = cost
        self.type = type
        self.photo = photo
    }
}

protocol AddGoalViewControllerDelegate:class {
    func addGoalViewControllerDidCancel(_ controller: AddGoalViewController)
    func addGoalViewController(_ controller: AddGoalViewController, didFinishAdding goal: GoalModel)
    func addGoalViewControllerWillShow(_ controller: AddGoalViewController)
}

class AddGoalViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var nameTextField: NMLTextField!
    @IBOutlet var costTextField: NMLTextField!
    @IBOutlet var descriptionTextField: NMLTextField!
    @IBOutlet var mainView: UIView!
    @IBOutlet var typeControl: BetterSegmentedControl!
    // delegate
    weak var delegate: AddGoalViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let type:BehaviorRelay<String> = BehaviorRelay(value: "Spending")
    let photo:BehaviorRelay<UIImage?> = BehaviorRelay(value: nil)
    private let pickerController: UIImagePickerController = UIImagePickerController()
}

extension AddGoalViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.mediaTypes = ["public.image"]
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        typeControl.segments = LabelSegment.segments(withTitles: ["Spending", "Charity"], numberOfLines: 1, normalBackgroundColor: .clear, normalFont: UIFont.systemFont(ofSize: 15, weight: .light), normalTextColor: ThemePrimaryColor, selectedBackgroundColor: ThemePrimaryColor, selectedFont: UIFont.systemFont(ofSize: 16, weight: .bold), selectedTextColor: .white)
        
        typeControl.options = [.cornerRadius(20),.bouncesOnChange(true), .alwaysAnnouncesValue(true), .backgroundColor(.clear),.indicatorViewBorderWidth(CGFloat(5)),.indicatorViewBackgroundColor(ThemePrimaryColor)]
        
        typeControl.rx.controlEvent(.valueChanged).observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] _ in
            switch self.typeControl.index {
            case 0:
                self.type.accept("Spending")
            case 1:
                self.type.accept("Charity")
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: {[self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.addGoalViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: {[self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.addGoalViewController(self, didFinishAdding: GoalModel(self.nameTextField.value.value!, self.descriptionTextField.value.value ?? "", Double(self.costTextField.value.value!)!, self.type.value, self.photo.value))
        }).disposed(by: disposeBag)
        
        let allValid: Observable<Bool> = Observable.combineLatest(nameTextField.valid, costTextField.valid) { $0 && $1 }
        // bind done isEnabled
        allValid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
        photo.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] photo in
            guard let photo = photo else { return }
            self.photoImageView.image = photo
        }).disposed(by: disposeBag)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        delegate?.addGoalViewControllerWillShow(self)
    }
    
    @IBAction func handlePhotoTap(_ sender: Any?) {
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Set Goal Photo", preferredStyle: .actionSheet)
        
        // 2
        let pickAction = UIAlertAction(title: "Photo Library", style: .default, handler: { [self] _ in
            self.pickerController.sourceType = .photoLibrary
            self.present(self.pickerController, animated: true)
        })
        let cameraAction = UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.pickerController.sourceType = .camera
            self.present(self.pickerController, animated: true)
        })
        
        // 3
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        // 4
        optionMenu.addAction(pickAction)
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
}

extension AddGoalViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }
        let resizedImage = image.resized(to: CGSize(width: 100, height: 100))
        self.photo.accept(resizedImage)
        self.pickerController.dismiss(animated: true)
    }
}

extension AddGoalViewController: UINavigationControllerDelegate {
    
}
