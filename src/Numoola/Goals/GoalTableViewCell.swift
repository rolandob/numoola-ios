//
//  GoalTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/17/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit

class GoalTableViewCell: UITableViewCell {
    // fields
    var goalView: GoalView!
}

extension GoalTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tintColor = ThemePrimaryColor
        // create avatar view
        self.goalView = UINib(nibName: "GoalView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? GoalView
        self.goalView.frame.size.width = self.contentView.frame.size.width
        self.goalView.frame.size.height = self.contentView.frame.size.height
        self.contentView.addSubview(self.goalView)
    }
}
