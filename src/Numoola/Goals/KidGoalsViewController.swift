//
//  KidGoalsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/24/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class KidGoalsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var summaryView: UIView!
    weak var titleView: PageTitleView!
    // fields
    var disposeBag = DisposeBag()
    let kidSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "kid")
    let goals:BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    let filters:BehaviorRelay<FilterOptions?> = BehaviorRelay(value: FilterOptions(
        [
            FilterOptionGroup(
                in: 0,
                identifier: "status",
                label: "Status",
                items: [
                    FilterOption(identifier: "Any", label: "Any", selected: true),
                    FilterOption(identifier: "Progress", label: "In Progress"),
                    FilterOption(identifier: "ToApprove", label: "Pending Approval"),
                    FilterOption(identifier: "Declined", label: "Declined"),
                    FilterOption(identifier: "Ready", label: "Ready to Complete"),
                    FilterOption(identifier: "Purchased", label: "Purchased")
                ]
            ),
            FilterOptionGroup(
                in: 1,
                identifier: "type",
                label: "Type",
                items: [
                    FilterOption(identifier: "Any", label: "Any", selected: true),
                    FilterOption(identifier: "Spending", label: "Spending Goal"),
                    FilterOption(identifier: "Saving", label: "Saving Goal"),
                    FilterOption(identifier: "Charity", label: "Charity Goal")
                ]
            )
        ]))
}

extension KidGoalsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.tableView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:  #selector(getGoals), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        
        kidSubject.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] kid in
            guard let kid = kid else { return }
            if self.titleView != nil {
                self.titleView.removeFromSuperview()
            }
            self.titleView = UINib(nibName: "PageTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PageTitleView
            self.titleView.frame.size.width = self.summaryView.frame.size.width
            self.titleView.frame.size.height = self.summaryView.frame.size.height
            self.summaryView.addSubview(self.titleView)
            self.titleView.user.accept(kid)
            self.titleView.title.accept("\(kid["firstName"].stringValue)'s Goals")
            self.getGoals()
        }).disposed(by: disposeBag)
        
        // bind goals to table view
        goals.bind(to: tableView.rx.items(cellIdentifier: "GoalItem")) { (row, goal, cell) in
            if let cell = cell as? GoalTableViewCell {
                cell.goalView.goal.accept(goal)
                self.getGoalBalance(for: goal, at: cell)
            }
            }.disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // set default background when no goals are available
        goals.asObservable()
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [unowned self] goals in
                if goals.count == 0 {
                    let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
                    noDataLabel.text = "No goals available"
                    noDataLabel.textColor = ThemePrimaryColor
                    noDataLabel.textAlignment = NSTextAlignment.center
                    self.tableView.backgroundView = noDataLabel
                    self.tableView.separatorStyle = .none
                } else {
                    self.tableView.backgroundView = nil
                    self.tableView.separatorStyle = .singleLine
                }
            }).disposed(by: disposeBag)
        
        filters.subscribe(onNext: { [self] filters in
            guard let _ = filters else { return }
            self.getGoals()
        }).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    private func getGoalBalance(for goal: JSON, at row: GoalTableViewCell) {
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/goals/\(goal["id"].stringValue)/balance",
            api: "numoola-q2"
            ).subscribe(onNext: {result in
                switch result {
                case .success(let data):
                    if let json = data {
                        row.goalView.balance.accept(json["availableBalance"].doubleValue)
                    }
                case .failure(let error as NSError):
                    print(error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    
    @objc private func getGoals() {
        let statusOption = filters.value!.getSelectedOption(inGroup: "status")
        
        var statusFilter:String?
        
        switch statusOption?.key {
        case "Progress":
            statusFilter = "status/value eq 'Approved'"
        case "ToApprove":
            statusFilter = "status/value eq 'Pending'"
        case "Declined":
            statusFilter = "status/value eq 'Declined'"
        case "Ready":
            statusFilter = "status/value eq 'Ready'"
        case "Purchased":
            statusFilter = "status/value eq 'Done'"
        default:
            statusFilter = nil
        }
        
        let typeOption = filters.value!.getSelectedOption(inGroup: "type")
        
        var typeFilter:String?
        
        switch typeOption?.key {
        case "Spending":
            typeFilter = "type/value eq 'Spending'"
        case "Saving":
            typeFilter = "type/value eq 'Saving'"
        case "Charity":
            typeFilter = "type/value eq 'Charity'"
        default:
            typeFilter = nil
        }
        
        var filter = statusFilter != nil ? "(\(statusFilter!))" : ""
        if typeFilter != nil {
            filter = filter.isEmpty ? "(\(typeFilter!))" : "\(filter) and (\(typeFilter!))"
        }
        
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/goals",
            with: RequestOptions(
                select: "id,name,description,cost,status/value as 'status',status/description as 'statusDescription',type/value as 'type',type/description as 'typeDescription', photo/id as 'photoId', photo/$modifiedOn as 'photoModifiedOn'",
                filter: filter
        ))
        .observeOn(ConcurrentMainScheduler.instance)
        .subscribe(onNext: {[self] result in
            switch result {
            case .success(let data):
                if let json = data {
                    self.goals.accept(json["value"].arrayValue)
                }
            case .failure(let error):
                Notification.showError(using: error)
            }
            self.tableView.refreshControl?.endRefreshing()
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FilterGoalsSegue" {
            if let controller = segue.destination as? FilterViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "AddGoalSegue" {
            if let controller = segue.destination as? AddGoalViewController {
                controller.delegate = self
            }
        }
    }
}

extension KidGoalsViewController: UITableViewDelegate {
   
    func contextualDeleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive,title: "Delete", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let goal = self.goals.value[indexPath.row]
            let emptyPayload = JSON(parseJSON: "{}")
            do {
                try Api.instance().rxInvoke(using: "Post", to: "userAccount/self/goals/\(goal["id"].stringValue)/delete", payload: emptyPayload.rawData())
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success:
                            Notification.show(title: "Goal Deleted", message: "Goal \"\(goal["name"].stringValue)\" has been deleted", type: .Success)
                            completionHandler(true)
                        case .failure(let error as NSError):
                            print(error.localizedDescription)
                            completionHandler(false)
                        }
                    }).disposed(by: self.disposeBag)
            } catch let error as NSError {
                print(error.userInfo)
                completionHandler(false)
            }
        })
        action.backgroundColor = ThemeErrorColor
        return action
    }
    
    func contextualAddMoolaAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Add Moola", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let goal = self.goals.value[indexPath.row]
            print("Add Moola to goal \(goal["id"].stringValue)")
            completionHandler(true)
        })
        action.backgroundColor = ThemeAccentColor1
        return action
    }
    
    func contextualCompleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Complete", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let goal = self.goals.value[indexPath.row]
            print("Complete goal \(goal["id"].stringValue)")
            completionHandler(true)
        })
        action.backgroundColor = ThemeAccentColor1
        return action
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actions:[UIContextualAction] = []
        let goal = self.goals.value[indexPath.row]
        
        let cell = tableView.cellForRow(at: indexPath) as? GoalTableViewCell
        let goalBalance = cell?.goalView.balance.value
        let goalCost = goal["cost"].doubleValue
        
        let status = goal["status"].stringValue
        switch status {
        case "Approved":
            actions.append(self.contextualDeleteAction(forRowAtIndexPath: indexPath))
            if goalBalance! < goalCost {
                actions.append(self.contextualAddMoolaAction(forRowAtIndexPath: indexPath))
            } else {
                actions.append(self.contextualCompleteAction(forRowAtIndexPath: indexPath))
            }
        case "Pending", "Declined", "Ready":
            actions.append(self.contextualDeleteAction(forRowAtIndexPath: indexPath))
        default:
            print("Unknow status")
        }
        let swipeConfig = UISwipeActionsConfiguration(actions: actions)
        swipeConfig.performsFirstActionWithFullSwipe = false
        return swipeConfig
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension KidGoalsViewController: FilterViewControllerDelegate {
    
    func filterViewControllerWillShow(_ controller: FilterViewController) {
        UIController.instance().hideTabBar()
    }
    
    func filterViewControllerDidCancel(_ controller: FilterViewController) {
        UIController.instance().showTabBar()
    }
    
    func filterViewController(_ controller: FilterViewController, didFilter filters: FilterOptions!) {
        UIController.instance().showTabBar()
        self.filters.accept(filters)
    }
    
    func filterViewControllerFiltersToShow(_ controller: FilterViewController) -> FilterOptions! {
        return filters.value!.copy() as? FilterOptions
    }
    
    func filterViewControllerTitle(_ controller: FilterViewController) -> String {
        return "Filter Goals"
    }
}

extension KidGoalsViewController: AddGoalViewControllerDelegate {
    func addGoalViewControllerDidCancel(_ controller: AddGoalViewController) {
        UIController.instance().showTabBar()
    }
    
    func addGoalViewController(_ controller: AddGoalViewController, didFinishAdding goal: GoalModel) {
        
        ModalLogo.showModal(with: "Creating...", in: { () -> Observable<Result<JSON?, Error>> in
            var goalInstance = JSON(parseJSON: "{}")
            goalInstance["$class"].string = "Goal"
            goalInstance["name"].string = goal.name
            goalInstance["description"].string = goal.description
            goalInstance["cost"].double = goal.cost
            goalInstance["status"].string = "Pending"
            goalInstance["type"].string = goal.type
            
            var kidInstance = JSON(parseJSON: "{}")
            kidInstance["loaded"].bool = false
            kidInstance["$class"].string = "Kid"
            kidInstance["id"].string = self.kidSubject.value!["id"].string
            
            goalInstance["kid"].object = kidInstance.object
            
            if goal.photo != nil {
                return Api.instance().rxUploadFile(content: goal.photo!.pngData()?.base64EncodedString().data(using: .utf8), type: "text/plain")
                    .flatMap({ result -> Observable<Result<JSON?, Error>> in
                        switch result {
                        case .success(let data):
                            if let content = data {
                                
                                var documentInstance = JSON(parseJSON: "{}")
                                documentInstance["$class"].string = "Document"
                                documentInstance["name"].string = "GoalPhoto"
                                documentInstance["type"].string = "Resource"
                                documentInstance["status"].string = "Completed"
                                documentInstance["content"].object = content.object
                                
                                goalInstance["photo"].object = documentInstance.object
                                return self.addGoal(with: goalInstance)
                            }
                        case .failure(let error as NSError):
                            print(error.localizedDescription)
                        }
                        return Observable.just(.failure(NSError(domain: "", code: 0, userInfo: ["message": "An error has occurred when adding the Goal, please try again"])))
                    })
            } else {
                return self.addGoal(with: goalInstance)
            }
        }()).subscribe(onNext: { result in
                switch result {
                case .success(let data):
                    if let goal = data {
                        self.getGoals()
                        Notification.show(title: "Goal Added", message: "Congratulations! The goal \"\(goal["name"].stringValue)\" was added successfully, now you can contribute Moola to achieve it", type: .Success)
                    }
                case .failure(let error as NSError):
                    print(error.userInfo)
                    Notification.show(title: "Something went wrong", message: (error.userInfo["message"] as? String)!, type: .Error)
                }
            UIController.instance().showTabBar()
            }).disposed(by: disposeBag)
    }
    
    func addGoalViewControllerWillShow(_ controller: AddGoalViewController) {
        UIController.instance().hideTabBar()
    }
    
    private func addGoal(with instance: JSON) -> Observable<Result<JSON?, Error>> {
            do {
                let data = try instance.rawData()
                return Api.instance().rxInvoke(using: "Post", to: "userAccount/self/goals", payload: data)
            } catch let error as NSError {
                print(error.localizedDescription)
                return Observable.just(.failure(NSError(domain: "", code: 0, userInfo: ["message": "An error has occurred when adding the Goal, please try again"])))
            }
    }
}
