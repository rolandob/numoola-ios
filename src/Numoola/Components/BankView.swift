//
//  BankView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/5/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import FontAwesome_swift

class BankView: UIControl {
    // controls
    @IBOutlet var accountNameLabel: UILabel!
    @IBOutlet var accountNumberLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var iconView: FontAwesomeImageView!
    // fields
    var disposeBag = DisposeBag()
    let bank:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
}

extension BankView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconView.imageColor = ThemeSecondaryColor
        iconView.awakeFromNib()
        
        bank.subscribe(onNext: { [self] bank in
            guard let bank = bank else { return }
            self.accountNameLabel.text = "\(bank["alias"].stringValue) - \(bank["firstName"].stringValue) \(bank["lastName"].stringValue)"
            self.accountNumberLabel.text = "****\(bank["accountNumber"].stringValue.suffix(4))"
            self.typeLabel.text = bank["type"].stringValue
            self.statusLabel.text = bank["status"].stringValue
        }).disposed(by: disposeBag)
    }
}
