//
//  GoalView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/28/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import MBCircularProgressBar

class GoalView: UIControl {
    // controls
    @IBOutlet var progressView: MBCircularProgressBarView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var costLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var photoImageView: UIImageView!
    // fields
    var disposeBag = DisposeBag()
    let goal:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let balance:BehaviorRelay<Double> = BehaviorRelay(value: 0.0)
}

extension GoalView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        progressView.progressAngle = 100
        progressView.showUnitString = false
        progressView.valueFontSize = 12
        progressView.progressLineWidth = 3
        goal.subscribe(onNext: { [self] goal in
            guard let goal = goal else { return }
            self.nameLabel.text = goal["name"].stringValue
            self.descriptionLabel.text = goal["description"].stringValue
            self.costLabel.text = "\(goal["cost"].floatValue)"
            self.statusLabel.text = goal["statusDescription"].stringValue
            self.typeLabel.text = goal["typeDescription"].stringValue
            self.progressView.maxValue = CGFloat(goal["cost"].floatValue)
            
            let status = goal["status"].stringValue
            switch status {
            case "Done", "Ready":
                self.progressView.progressColor = ThemePrimaryColor
                self.progressView.progressStrokeColor = ThemePrimaryColor
            default:
                self.progressView.progressColor = ThemePrimaryColor
                self.progressView.progressStrokeColor = ThemePrimaryColor
            }
            
            self.loadPhoto(for: goal)
        }).disposed(by: disposeBag)
        
        balance.observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] balance in
                UIView.animate(withDuration: 1, animations: {
                    if let goal = self.goal.value {
                        if goal["status"].stringValue == "Done" {
                            self.progressView.value = CGFloat(goal["cost"].floatValue)
                        } else {
                            self.progressView.value = CGFloat(balance)
                        }
                    } else {
                        self.progressView.value = CGFloat(balance)
                    }
                })
            }).disposed(by: disposeBag)
    }
    
    private func loadPhoto(for goal: JSON) {
        if goal["photoId"].exists() && goal["photoId"] != JSON.null {
            
            var goalInstance = JSON(parseJSON: "{}")
            goalInstance["$class"].string = "Goal"
            goalInstance["id"].string = goal["id"].string
            var photoInstance = JSON(parseJSON: "{}")
            photoInstance["id"].string = goal["photoId"].string
            photoInstance["$modifiedOn"].string = goal["photoModifiedOn"].string
            
            goalInstance["photo"].object = photoInstance.object
            
            Api.instance().downloadPhoto(for: goalInstance, withField: "photo", completionHandler: {
                data, error in
                if let error = error as NSError? {
                    print(error.localizedDescription)
                } else {
                    DispatchQueue.main.async {
                        if let base64Data = data {
                            let decodedData = Data(base64Encoded: base64Data)
                            self.photoImageView.contentMode = .scaleAspectFit
                            self.photoImageView.image = UIImage(data: decodedData!)
                            self.photoImageView.layoutIfNeeded()
                        }
                    }
                }
            })
        } else {
            self.photoImageView.image = UIImage(named: "GoalImage")
        }
    }
}
