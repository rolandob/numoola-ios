//
//  FieldCheckView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/19/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FontAwesome_swift

class FieldCheckView: UIControl {
    // controls
    @IBOutlet var checkView: FontAwesomeImageView!
    // fields
    var disposeBag = DisposeBag()
    private let hiddenSubject:BehaviorRelay<Bool> = BehaviorRelay(value: true)
}

extension FieldCheckView {
    override func awakeFromNib() {
        super.awakeFromNib()
        checkView?.imageColor = ThemeSuccessColor
        checkView?.awakeFromNib()
        
        hiddenSubject.subscribe(onNext: { [self] value in
            self.checkView.isHidden = value
        }).disposed(by: disposeBag)
    }
}

extension FieldCheckView:FieldCheckComponent {
    func setHidden(_ hidden: Bool) {
        hiddenSubject.accept(hidden)
    }
}
