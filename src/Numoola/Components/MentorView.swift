//
//  MentorView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/24/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SwiftyJSON

class MentorView: UIControl {
    // controls
    @IBOutlet var mentorAvatarView: UIImageView!
    @IBOutlet var kidAvatarView: UIImageView!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var mentorLabel: UILabel!
    @IBOutlet var kidLabel: UILabel!
    // fields
    let disposeBag = DisposeBag()
    let mentor:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
}

extension MentorView {
    override func awakeFromNib() {
        super.awakeFromNib()
        mentorAvatarView.backgroundColor = ThemePrimaryColor
        kidAvatarView.backgroundColor = ThemePrimaryColor
        mentorAvatarView.layer.cornerRadius = mentorAvatarView.frame.width/2
        kidAvatarView.layer.cornerRadius = kidAvatarView.frame.width/2
        mentor.subscribe(onNext: { [self] mentor in
            guard let mentor = mentor else { return }
            self.mentorLabel.text = "\(mentor["relationship"].stringValue) \(mentor["currentMentorFirstName"].string ?? mentor["mentorFirstName"].stringValue)"
            self.kidLabel.text = mentor["kidFirstName"].stringValue
            if mentor["status"].stringValue == "Invited" {
                self.statusLabel.text = "Invited to mentor"
            } else {
                self.statusLabel.text = "Mentors"
            }
            
            if mentor["kidPhotoId"].exists() && mentor["kidPhotoId"].string != nil {
                let photoInstance = JSON(dictionaryLiteral: ("id", mentor["kidPhotoId"].stringValue), ("$modifiedOn", mentor["kidPhotoModifiedOn"]))
                let kidInstance = JSON(dictionaryLiteral: ("id", mentor["kidId"].stringValue), ("photo", photoInstance), ("$class", "Kid"))
                Api.instance().downloadPhoto(for: kidInstance, withField: "photo", completionHandler: {
                    data, error in
                    if let error = error as NSError? {
                        print(error.localizedDescription)
                    } else {
                        DispatchQueue.main.async {
                            if let base64Data = data {
                                let decodedData = Data(base64Encoded: base64Data)
                                self.kidAvatarView.contentMode = .scaleAspectFit
                                self.kidAvatarView.image = UIImage(data: decodedData!)
                                self.kidAvatarView.layoutIfNeeded()
                                self.kidAvatarView.backgroundColor = .clear
                            }
                        }
                    }
                })
            } else {
                self.kidAvatarView.image = UIImage(named: "Avatar")
                self.kidAvatarView.backgroundColor = ThemePrimaryColor
            }
            if mentor["mentorPhotoId"].exists() && mentor["mentorPhotoId"].string != nil {
                let photoInstance = JSON(dictionaryLiteral: ("id", mentor["mentorPhotoId"].stringValue), ("$modifiedOn", mentor["mentorPhotoModifiedOn"]))
                let mentorInstance = JSON(dictionaryLiteral: ("id", mentor["mentorId"].stringValue), ("photo", photoInstance), ("$class", "Parent"))
                Api.instance().downloadPhoto(for: mentorInstance, withField: "photo", completionHandler: {
                    data, error in
                    if let error = error as NSError? {
                        print(error.localizedDescription)
                    } else {
                        DispatchQueue.main.async {
                            if let base64Data = data {
                                let decodedData = Data(base64Encoded: base64Data)
                                self.mentorAvatarView.contentMode = .scaleAspectFit
                                self.mentorAvatarView.image = UIImage(data: decodedData!)
                                self.mentorAvatarView.layoutIfNeeded()
                                self.mentorAvatarView.backgroundColor = .clear
                            }
                        }
                    }
                })
            } else {
                self.mentorAvatarView.image = UIImage(named: "Avatar")
                self.mentorAvatarView.backgroundColor = ThemePrimaryColor
            }
        }).disposed(by: disposeBag)
    }
}
