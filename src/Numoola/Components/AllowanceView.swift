//
//  AllowanceView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/26/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class AllowanceView: UIControl {
    // controls
    @IBOutlet var mainView: UIView!
    @IBOutlet var allowanceLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let allowance:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    var handleAllowanceTap: ( () -> Void )?
}

extension AllowanceView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        allowance.subscribe(onNext: { [self] allowance in
            guard let allowance = allowance else { return }
            if allowance["id"].exists() {
                let occurenceType = allowance["recurrenceType"]["value"].string!
                switch occurenceType {
                case "Daily":
                    self.allowanceLabel.text = "$\(allowance["amount"].float!) daily"
                case "Weekly":
                    self.allowanceLabel.text = "$\(allowance["amount"].float!) every week on \(Date.from(string: allowance["startDate"].string!, format: .DateTime)!.to(format: .WeekDay)!)"
                case "BiWeekly":
                    self.allowanceLabel.text = "$\(allowance["amount"].float!) every two weeks on \(Date.from(string: allowance["startDate"].string!, format: .DateTime)!.to(format: .WeekDay)!)"
                case "Monthly":
                    self.allowanceLabel.text = "$\(allowance["amount"].float!) every month on the \(Date.from(string: allowance["startDate"].string!, format: .DateTime)!.to(format: .Day)!)"
                default:
                    self.allowanceLabel.text = "$0.0"
                }
            } else {
                self.allowanceLabel.text = "$0.0"
            }
        }).disposed(by: disposeBag)
        
        mainView.layer.cornerRadius = 20
        mainView.backgroundColor = .white
        mainView.layer.borderColor = ThemePrimaryColor.cgColor
        mainView.layer.borderWidth = 1
    }
    
    @IBAction func handleTap(_ sender: Any) {
        if handleAllowanceTap != nil {
            self.handleAllowanceTap!()
        }
    }
}
