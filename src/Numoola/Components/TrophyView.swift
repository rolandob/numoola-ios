//
//  TrophyView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/23/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class TrophyView: UIControl {
    // controls
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var progressView: UIProgressView!
    // fields
    var disposeBag = DisposeBag()
    let trophy:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
}

extension TrophyView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.progressView.tintColor = ThemePrimaryColor
        self.progressView.transform = CGAffineTransform(scaleX: 1.0, y: 2.0)
        trophy.subscribe(onNext: { [self] trophy in
            guard let trophy = trophy else { return }
            if trophy["level"].exists() == false || trophy["level"].stringValue == "" {
                self.imageView.image = UIImage(named: "reward-\(trophy["type"].stringValue)-default")
            } else {
                self.imageView.image = UIImage(named: "reward-\(trophy["type"].stringValue)-\(trophy["level"].stringValue)")
            }
            if trophy["points"].exists() == false {
                self.progressView.isHidden = true
            } else {
                self.progressView.isHidden = false
                let maxPoints = trophy["maxPoints"].doubleValue
                let points = trophy["points"].doubleValue
                self.progressView.setProgress(Float(points / maxPoints), animated: true)
            }
        }).disposed(by: disposeBag)
    }
}
