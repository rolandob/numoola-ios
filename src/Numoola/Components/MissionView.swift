//
//  MissionView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/28/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class MissionView: UIControl {
    // controls
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var occurrenceLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let mission:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
}

extension MissionView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mission.subscribe(onNext: { [self] mission in
            guard let mission = mission else { return }
            self.nameLabel.text = mission["name"].stringValue
            self.amountLabel.text = "\(mission["payout"].floatValue)"
            self.occurrenceLabel.text = mission["recurrence"].stringValue
            self.dateLabel.text = Date.from(string: mission["startDate"].stringValue, format: .Persistent)?.to(format: .Display)
            self.statusLabel.text = mission["statusDescription"].stringValue
        }).disposed(by: disposeBag)
    }
}
