//
//  RewardsView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/25/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class RewardsView: UIControl {
    // controls
    @IBOutlet var mainView: UIView!
    @IBOutlet var pointsLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let points:BehaviorRelay<Int?> = BehaviorRelay(value: nil)
    var handleRewardsTap: ( () -> Void )?
}

extension RewardsView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        points.subscribe(onNext: { [self] points in
            guard let points = points else { return }
            self.pointsLabel.text = "\(points)"
        }).disposed(by: disposeBag)
        
        mainView.layer.cornerRadius = 20
        mainView.backgroundColor = .white
        mainView.layer.borderColor = ThemePrimaryColor.cgColor
        mainView.layer.borderWidth = 1
    }
    
    @IBAction func handleTap(_ sender: Any) {
        if handleRewardsTap != nil {
            self.handleRewardsTap!()
        }
    }
}
