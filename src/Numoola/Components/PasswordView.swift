//
//  PasswordView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/10/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import FontAwesome_swift

class PasswordView: UIControl {
    // controls
    @IBOutlet var passwordTextField: NMLTextField!
    @IBOutlet var repeatPasswordTextField: NMLTextField!
    @IBOutlet var minIconView: FontAwesomeImageView!
    @IBOutlet var numberIconView: FontAwesomeImageView!
    @IBOutlet var symbolIconView: FontAwesomeImageView!
    @IBOutlet var uppercaseIconView: FontAwesomeImageView!
    @IBOutlet var lowercaseIconView: FontAwesomeImageView!
    // fields
    var disposeBag = DisposeBag()
    let password:BehaviorRelay<String?> = BehaviorRelay(value: "")
    let repeatPassword:BehaviorRelay<String?> = BehaviorRelay(value: "")
    var isValid: BehaviorRelay<Bool> = BehaviorRelay(value: false)
}

extension PasswordView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let isPasswordValid: Observable<Bool> = password.map{ value -> Bool in value != nil && value!.isEmpty == false }.share(replay: 1)
        let isConfirmPasswordValid: Observable<Bool> = repeatPassword.map{ value -> Bool in value != nil && value!.isEmpty == false }.share(replay: 1)
        let areBothPasswordsValid: Observable<Bool> = Observable.combineLatest(password, repeatPassword).map{ pass1, pass2 -> Bool in pass1 == pass2}
        Observable.combineLatest(isPasswordValid, isConfirmPasswordValid, areBothPasswordsValid) { $0 && $1 && $2 }.bind(to: isValid).disposed(by: disposeBag)
    }
}
