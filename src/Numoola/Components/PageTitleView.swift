//
//  PageTitleView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/24/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SwiftyJSON

class PageTitleView: UIControl {
    // controls
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var mainView: UIView!
    // fields
    let disposeBag = DisposeBag()
    let havePhoto:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let user:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let title:BehaviorRelay<String?> = BehaviorRelay(value: "")
}

extension PageTitleView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.mainView.backgroundColor = theme.getColor(with: PageTitleStyles.BackgroundColor, for: .normal)
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: PageTitleStyles.CornerRadius, for: .normal)!)
            self.titleLabel.textColor = theme.getColor(with: PageTitleStyles.TitleColor, for: .normal)
            self.avatarImageView.backgroundColor = theme.getColor(with: PageTitleStyles.AvatarBackgroundColor, for: .normal)
        }).disposed(by: disposeBag)
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height/2
        
        user.subscribe(onNext: { [self] user in
            guard let user = user else { return }
            self.setAvatarImage(for: user)
        }).disposed(by: disposeBag)
        
        havePhoto.subscribe(onNext: { [self] value in
            self.avatarImageView.backgroundColor = value ? .white : ThemePrimaryColor
        }).disposed(by: disposeBag)
        
        title.subscribe(onNext: { [self] text in
            self.titleLabel.text = text
        }).disposed(by: disposeBag)
        
    }
    
    private func setAvatarImage(for user: JSON) {
        if user["photo"].exists() {
            Api.instance().downloadPhoto(for: user, withField: "photo", completionHandler: {
                data, error in
                if (error as NSError?) != nil {
                    self.havePhoto.accept(false)
                } else {
                    DispatchQueue.main.async {
                        if let base64Data = data {
                            let decodedData = Data(base64Encoded: base64Data)
                            self.avatarImageView.image = UIImage(data: decodedData!)
                            self.havePhoto.accept(true)
                        }
                    }
                }
            })
        } else {
            avatarImageView.image = UIImage(named: "Avatar")
            self.havePhoto.accept(false)
        }
    }
}
