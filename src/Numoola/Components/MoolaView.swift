//
//  MoolaView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/26/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MoolaView: UIControl {
    // controls
    @IBOutlet var mainView: UIView!
    @IBOutlet var moolaLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let moola:BehaviorRelay<Double?> = BehaviorRelay(value: nil)
    var handleMoolaTap: ( () -> Void )?
    var gradient: CAGradientLayer!
}

extension MoolaView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        moola.subscribe(onNext: { [self] moola in
            guard let moola = moola else { return }
            
            let behavior = NSDecimalNumberHandler(roundingMode: .plain, scale: 2, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: true)
            let totalBalanceRounded = NSDecimalNumber(value: moola).rounding(accordingToBehavior: behavior)
            
            let loadingProcess = LoadingProcess(minValue: 0, maxValue: totalBalanceRounded.floatValue)
            
            loadingProcess.simulateLoading(
                toValue: totalBalanceRounded.floatValue,
                step: totalBalanceRounded.floatValue / Float(40),
                valueChanged: { currentValue in
                    let roundedValue = NSDecimalNumber(value: currentValue).rounding(accordingToBehavior: behavior)
                    self.moolaLabel.text = "$\(roundedValue)"
            },
                completion: { value in
                    self.moolaLabel.text = "$\(totalBalanceRounded)"
            }
            )
            if self.gradient == nil {
                
                self.gradient = CAGradientLayer()
                
                self.gradient.frame = self.mainView.frame
                self.gradient.colors = [ThemeWarningColor.cgColor, UIColor.white.cgColor]
                self.gradient.cornerRadius = self.mainView.frame.height/2
                self.gradient.borderColor = ThemePrimaryColor.cgColor
                self.gradient.borderWidth = 1
                self.mainView.layer.insertSublayer(self.gradient, at: 0)
                self.mainView.isHidden = false
            }
        }).disposed(by: disposeBag)
        mainView.isHidden = true
        mainView.backgroundColor = .clear
    }
    
    @IBAction func handleTap(_ sender: Any) {
        if handleMoolaTap != nil {
            self.handleMoolaTap!()
        }
    }
}
