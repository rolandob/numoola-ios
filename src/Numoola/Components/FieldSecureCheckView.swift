//
//  FieldSecureCheckView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/19/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FontAwesome_swift

class FieldSecureCheckView: UIControl {
    // controls
    @IBOutlet var checkView: FontAwesomeImageView!
    @IBOutlet var secureView: FontAwesomeImageView!
    @IBOutlet var secureMainView: UIView!
    // fields
    var disposeBag = DisposeBag()
    private var show = false
    private let hiddenSubject:BehaviorRelay<Bool> = BehaviorRelay(value: true)
    let showSecure:BehaviorRelay<Bool> = BehaviorRelay(value:false)
}

extension FieldSecureCheckView {
    override func awakeFromNib() {
        super.awakeFromNib()
        checkView?.imageColor = ThemeSuccessColor
        checkView?.awakeFromNib()
        
        secureView?.imageColor = ThemeSecondaryColor
        secureView?.awakeFromNib()
        
        let gesture = UITapGestureRecognizer(target: self, action:#selector(handleSecureTap(recognizer:)))
        self.secureMainView?.addGestureRecognizer(gesture)
        
        hiddenSubject.subscribe(onNext: { [self] value in
            self.checkView.isHidden = value
        }).disposed(by: disposeBag)
    }
    
    @objc func handleSecureTap(recognizer: UITapGestureRecognizer) {
        show = !show
        if show == true {
            secureView.cssCode = "fa-eye"
        } else {
            secureView.cssCode = "fa-eye-slash"
        }
        secureView.awakeFromNib()
        showSecure.accept(show)
    }
}


extension FieldSecureCheckView:FieldCheckComponent {
    func setHidden(_ hidden: Bool) {
        hiddenSubject.accept(hidden)
    }
}

