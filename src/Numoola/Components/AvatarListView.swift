//
//  AvatarListView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/31/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON

class AvatarListView: UIView {

    var handleAvatarTap: ( (_ kid: JSON?) -> Void )?
    
    @IBOutlet var kidsScrollView: UIScrollView!
    
    var kids: JSON!
    var selectedKid: JSON!
    
    private var selectedView: AvatarView!
    
    override func layoutSubviews() {
        if kids != nil && selectedView == nil {
            var xPosition:CGFloat = 0
            var scrollViewContentSize:CGFloat=0;
            kidsScrollView.autoresizesSubviews = false
            var viewToSelect: AvatarView! = nil
            for kid in kids.array! {
                let view = UINib(nibName: "AvatarView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AvatarView
                
                view.kid = kid
                view.handleAvatarTap = {
                    kid in
                    if self.selectedView != nil {
                        if self.selectedView.kid!["id"].string! != view.kid!["id"].string! {
                            self.selectView(view)
                        }
                    } else {
                        self.selectView(view)
                    }
                }
                view.frame.origin.x = xPosition
                self.kidsScrollView.addSubview(view)
                let spacer:CGFloat = 10
                xPosition+=view.frame.size.width + spacer
                scrollViewContentSize+=view.frame.size.width + spacer
                self.kidsScrollView.contentSize = CGSize(width: scrollViewContentSize, height: view.frame.size.height)
                if viewToSelect == nil {
                    if selectedKid == nil {
                        selectedKid = kid
                        viewToSelect = view
                    } else if selectedKid!["id"].stringValue == kid["id"].stringValue {
                        viewToSelect = view
                    }
                }
            }
            if viewToSelect != nil {
                self.selectView(viewToSelect)
            }
        }
    }
    
    private func selectView(_ view: AvatarView) {
        if self.selectedView != nil {
            self.selectedView!.avatarSelected.accept(false)
        }
        self.selectedView = view
        self.selectedView!.avatarSelected.accept(true)
        if self.handleAvatarTap != nil {
            self.handleAvatarTap!(view.kid)
        }
    }
}
