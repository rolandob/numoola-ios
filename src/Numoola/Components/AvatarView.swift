//
//  AvatarView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/31/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class AvatarView: UIControl {
    
    var handleAvatarTap: ( (_ kid: JSON?) -> Void )?
    // controls
    @IBOutlet var mainView: UIView!
    @IBOutlet var avatarImage: UIImageView!
    @IBOutlet var avatarLabel: UILabel!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var widthConstraint: NSLayoutConstraint!
    // fields
    var kid: JSON!
    var disposeBag = DisposeBag()
    let havePhoto:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let avatarSelected:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        havePhoto.subscribe(onNext: { [self] havePhoto in
            guard let theme = UITheme.instance().current.value else { return }
            self.mainView.backgroundColor = self.havePhoto.value ? theme.getColor(with: AvatarStyles.BackgroundColor, for: .application) : (self.avatarSelected.value ? theme.getColor(with: AvatarStyles.BackgroundColor, for: .selected) : theme.getColor(with: AvatarStyles.BackgroundColor, for: .normal))
        }).disposed(by: disposeBag)
        
        avatarSelected.subscribe(onNext: { [self] avatarSelected in
            if avatarSelected {
                UIView.animate(withDuration: 0.5, animations: {
                    self.avatarImage.transform = CGAffineTransform(scaleX: -1, y: 1)
                }) {
                    completed in
                    self.setStyles()
                }
            } else {
                self.avatarImage.transform = .identity
                self.setStyles()
            }
        }).disposed(by: disposeBag)
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let _ = theme else { return }
            self.setStyles()
        }).disposed(by: disposeBag)
    }
    
    private func setStyles() {
        guard let theme = UITheme.instance().current.value else { return }
     
        if theme.getBool(with: AvatarStyles.CircledImage, for: .normal) == true {
            self.avatarImage.layer.cornerRadius = self.avatarImage.frame.height/2
        }
        
        self.avatarImage.backgroundColor = theme.getColor(with: AvatarStyles.ImageBackgroundColor, for: .normal)
        self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: AvatarStyles.CornerRadius, for: .normal)!)
        self.mainView.layer.borderColor = self.avatarSelected.value ? theme.getColor(with: AvatarStyles.BorderColor, for: .selected)!.cgColor : .none
        self.mainView.backgroundColor = self.havePhoto.value ? theme.getColor(with: AvatarStyles.BackgroundColor, for: .application) : (self.avatarSelected.value ? theme.getColor(with: AvatarStyles.BackgroundColor, for: .selected) : theme.getColor(with: AvatarStyles.BackgroundColor, for: .normal))
        self.avatarLabel.textColor = self.avatarSelected.value ? theme.getColor(with: AvatarStyles.TextColor, for: .selected) : theme.getColor(with: AvatarStyles.TextColor, for: .normal)
        self.avatarLabel.font = self.avatarSelected.value ? theme.getFont(with: AvatarStyles.TextFont, for: .selected) : theme.getFont(with: AvatarStyles.TextFont, for: .normal)
        self.mainView.layer.borderWidth = self.avatarSelected.value ? CGFloat(theme.getSize(with: AvatarStyles.BorderWidth, for: .selected)!) : CGFloat(theme.getSize(with: AvatarStyles.BorderWidth, for: .normal)!)
    }

    override func layoutSubviews() {
        setStyles()
        loadKidPhoto()
        if kid != nil {
            avatarLabel.text = kid["firstName"].string!
        }
    }
    
    private func loadKidPhoto() {
        if self.kid != nil && self.kid["photo"].exists() {
            Api.instance().downloadPhoto(for: self.kid, withField: "photo", completionHandler: {
                data, error in
                if let error = error as NSError? {
                    print(error.localizedDescription)
                } else {
                    DispatchQueue.main.async {
                        if let base64Data = data {
                            let decodedData = Data(base64Encoded: base64Data)
                            self.avatarImage.contentMode = .scaleAspectFit
                            self.avatarImage.image = UIImage(data: decodedData!)
                            self.avatarImage.layoutIfNeeded()
                            self.havePhoto.accept(true)
                        }
                    }
                }
            })
        }
    }
    
    @IBAction func handleTap(_ sender: Any) {
        if handleAvatarTap != nil {
            self.handleAvatarTap!(kid)
        }
    }
}
