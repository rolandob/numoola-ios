//
//  KidMissionsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/24/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class KidMissionsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var summaryView: UIView!
    weak var titleView: PageTitleView!
    // fields
    var disposeBag = DisposeBag()
    let kidSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "kid")
    let missions:BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    let filters:BehaviorRelay<FilterOptions?> = BehaviorRelay(value: FilterOptions(
        [
            FilterOptionGroup(
                in: 0,
                identifier: "status",
                label: "Status",
                items: [
                    FilterOption(identifier: "Any", label: "Any"),
                    FilterOption(identifier: "ToApprove", label: "Pending Approval"),
                    FilterOption(identifier: "Incompleted", label: "Incomplete", selected: true),
                    FilterOption(identifier: "Completed", label: "Completed")
                ]
            ),
            FilterOptionGroup(
                in: 1,
                identifier: "amount",
                label: "Money amount",
                items: [
                    FilterOption(identifier: "AnyAmount", label: "Any amount", selected: true),
                    FilterOption(identifier: "WithAmount", label: "With Money Amount"),
                    FilterOption(identifier: "WithoutAmount", label: "With No Money Amount")
                ]
            ),
            FilterOptionGroup(
                in: 2,
                identifier: "date",
                label: "Due Date",
                items: [
                    FilterOption(identifier: "AnyDate", label: "Any", selected: true),
                    FilterOption(identifier: "EQToday", label: "Today"),
                    FilterOption(identifier: "GTToday", label: "After Today"),
                    FilterOption(identifier: "LTToday", label: "Before Today"),
                    FilterOption(identifier: "EQWeek", label: "This Week")
                ]
            )
        ]))
}

extension KidMissionsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.tableView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:  #selector(getMissions), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        
        kidSubject.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] kid in
            guard let kid = kid else { return }
            
            if self.titleView != nil {
                self.titleView.removeFromSuperview()
            }
            self.titleView = UINib(nibName: "PageTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PageTitleView
            self.titleView.frame.size.width = self.summaryView.frame.size.width
            self.titleView.frame.size.height = self.summaryView.frame.size.height
            self.summaryView.addSubview(self.titleView)
            self.titleView.user.accept(kid)
            self.titleView.title.accept("\(kid["firstName"].stringValue)'s Missions")
            
            self.getMissions()
        }).disposed(by: disposeBag)
        
        // bind missions to table view
        missions.bind(to: tableView.rx.items(cellIdentifier: "MissionItem")) { (row, mission, cell) in
            if let cell = cell as? MissionTableViewCell {
                cell.missionView.mission.accept(mission)
            }
            }.disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // set default background when no missions are available
        missions.asObservable()
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [unowned self] missions in
                if missions.count == 0 {
                    let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
                    noDataLabel.text = "No missions available"
                    noDataLabel.textColor = ThemePrimaryColor
                    noDataLabel.textAlignment = NSTextAlignment.center
                    self.tableView.backgroundView = noDataLabel
                    self.tableView.separatorStyle = .none
                } else {
                    self.tableView.backgroundView = nil
                    self.tableView.separatorStyle = .singleLine
                }
            }).disposed(by: disposeBag)
        
        filters.subscribe(onNext: { [self] filters in
            guard let _ = filters else { return }
            self.getMissions()
        }).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    @objc private func getMissions() {
        let statusOption = filters.value!.getSelectedOption(inGroup: "status")
        
        var statusFilter:String?
        
        switch statusOption?.key {
        case "ToApprove":
            statusFilter = "status/value eq 'Ready'"
        case "Incompleted":
            statusFilter = "status/value eq 'Pending' or status/value eq 'NotCompleted'"
        case "Completed":
            statusFilter = "status/value eq 'Completed'"
        default:
            statusFilter = nil
        }
        
        let amountOption = filters.value!.getSelectedOption(inGroup: "amount")
        
        var amountFilter:String?
        
        switch amountOption?.key {
        case "WithAmount":
            amountFilter = "payout gt 0"
        case "WithoutAmount":
            amountFilter = "payout eq 0"
        default:
            amountFilter = nil
        }
        
        let dateOption = filters.value!.getSelectedOption(inGroup: "date")
        
        var dateFilter:String?
        
        let weekStartDateString = Date.getWeekDate(for: .Sunday)!.to(format: .Persistent)
        let weekEndDateString = Date.getWeekDate(for: .Saturday)!.to(format: .Persistent)
        let currentDateString = Date().to(format: .Persistent)
        
        switch dateOption?.key {
        case "EQToday":
            dateFilter = "startDate eq \(currentDateString!)"
        case "GTToday":
            dateFilter = "startDate gt \(currentDateString!)"
        case "LTToday":
            dateFilter = "startDate lt \(currentDateString!)"
        case "EQWeek":
            dateFilter = "startDate ge \(weekStartDateString!) and startDate le \(weekEndDateString!)"
        default:
            dateFilter = nil
        }
        
        var filter = statusFilter != nil ? "(\(statusFilter!))" : ""
        if amountFilter != nil {
            filter = filter.isEmpty ? "(\(amountFilter!))" : "\(filter) and (\(amountFilter!))"
        }
        if dateFilter != nil {
            filter = filter.isEmpty ? dateFilter! : "\(filter) and (\(dateFilter!))"
        }
        
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/kidChores",
            with: RequestOptions(
                select: "id,name,payout,startDate,recurrenceType/value as 'recurrence',status/value as 'status',status/description as 'statusDescription'",
                filter: filter
            ))
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: {[self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.missions.accept(json["value"].arrayValue)
                    }
                case .failure(let error):
                    Notification.showError(using: error)
                }
                self.tableView.refreshControl?.endRefreshing()
            }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FilterMissionsSegue" {
            if let controller = segue.destination as? FilterViewController {
                controller.delegate = self
            }
        }
    }
}

extension KidMissionsViewController: UITableViewDelegate {
    
    func contextualCompleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Complete", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let mission = self.missions.value[indexPath.row]
            do {
                let data = try JSON(parseJSON: "{}").rawData()
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/kidChores/\(mission["id"].stringValue)/complete", payload: data)
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success:
                            Notification.show(title: "Mission Completed", message: "Congratulations \(self.kidSubject.value!["firstName"].stringValue)! You have completed the mission \"\(mission["name"].stringValue)\"", type: .Success)
                            self.getMissions()
                            completionHandler(true)
                        case .failure(let error as NSError):
                            print(error.localizedDescription)
                            completionHandler(false)
                        }
                    }).disposed(by: self.disposeBag)
            } catch {
                completionHandler(false)
            }
        })
        action.backgroundColor = ThemeAccentColor1
        return action
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actions:[UIContextualAction] = []
        let mission = self.missions.value[indexPath.row]
        let status = mission["status"].stringValue
        switch status {
        case "Pending", "NotCompleted":
            actions.append(self.contextualCompleteAction(forRowAtIndexPath: indexPath))
        default:
            print("Unknow status")
        }
        let swipeConfig = UISwipeActionsConfiguration(actions: actions)
        swipeConfig.performsFirstActionWithFullSwipe = true
        return swipeConfig
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension KidMissionsViewController: FilterViewControllerDelegate {
    
    func filterViewControllerWillShow(_ controller: FilterViewController) {
        UIController.instance().hideTabBar()
    }
    
    func filterViewControllerDidCancel(_ controller: FilterViewController) {
        UIController.instance().showTabBar()
    }
    
    func filterViewController(_ controller: FilterViewController, didFilter filters: FilterOptions!) {
        UIController.instance().showTabBar()
        self.filters.accept(filters)
    }
    
    func filterViewControllerFiltersToShow(_ controller: FilterViewController) -> FilterOptions! {
        return filters.value!.copy() as? FilterOptions
    }
    
    func filterViewControllerTitle(_ controller: FilterViewController) -> String {
        return "Filter Missions"
    }
    
}
