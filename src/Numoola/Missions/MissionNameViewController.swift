//
//  MissionNameViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/10/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

protocol MissionNameViewControllerDelegate:class {
    func missionNameViewControllerDidCancel(_ controller: MissionNameViewController)
    func missionNameViewController(_ controller: MissionNameViewController, didFinishEditing missionName: String)
    func missionNameViewControllerNameForEdition(_ controller: MissionNameViewController) -> String!
}

class MissionNameViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var nameTextField: NMLTextField!
    @IBOutlet var missionTableView: UITableView!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    // delegate
    weak var delegate: MissionNameViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let names: BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
}

extension MissionNameViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        // bind done isEnabled
        nameTextField.valid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        // button bindings
        doneButton.rx.tap.subscribe(onNext: {[weak self] _ in
            self?.navigationController?.popViewController(animated: true)
            self?.delegate?.missionNameViewController(self!, didFinishEditing: (self?.nameTextField.value.value)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: {[weak self] _ in
            self?.navigationController?.popViewController(animated: true)
            self?.delegate?.missionNameViewControllerDidCancel(self!)
        }).disposed(by: disposeBag)
        // set name default value
        if let defaultName = delegate?.missionNameViewControllerNameForEdition(self) {
            if defaultName.isEmpty == false {
                nameTextField.setValue(defaultName)
            }
        }
        // bind names to table view
        names.bind(to: missionTableView.rx.items(cellIdentifier: "MissionName")) { (row, name, cell) in
            if let cell = cell as? MissionNameTableViewCell {
                cell.name.accept(name["name"].stringValue)
            }
            }.disposed(by: disposeBag)
        // on name selected fill up the text field
        Observable.zip(missionTableView.rx.itemSelected, missionTableView.rx.modelSelected(JSON.self)).bind { [unowned self] indexPath, name in
            self.nameTextField.setValue(name["name"].stringValue)
            self.missionTableView.deselectRow(at: indexPath, animated: true)
        }.disposed(by: disposeBag)
        // set default background when no names are available
        names.asObservable()
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [unowned self] names in
                if names.count == 0 {
                    let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.missionTableView.bounds.size.width, height: self.missionTableView.bounds.size.height))
                    noDataLabel.text = "No suggestions available"
                    noDataLabel.textColor = ThemePrimaryColor
                    noDataLabel.textAlignment = NSTextAlignment.center
                    self.missionTableView.backgroundView = noDataLabel
                } else {
                    self.missionTableView.backgroundView = nil
                }
        }).disposed(by: disposeBag)
        // get most used names using the api
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/missionNames"
            ).subscribe({event in
                if let result = event.element, let data = try? result.get() {
                    //self.names = data["value"].array
                    self.names.accept(data["value"].arrayValue)
                }
                }
            ).disposed(by: disposeBag)
    }
}
