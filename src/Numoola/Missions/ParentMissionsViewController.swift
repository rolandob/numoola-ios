//
//  ParentMissionsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/5/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import FontAwesome_swift
import RxSwift
import RxCocoa

class ParentMissionsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var kidsView: UIView!
    @IBOutlet var missionsTableView: UITableView!
    @IBOutlet var addMissionButton: NMLBarButtonItem!
    @IBOutlet var filterMissionButton: NMLBarButtonItem!
    @IBOutlet var noChildStackView: UIStackView!
    @IBOutlet var summaryView: UIView!
    weak var titleView: PageTitleView!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let kid:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let missions:BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    let filters:BehaviorRelay<FilterOptions?> = BehaviorRelay(value: FilterOptions(
        [
            FilterOptionGroup(
                in: 0,
                identifier: "status",
                label: "Status",
                items: [
                    FilterOption(identifier: "Any", label: "Any"),
                    FilterOption(identifier: "ToApprove", label: "Pending Approval"),
                    FilterOption(identifier: "Incompleted", label: "Incomplete", selected: true),
                    FilterOption(identifier: "Completed", label: "Completed")
                ]
            ),
            FilterOptionGroup(
                in: 1,
                identifier: "amount",
                label: "Money amount",
                items: [
                    FilterOption(identifier: "AnyAmount", label: "Any amount", selected: true),
                    FilterOption(identifier: "WithAmount", label: "With Money Amount"),
                    FilterOption(identifier: "WithoutAmount", label: "With No Money Amount")
                ]
            ),
            FilterOptionGroup(
                in: 2,
                identifier: "date",
                label: "Due Date",
                items: [
                    FilterOption(identifier: "AnyDate", label: "Any", selected: true),
                    FilterOption(identifier: "EQToday", label: "Today"),
                    FilterOption(identifier: "GTToday", label: "After Today"),
                    FilterOption(identifier: "LTToday", label: "Before Today"),
                    FilterOption(identifier: "EQWeek", label: "This Week")
                ]
            )
        ]))
    var avatarListView: AvatarListView!
}

extension ParentMissionsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.missionsTableView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:  #selector(getMissionsForCurrentUser), for: .valueChanged)
        self.missionsTableView.refreshControl = refreshControl
        
        parentSubject
            .subscribe(onNext:{ parent in
                guard let parent = parent else { return }
                if self.titleView != nil {
                    self.titleView.removeFromSuperview()
                }
                self.titleView = UINib(nibName: "PageTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PageTitleView
                self.titleView.frame.size.width = self.summaryView.frame.size.width
                self.titleView.frame.size.height = self.summaryView.frame.size.height
                self.summaryView.addSubview(self.titleView)
                
                if self.avatarListView != nil {
                    self.avatarListView.removeFromSuperview()
                }
                self.avatarListView = UINib(nibName: "AvatarListView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? AvatarListView
                self.avatarListView.frame.size.width = self.kidsView.frame.size.width
                self.avatarListView.frame.size.height = self.kidsView.frame.size.height
                
                self.avatarListView.handleAvatarTap = self.handleAvatarTap
                self.avatarListView.kids = parent["kids"]
                if self.kid.value != nil {
                    self.avatarListView.selectedKid = self.kid.value
                }
                self.kidsView.addSubview(self.avatarListView)
            }).disposed(by: disposeBag)
        
        kid.subscribe(onNext: { [self] kid in
            guard let kid = kid else { return }
            self.getMissions(for: kid)
            self.titleView.user.accept(kid)
            self.titleView.title.accept("\(kid["firstName"].stringValue)'s Missions")
        }).disposed(by: disposeBag)
        
        filters.subscribe(onNext: { [self] filters in
            guard let _ = filters, let kid = self.kid.value else { return }
            self.getMissions(for: kid)
        }).disposed(by: disposeBag)
        
        // bind missions to table view
        missions.bind(to: missionsTableView.rx.items(cellIdentifier: "MissionItem")) { (row, mission, cell) in
            if let cell = cell as? MissionTableViewCell {
                cell.missionView.mission.accept(mission)
            }
            }.disposed(by: disposeBag)
        
        missionsTableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // set default background when no missions are available
        missions.asObservable()
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [unowned self] missions in
                if missions.count == 0 {
                    let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.missionsTableView.bounds.size.width, height: self.missionsTableView.bounds.size.height))
                    noDataLabel.text = "No missions available"
                    noDataLabel.textColor = ThemePrimaryColor
                    noDataLabel.textAlignment = NSTextAlignment.center
                    self.missionsTableView.backgroundView = noDataLabel
                    self.missionsTableView.separatorStyle = .none
                } else {
                    self.missionsTableView.backgroundView = nil
                    self.missionsTableView.separatorStyle = .singleLine
                }
            }).disposed(by: disposeBag)
        
        let noChildren: Observable<Bool> = parentSubject.map { parent -> Bool in parent!["kids"].arrayValue.count == 0 }.share(replay: 1)
        
        noChildren.bind(to: kidsView.rx.isHidden).disposed(by: disposeBag)
        noChildren.bind(to: summaryView.rx.isHidden).disposed(by: disposeBag)
        noChildren.bind(to: missionsTableView.rx.isHidden).disposed(by: disposeBag)
        noChildren.map{ value in !value }.bind(to: addMissionButton.rx.isEnabled).disposed(by: disposeBag)
        noChildren.map{ value in !value }.bind(to: filterMissionButton.rx.isEnabled).disposed(by: disposeBag)
        noChildren.map{ value in !value }.bind(to: noChildStackView.rx.isHidden).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    private func handleAvatarTap(user: JSON!) -> Void {
        if (kid.value != nil && kid.value!["id"].stringValue != user!["id"].stringValue) || kid.value == nil {
            kid.accept(user)
        }
    }
    
    @objc func getMissionsForCurrentUser() {
        if let kid = kid.value {
            self.getMissions(for: kid)
        }
    }
    
    private func getMissions(for kid: JSON) {
        let statusOption = filters.value!.getSelectedOption(inGroup: "status")
        
        var statusFilter:String?
        
        switch statusOption?.key {
        case "ToApprove":
            statusFilter = "status/value eq 'Ready'"
        case "Incompleted":
            statusFilter = "status/value eq 'Pending' or status/value eq 'NotCompleted'"
        case "Completed":
            statusFilter = "status/value eq 'Completed'"
        default:
            statusFilter = nil
        }
        
        let amountOption = filters.value!.getSelectedOption(inGroup: "amount")
        
        var amountFilter:String?
        
        switch amountOption?.key {
        case "WithAmount":
            amountFilter = "payout gt 0"
        case "WithoutAmount":
            amountFilter = "payout eq 0"
        default:
            amountFilter = nil
        }
        
        let dateOption = filters.value!.getSelectedOption(inGroup: "date")
        
        var dateFilter:String?
        
        let weekStartDateString = Date.getWeekDate(for: .Sunday)!.to(format: .Persistent)
        let weekEndDateString = Date.getWeekDate(for: .Saturday)!.to(format: .Persistent)
        let currentDateString = Date().to(format: .Persistent)
        
        switch dateOption?.key {
        case "EQToday":
            dateFilter = "startDate eq \(currentDateString!)"
        case "GTToday":
            dateFilter = "startDate gt \(currentDateString!)"
        case "LTToday":
            dateFilter = "startDate lt \(currentDateString!)"
        case "EQWeek":
            dateFilter = "startDate ge \(weekStartDateString!) and startDate le \(weekEndDateString!)"
        default:
            dateFilter = nil
        }
        
        var filter = statusFilter != nil ? "(\(statusFilter!))" : ""
        if amountFilter != nil {
            filter = filter.isEmpty ? "(\(amountFilter!))" : "\(filter) and (\(amountFilter!))"
        }
        if dateFilter != nil {
            filter = filter.isEmpty ? dateFilter! : "\(filter) and (\(dateFilter!))"
        }
        
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/chores/\(kid["id"].stringValue)",
            with: RequestOptions(
                select: "id,name,payout,startDate,recurrenceType/value as 'recurrence',status/value as 'status',status/description as 'statusDescription'",
                filter: filter
            ))
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: {[self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.missions.accept(json["value"].arrayValue)
                    }
                case .failure(let error):
                    Notification.showError(using: error)
                }
                self.missionsTableView.refreshControl?.endRefreshing()
            }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FilterMissionsSegue" {
            if let controller = segue.destination as? FilterViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "AddMissionSegue" {
            if let controller = segue.destination as? AddMissionViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "AddChildSegue" {
            if let controller = segue.destination as? SignupChildViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentMissionsViewController: UITableViewDelegate {
    
    func contextualApproveAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Approve", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let mission = self.missions.value[indexPath.row]
            do {
            let data = try JSON(parseJSON: "{}").rawData()
            Api.instance().rxInvoke(using: "Put", to: "userAccount/self/chores/\(self.kid.value!["id"].stringValue)/\(mission["id"].stringValue)/approve", payload: data)
                .observeOn(ConcurrentMainScheduler.instance)
                .subscribe(onNext: { result in
                    switch result {
                    case .success:
                        Notification.show(title: "Mission Approved", message: "Mission \"\(mission["name"].stringValue)\" has been approved for \(self.kid.value!["firstName"].stringValue)", type: .Success)
                        self.getMissions(for: self.kid.value!)
                        completionHandler(true)
                    case .failure(let error as NSError):
                        print(error.localizedDescription)
                        completionHandler(false)
                    }
                }).disposed(by: self.disposeBag)
            } catch {
                completionHandler(false)
            }
        })
        action.backgroundColor = ThemeAccentColor1
        return action
    }
    func contextualCompleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Complete", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let mission = self.missions.value[indexPath.row]
            do {
                let data = try JSON(parseJSON: "{}").rawData()
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/chores/\(self.kid.value!["id"].stringValue)/\(mission["id"].stringValue)/completeAndApprove", payload: data)
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success:
                            Notification.show(title: "Mission Completed", message: "Mission \"\(mission["name"].stringValue)\" has been completed on behalf of \(self.kid.value!["firstName"].stringValue)", type: .Success)
                            self.getMissions(for: self.kid.value!)
                            completionHandler(true)
                        case .failure(let error as NSError):
                            print(error.localizedDescription)
                            completionHandler(false)
                        }
                    }).disposed(by: self.disposeBag)
            } catch {
                completionHandler(false)
            }
        })
        action.backgroundColor = ThemeAccentColor1
        return action
    }
    func contextualRejectAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Reject", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let mission = self.missions.value[indexPath.row]
            do {
                // TODO capture comments
                let data = try JSON(parseJSON: "{}").rawData()
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/chores/\(self.kid.value!["id"].stringValue)/\(mission["id"].stringValue)/incomplete", payload: data)
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success:
                            Notification.show(title: "Mission Rejected", message: "Mission \"\(mission["name"].stringValue)\" has been marked as rejected for \(self.kid.value!["firstName"].stringValue)", type: .Success)
                            self.getMissions(for: self.kid.value!)
                            completionHandler(true)
                        case .failure(let error as NSError):
                            print(error.localizedDescription)
                            completionHandler(false)
                        }
                    }).disposed(by: self.disposeBag)
            } catch {
                completionHandler(false)
            }
        })
        action.backgroundColor = ThemeErrorColor
        return action
    }
    func contextualDeleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive,title: "Delete", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let mission = self.missions.value[indexPath.row]
            Api.instance().rxInvoke(using: "Delete", to: "userAccount/self/chores/\(self.kid.value!["id"].stringValue)/\(mission["id"].stringValue)")
                .observeOn(ConcurrentMainScheduler.instance)
                .subscribe(onNext: { result in
                    switch result {
                    case .success:
                        Notification.show(title: "Mission Deleted", message: "Mission \"\(mission["name"].stringValue)\" has been deleted for \(self.kid.value!["firstName"].stringValue)", type: .Success)
                        completionHandler(true)
                    case .failure(let error as NSError):
                        print(error.localizedDescription)
                        completionHandler(false)
                    }
                }).disposed(by: self.disposeBag)
        })
        action.backgroundColor = ThemeErrorColor
        return action
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actions:[UIContextualAction] = []
        let mission = self.missions.value[indexPath.row]
        let status = mission["status"].stringValue
        switch status {
        case "Pending", "NotCompleted":
            actions.append(self.contextualDeleteAction(forRowAtIndexPath: indexPath))
            actions.append(self.contextualCompleteAction(forRowAtIndexPath: indexPath))
        case "Ready":
            actions.append(self.contextualRejectAction(forRowAtIndexPath: indexPath))
            actions.append(self.contextualApproveAction(forRowAtIndexPath: indexPath))
        default:
            print("Unknow status")
        }
        let swipeConfig = UISwipeActionsConfiguration(actions: actions)
        swipeConfig.performsFirstActionWithFullSwipe = false
        return swipeConfig
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension ParentMissionsViewController: FilterViewControllerDelegate {
    
    func filterViewControllerWillShow(_ controller: FilterViewController) {
        UIController.instance().hideTabBar()
    }
    
    func filterViewControllerDidCancel(_ controller: FilterViewController) {
        UIController.instance().showTabBar()
    }
    
    func filterViewController(_ controller: FilterViewController, didFilter filters: FilterOptions!) {
        UIController.instance().showTabBar()
        self.filters.accept(filters)
    }
    
    func filterViewControllerFiltersToShow(_ controller: FilterViewController) -> FilterOptions! {
        return filters.value!.copy() as? FilterOptions
    }
    
    func filterViewControllerTitle(_ controller: FilterViewController) -> String {
        return "Filter Missions"
    }
    
}

extension ParentMissionsViewController: AddMissionViewControllerDelegate {
    func addMissionViewControllerDidCancel(_ controller: AddMissionViewController) {
        UIController.instance().showTabBar()
    }
    
    func addMissionViewController(_ controller: AddMissionViewController, didFinishAdding mission: JSON) {
        UIController.instance().showTabBar()
        
        var missionRequests:[Observable<Result<JSON?, Error>>] = []
        
        let children = mission["children"].arrayValue
        for child in children {
            var missionInstance = JSON(parseJSON: "{}")
            missionInstance["$class"].string = "Chore"
            missionInstance["name"].string = mission["name"].string
            missionInstance["startDate"].string = Date.from(string: mission["startDate"].stringValue, format: .Persistent)!.toCurrentDateTime()!.to(format: .DateTime)
            missionInstance["payout"].double = Double(mission["payout"].stringValue)
            
            var kidInstance = JSON(parseJSON: "{}")
            kidInstance["$class"].string = "Kid"
            kidInstance["id"].string = child["id"].string
            kidInstance["loaded"].bool = false
            
            missionInstance["kid"].object = kidInstance
            missionInstance["recurrenceType"].string = mission["occurrence"].string
            missionInstance["status"].string = "Pending"
            do {
                try missionRequests.append(Api.instance().rxInvoke(using: "Post", to: "userAccount/self/chores/\(child["id"].stringValue)", payload: missionInstance.rawData()))
            }
            catch let error as NSError{
                print(error.localizedDescription)
            }
        }
        ModalLogo.showModal(with: "Creating...", in: Observable.zip(missionRequests))
            .subscribe({ result in
                self.getMissions(for: self.kid.value!)
                Notification.show(title: "Mission Created", message: "The mission \"\(mission["name"].stringValue)\" has been created for \(children.map({ child in child["firstName"].stringValue}).joined(separator: ","))", type: .Success)
            }).disposed(by: disposeBag)
    }
    
    func addMissionViewControllerWillShow(_ controller: AddMissionViewController) {
        UIController.instance().hideTabBar()
    }
}

extension ParentMissionsViewController: SignupChildViewControllerDelegate {
    func signupChildViewControllerDidCancel(_ controller: SignupChildViewController) {
        UIController.instance().showTabBar()
    }
    
    func signupChildViewController(_ controller: SignupChildViewController, didFinishAdding kid: JSON) {
        UIController.instance().showTabBar()
    }
    
    func signupChildViewControllerWillShow(_ controller: SignupChildViewController) {
        UIController.instance().hideTabBar()
    }
}
