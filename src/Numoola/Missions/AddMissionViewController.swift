//
//  AddMissionViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/1/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import FontAwesome_swift

protocol AddMissionViewControllerDelegate:class {
    func addMissionViewControllerDidCancel(_ controller: AddMissionViewController)
    func addMissionViewController(_ controller: AddMissionViewController, didFinishAdding mission: JSON)
    func addMissionViewControllerWillShow(_ controller: AddMissionViewController)
}

class AddMissionViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var nameTextField: NMLTextField!
    @IBOutlet var payoutTextField: NMLTextField!
    @IBOutlet var startDateTextField: NMLTextField!
    @IBOutlet var childrenTextField: NMLTextField!
    // delegate
    weak var delegate: AddMissionViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let startDate:BehaviorRelay<Date> = BehaviorRelay(value: Date())
    let occurrence:BehaviorRelay<LookupType> = BehaviorRelay(value: LookupType("OneTime", "One Time"))
    let childrenArray:BehaviorRelay<[SelectableItem]?> = BehaviorRelay(value:nil)
}

extension AddMissionViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.addMissionViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            let mission = JSON(dictionaryLiteral:
                ("name", self.nameTextField.value.value!),
                ("startDate", self.startDate.value.to(format: .Persistent)!),
                ("occurrence", self.occurrence.value.value),
                ("payout", self.payoutTextField.value.value!),
                ("children", self.childrenArray.value!.filter({ item in item.selected == true }).map({ item in item.item }))
            )
            self.navigationController?.popViewController(animated: true)
            self.delegate?.addMissionViewController(self, didFinishAdding: mission)
        }).disposed(by: disposeBag)
        
        startDateTextField.setValue("\(startDate.value.to(format: .DisplayLong)!) - \(occurrence.value.description)")
        
        childrenArray.subscribe(onNext: { [self] childrenItems in
            guard let childrenItems = childrenItems else { return }
            if childrenItems.filter({ $0.selected == true }).count > 0 {
                self.childrenTextField.setValue(childrenItems.filter({ $0.selected == true }).map({ $0.item["firstName"].stringValue }).joined(separator: ", "))
            } else {
                self.childrenTextField.setValue("")
            }
        }).disposed(by: disposeBag)
        
        // bind done isEnabled
        Observable.combineLatest(nameTextField.valid, payoutTextField.valid, childrenTextField.valid, startDateTextField.valid) { $0 && $1 && $2 && $3 }.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
        parentSubject
            .subscribe(onNext:{ [self] parent in
                guard let parent = parent else { return }
                var childItems:[SelectableItem] = []
                parent["kids"].arrayValue.forEach({json in
                    childItems.append(SelectableItem(with: json, isSelected: false))
                })
                self.childrenArray.accept(childItems)
            }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        delegate?.addMissionViewControllerWillShow(self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SetMissionNameSegue" {
            if let controller = segue.destination as? MissionNameViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "SetMissionStartDateSegue" {
            if let controller = segue.destination as? StartDateViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "SelectChildrenSegue" {
            if let controller = segue.destination as? SelectAvatarViewController {
                controller.delegate = self
            }
        }
    }
}

extension AddMissionViewController: MissionNameViewControllerDelegate {
    func missionNameViewControllerDidCancel(_ controller: MissionNameViewController) {}
    
    func missionNameViewController(_ controller: MissionNameViewController, didFinishEditing missionName: String) {
        self.nameTextField.setValue(missionName)
    }
    
    func missionNameViewControllerNameForEdition(_ controller: MissionNameViewController) -> String! {
        return self.nameTextField.value.value ?? ""
    }
}

extension AddMissionViewController: StartDateViewControllerDelegate {
    func startDateViewControllerDidCancel(_ controller: StartDateViewController) {}
    
    func startDateViewController(_ controller: StartDateViewController, didFinishEditing recurrentDate: RecurrentDate) {
        startDate.accept(recurrentDate.date)
        occurrence.accept(recurrentDate.occurrence)
        startDateTextField.setValue("\(recurrentDate.date.to(format: .DisplayLong)!) - \(recurrentDate.occurrence.description)")
    }
    
    func startDateViewControllerDateForEdition(_ controller: StartDateViewController) -> RecurrentDate {
        return RecurrentDate(startDate.value, occurrence.value, "Mission Start Date")
    }
}

extension AddMissionViewController: SelectAvatarViewControllerDelegate {
    func selectAvatarViewControllerDidCancel(_ controller: SelectAvatarViewController) {}
    
    func selectAvatarViewController(_ controller: SelectAvatarViewController, didFinishSelecting children: SelectableItemCollection) {
        childrenArray.accept(children.items)
    }
    
    func selectAvatarViewControllerItemsForView(_ controller: SelectAvatarViewController) -> SelectableItemCollection {
        return SelectableItemCollection(childrenArray.value!)
    }
}
