//
//  MissionNameTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/10/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MissionNameTableViewCell: UITableViewCell {
    // controls
    @IBOutlet var nameLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let name:BehaviorRelay<String?> = BehaviorRelay(value: "")
}

extension MissionNameTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // bindings
        name.subscribe(onNext: { [self] name in
            self.nameLabel.text = name
        }).disposed(by: disposeBag)
    }
}
