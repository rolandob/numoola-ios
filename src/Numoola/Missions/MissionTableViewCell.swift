//
//  MissionTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/7/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit

class MissionTableViewCell: UITableViewCell {
    // fields
    var missionView: MissionView!
}

extension MissionTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tintColor = ThemePrimaryColor
        // create avatar view
        self.missionView = UINib(nibName: "MissionView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? MissionView
        self.missionView.frame.size.width = self.contentView.frame.size.width
        self.missionView.frame.size.height = self.contentView.frame.size.height
        self.contentView.addSubview(self.missionView)
    }
}
