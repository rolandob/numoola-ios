//
//  SignupParentMainViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/12/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import BetterSegmentedControl
import SwiftyJSON
import AWSMobileClient

class SignupParentMainViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var control: BetterSegmentedControl!
    // fields
    var disposeBag = DisposeBag()
    let navigationSubject:BehaviorRelay<String> = BehaviorRelay(value: "signupParentPersonal")
    weak var wizardController:WizardPageViewController?
    var username:String = ""
    var password:String = ""
}

extension SignupParentMainViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        control.segments = LabelSegment.segments(withTitles: ["Personal", "Contact", "Account"], numberOfLines: 1, normalBackgroundColor: .clear, normalFont: UIFont.systemFont(ofSize: 15, weight: .light), normalTextColor: ThemeSecondaryColor, selectedBackgroundColor: ThemeSecondaryColor, selectedFont: UIFont.systemFont(ofSize: 16, weight: .bold), selectedTextColor: .white)
        
        control.options = [.cornerRadius(20),.bouncesOnChange(true), .alwaysAnnouncesValue(true), .backgroundColor(.clear),.indicatorViewBorderWidth(CGFloat(5)),.indicatorViewBackgroundColor(ThemeSecondaryColor)]
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        control.rx.controlEvent(.valueChanged).subscribe(onNext: { [self] _ in
            switch self.control.index {
            case 0:
                if self.navigationSubject.value != "signupParentPersonal" {
                    self.navigationSubject.accept("signupParentPersonal")
                }
            case 1:
                if self.navigationSubject.value != "signupParentContact" {
                    self.navigationSubject.accept("signupParentContact")
                }
            case 2:
                if self.navigationSubject.value != "signupParentAccount" {
                    self.navigationSubject.accept("signupParentAccount")
                }
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        navigationSubject.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [self] value in
            switch value {
            case "signupParentPersonal":
                self.control.setIndex(0, animated: true)
            case "signupParentContact":
                self.control.setIndex(1, animated: true)
            case "signupParentAccount":
                self.control.setIndex(2, animated: true)
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.handleFinish()
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signupParentPagesSegue" {
            if let controller = segue.destination as? WizardPageViewController {
                controller.customDelegate = self
                self.wizardController = controller
                self.wizardController?.handleFinish = self.handleFinish
            }
        } else if segue.identifier == "ConfirmUserSegue" {
            if let controller = segue.destination as? SignupConfirmViewController {
                controller.delegate = self
            }
        }
    }
    
    func handleFinish() {
        let data = self.wizardController?.getData()
        var isValid = true
        var allData = JSON(parseJSON: "{}")
        
        for pageData in data! {
//            if pageData?.identifier == "signupParentAccount" {
//                if pageData?.data!["password"].stringValue != pageData?.data!["confirm password"].stringValue {
//                    Notification.show(title: "Sign Up", message: "The passwords do not match, please correct them and try again", type: .Information)
//                    return
//                }
//            }
            if pageData?.valid == false {
                isValid = false
            } else {
                pageData?.data?.forEach({ (key, value) in
                    allData[key] = value
                })
            }
        }
        if isValid == false {
            Notification.show(title: "Sign Up", message: "There is some data validation errors, please review them and try again", type: .Information)
        } else {
            print("Signup...")
            ModalLogo.showModal(with: "Signing Up...", in: Observable<Any?>.create({observer in

                var attributes:[String:String] = [:]
                attributes.updateValue(allData["first name"].stringValue, forKey: "given_name")
                attributes.updateValue(allData["last name"].stringValue, forKey: "family_name")
                attributes.updateValue(allData["email"].stringValue, forKey: "email")
                attributes.updateValue(Date.from(string: allData["date of birth"].stringValue, format: .DisplayLong)!.to(format: .Persistent)!, forKey: "birthdate")
                attributes.updateValue("parent", forKey: "custom:user_type")
                attributes.updateValue("+1\(allData["phone number"].stringValue)", forKey: "phone_number")
                self.username = allData["user name"].stringValue
                self.password = allData["password"].stringValue
                AWSMobileClient.sharedInstance().signUp(username: self.username, password: self.password, userAttributes: attributes, validationData: [:], completionHandler: { (result, error) in
                    observer.on(.completed)
                    DispatchQueue.main.async {
                        if let error = error {
                                Notification.show(title: "Sign Up", message: Errors.getErrorMessage(for: error), type: .Error)
                        } else if let _ = result {
                                self.performSegue(withIdentifier: "ConfirmUserSegue", sender: nil)
                        }
                    }
                })
                return Disposables.create()
            })).subscribe({ result in }).disposed(by: self.disposeBag)
        }
    }
}

extension SignupParentMainViewController: WizardPageViewControllerDelegate {
    func wizardPageViewControllerDidFinishNavigating(_ controller: WizardPageViewController, to identifier: String, with index: Int, withController controllerInstance: UIViewController) {
    }
    
    func wizardPageViewControllerNavigationListener(_ controller: WizardPageViewController) -> BehaviorRelay<String> {
        return navigationSubject
    }
    
    func wizardPageViewControllerPageIdentifiers(_ controller: WizardPageViewController) -> [String] {
        return ["signupParentPersonal", "signupParentContact", "signupParentAccount"]
    }
}

extension SignupParentMainViewController: SignupConfirmViewControllerDelegate {
    func signupConfirmViewControllerDidCancel(_ controller: SignupConfirmViewController) {}
    
    func signupConfirmViewController(_ controller: SignupConfirmViewController, didConfirmUser username: String, withPassword password: String?) {}
    
    func signupConfirmViewControllerWillConfirmUserWithUsername() -> String? {
        return self.username
    }
    
    func signupConfirmViewControllerWillConfirmUserWithPassword() -> String? {
        return self.password
    }
}
