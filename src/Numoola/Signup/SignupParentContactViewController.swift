//
//  SignupParentContactViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/12/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import InputMask

class SignupParentContactViewController: PageViewController {
    // controls
    @IBOutlet weak var phoneListener: MaskedTextFieldDelegate!
}
