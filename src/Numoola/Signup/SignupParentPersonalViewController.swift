//
//  SignupParentPersonalViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/12/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit

class SignupParentPersonalViewController: PageViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DateOfBirthSegue" {
            if let controller = segue.destination as? SelectDateViewController {
                controller.delegate = self
            }
        }
    }
}

extension SignupParentPersonalViewController: SelectDateViewControllerDelegate {
    func selectDateViewControllerWillShow(_ controller: SelectDateViewController) {}
    
    func selectDateViewControllerDidCancel(_ controller: SelectDateViewController) {}
    
    func selectDateViewController(_ controller: SelectDateViewController, didFinishEditing date: DateModel) {
        if let dobTextField = fields.first(where: {field in
            field.name == "date of birth"
        }) {
            dobTextField.setValue(date.value.to(format: .DisplayLong))
        }
    }
    
    func selectDateViewControllerModelForEdition(_ controller: SelectDateViewController) -> DateModel {
        if let dobTextField = fields.first(where: {field in
            field.name == "date of birth"
        }) {
            if let dateStr = dobTextField.value.value {
                return DateModel(.date, Date.from(string: dateStr, format: .DisplayLong)!, "Select Date of Birth")
            }
        }
        return DateModel(.date, Date(), "Select Date of Birth")
    }
}
