//
//  SignupSelectViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/27/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class SignupSelectViewController: UIViewController {
    // controls
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    // fields
    var disposeBag = DisposeBag()
}

extension SignupSelectViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            print("Cancel signup select")
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ConfirmUserSegue" {
            if let controller = segue.destination as? SignupConfirmViewController {
                controller.delegate = self
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

extension SignupSelectViewController: SignupConfirmViewControllerDelegate {
    func signupConfirmViewControllerDidCancel(_ controller: SignupConfirmViewController) {}
    func signupConfirmViewController(_ controller: SignupConfirmViewController, didConfirmUser username: String, withPassword password: String?) {
        navigationController?.popViewController(animated: true)
    }
    func signupConfirmViewControllerWillConfirmUserWithUsername() -> String? {
        return nil
    }
    func signupConfirmViewControllerWillConfirmUserWithPassword() -> String? {
        return nil
    }
}
