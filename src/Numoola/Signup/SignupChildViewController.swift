//
//  SignupChildViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/20/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa
import FontAwesome_swift
import AWSMobileClient

protocol SignupChildViewControllerDelegate:class {
    func signupChildViewControllerDidCancel(_ controller: SignupChildViewController)
    func signupChildViewController(_ controller: SignupChildViewController, didFinishAdding kid: JSON)
    func signupChildViewControllerWillShow(_ controller: SignupChildViewController)
}

class SignupChildViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var repeatPasswordTextField: NMLTextField!
    @IBOutlet var passwordTextField: NMLTextField!
    @IBOutlet var userNameTextField: NMLTextField!
    @IBOutlet var firstNameTextField: NMLTextField!
    @IBOutlet var dobTextField: NMLTextField!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    // delegate
    weak var delegate: SignupChildViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let parentSubject: BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
}

extension SignupChildViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        firstNameTextField.value.subscribe(onNext: { [self] value in
            guard let value = value else { return }
            self.navigationItem.title = value.isEmpty ? "Sign Up" : "Sign Up \(value)?"
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.signupChildViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] _ in
                
                ModalLogo.showModal(with: "Signing Up...", in: Observable<Any?>.create({observer in
                    
                    var attributes:[String:String] = [:]
                    attributes.updateValue(self.firstNameTextField.value.value!, forKey: "given_name")
                    attributes.updateValue(self.parentSubject.value!["lastName"].stringValue, forKey: "family_name")
                    attributes.updateValue(self.parentSubject.value!["emailAddresses"].arrayValue[0]["value"].stringValue, forKey: "email")
                    attributes.updateValue(Date.from(string: self.dobTextField.value.value!, format: .DisplayLong)!.to(format: .Persistent)!, forKey: "birthdate")
                    attributes.updateValue("kid", forKey: "custom:user_type")
                    attributes.updateValue(self.parentSubject.value!["id"].stringValue, forKey: "custom:parent_id")
                    attributes.updateValue("+1\(self.parentSubject.value!["phone"]["number"].stringValue.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())", forKey: "phone_number")
                    
                    AWSMobileClient.sharedInstance().signUp(username: self.userNameTextField.value.value!, password: self.passwordTextField.value.value!, userAttributes: attributes, validationData: [:], completionHandler: { (result, error) in
                        if let error = error  {
                            DispatchQueue.main.async {
                                Notification.showError(using: error)
                                observer.on(.completed)
                            }
                        } else if let _ = result {
                            let parentUsername = AWSMobileClient.sharedInstance().username!
                            let parentPassword = KeyChain.loadPassword(service: "www.numoola.com", account: AWSMobileClient.sharedInstance().username!)
                            Api.instance().loginUser(self.userNameTextField.value.value!, self.passwordTextField.value.value!, UserFetchOptions)
                                .observeOn(ConcurrentMainScheduler.instance)
                                .subscribe(onNext: { [self] result in
                                    observer.on(.completed)
                                    switch result {
                                    case .success(let data):
                                        if let kidData = data {
                                            Api.instance().loginUser(parentUsername, parentPassword!, UserFetchOptions)
                                                .observeOn(ConcurrentMainScheduler.instance)
                                                .subscribe(onNext: { [self] parentResult in
                                                    switch parentResult {
                                                    case .success(let parentData):
                                                        if let parentJson = parentData {
                                                            DataHub.instance().publishData(for: "parent", with: parentJson)
                                                            self.navigationController?.popViewController(animated: true)
                                                            self.delegate?.signupChildViewController(self, didFinishAdding: kidData)
                                                        }
                                                    case .failure(let error):
                                                        Notification.showError(using: error)
                                                    }
                                                }).disposed(by: self.disposeBag)
                                        }
                                    case .failure(let error):
                                        Notification.showError(using: error)
                                    }
                                }).disposed(by: self.disposeBag)
                        }
                    })
                    return Disposables.create()
                })).subscribe({ result in }).disposed(by: self.disposeBag)
            }).disposed(by: disposeBag)
        
        let allValid: Observable<Bool> = Observable.combineLatest(firstNameTextField.valid, userNameTextField.valid, dobTextField.valid, passwordTextField.valid, repeatPasswordTextField.valid) { $0 && $1 && $2 && $3 && $4}
        allValid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DateOfBirthSegue" {
            if let controller = segue.destination as? SelectDateViewController {
                controller.delegate = self
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegate?.signupChildViewControllerWillShow(self)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    @IBAction func openPrivacyPolicy() {
        if let url = URL(string: "http://www.numoola.com/terms-of-use/") {
            UIApplication.shared.open(url)
        }
    }
}

extension SignupChildViewController: SelectDateViewControllerDelegate {
    func selectDateViewControllerWillShow(_ controller: SelectDateViewController) {}
    
    func selectDateViewControllerDidCancel(_ controller: SelectDateViewController) {}
    
    func selectDateViewController(_ controller: SelectDateViewController, didFinishEditing date: DateModel) {
        self.dobTextField.setValue(date.value.to(format: .DisplayLong))
    }
    
    func selectDateViewControllerModelForEdition(_ controller: SelectDateViewController) -> DateModel {
        return DateModel(.date, Date.from(string: self.dobTextField.value.value ?? Date().to(format: .DisplayLong)!, format: .DisplayLong)!, "Select Date of Birth")
    }
}
