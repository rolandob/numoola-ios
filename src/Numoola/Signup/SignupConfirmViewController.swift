//
//  SignupConfirmViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/27/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import AWSMobileClient

protocol SignupConfirmViewControllerDelegate:class {
    func signupConfirmViewControllerDidCancel(_ controller: SignupConfirmViewController)
    func signupConfirmViewController(_ controller: SignupConfirmViewController, didConfirmUser username: String, withPassword password: String?)
    func signupConfirmViewControllerWillConfirmUserWithUsername() -> String?
    func signupConfirmViewControllerWillConfirmUserWithPassword() -> String?
}

class SignupConfirmViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var userNameTextField: NMLTextField!
    @IBOutlet var codeTextField: NMLTextField!
    @IBOutlet var resendButton: NMLButton!
    // delegate
    weak var delegate: SignupConfirmViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    var defaultPassword: String?
}

extension SignupConfirmViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            if self.defaultPassword == nil {
                self.navigationController?.popViewController(animated: true)
            } else {
                if let controller = self.navigationController?.viewControllers.first(where: { viewController in
                    guard let _ = viewController as? SignupSelectViewController else { return false }
                    return true
                }) {
                    print(controller)
                    self.navigationController?.popToViewController(controller, animated: true)
                }
            }
            self.delegate?.signupConfirmViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            guard let username = self.userNameTextField.value.value, let code = self.codeTextField.value.value else { return }
            
            ModalLogo.showModal(with: "Confirming...", in: Observable<Any?>.create({observer in
                AWSMobileClient.sharedInstance().confirmSignUp(username: username, confirmationCode: code, completionHandler: { (result, error)  in
                    DispatchQueue.main.async {
                        observer.on(.completed)
                        if let error = error {
                            Notification.show(title: "Sign Up", message: Errors.getErrorMessage(for: error), type: .Error)
                        } else if let _ = result {
                            self.navigationController?.popToRootViewController(animated: true)
                            // publish user confirmed data
                            DataHub.instance().publishData(for: "user-confirmed-data", with: JSON(dictionaryLiteral: ("username", username),("password", self.defaultPassword ?? "")))
                            self.delegate?.signupConfirmViewController(self, didConfirmUser: username, withPassword: self.defaultPassword)
                        }
                    }
                })
                return Disposables.create()
            })).subscribe({ result in }).disposed(by: self.disposeBag)
        }).disposed(by: disposeBag)
        
        let isAllValid: Observable<Bool> = Observable.combineLatest(userNameTextField.valid, codeTextField.valid) { $0 && $1 }
        
        isAllValid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
        defaultPassword = delegate?.signupConfirmViewControllerWillConfirmUserWithPassword()
        if let username = delegate?.signupConfirmViewControllerWillConfirmUserWithUsername() {
            self.userNameTextField.setValue(username)
        }
        
        userNameTextField.valid.bind(to: resendButton.rx.isEnabled).disposed(by: disposeBag)
        
        resendButton.rx.tap.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] _ in
            AWSMobileClient.sharedInstance().resendSignUpCode(username: self.userNameTextField.value.value!, completionHandler: { (result, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
                    } else {
                        Notification.show(title: "Confirmation code sent", message: "A new confirmation code was sent to your phone, please enter it below to confirm your account", type: .Success)
                    }
                }
            })
        }).disposed(by: disposeBag)
    }
}
