//
//  PasswordValidator.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/10/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class PasswordValidator {
    let password:BehaviorRelay<String?> = BehaviorRelay(value: "")
    let repeatPassword:BehaviorRelay<String?> = BehaviorRelay(value: "")
    let minimumPasswordValid:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let numberPasswordValid:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let symbolPasswordValid:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let uppercasePasswordValid:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let lowercasePasswordValid:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    var disposeBag = DisposeBag()
    let isValid:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    
    init() {
        password.subscribe(onNext: { [self] value in
            guard let pass = value else { return }
            // validate length
            self.minimumPasswordValid.accept(pass.count >= 8)
            // validate numbers
            if let _ = pass.rangeOfCharacter(from: .decimalDigits) {
                self.numberPasswordValid.accept(true)
            } else {
                self.numberPasswordValid.accept(false)
            }
            // validate symbols
            if let _ = pass.rangeOfCharacter(from: CharacterSet(charactersIn: "^$*.[]{}()?-\"!@#%&/\\,><':;|_~`+=")) {
                self.symbolPasswordValid.accept(true)
            } else {
                self.symbolPasswordValid.accept(false)
            }
            // validate uppercase
            let capitalLetterRegEx  = ".*[A-Z]+.*"
            let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
            let capitalresult = texttest.evaluate(with: pass)
            self.uppercasePasswordValid.accept(capitalresult)
            // validate lower case
            let lowercaseLetterRegEx  = ".*[a-z]+.*"
            let lowercasetexttest = NSPredicate(format:"SELF MATCHES %@", lowercaseLetterRegEx)
            let lowercaseresult = lowercasetexttest.evaluate(with: pass)
            self.lowercasePasswordValid.accept(lowercaseresult)
        }).disposed(by: disposeBag)
        
        let isPasswordValid: Observable<Bool> = password.map{ value -> Bool in value != nil && value!.isEmpty == false }.share(replay: 1)
        let isConfirmPasswordValid: Observable<Bool> = repeatPassword.map{ value -> Bool in value != nil && value!.isEmpty == false }.share(replay: 1)
        let areBothPasswordsValid: Observable<Bool> = Observable.combineLatest(password, repeatPassword).map{ pass1, pass2 -> Bool in pass1 == pass2}
        let isPasswordValidationValid: Observable<Bool> = Observable.combineLatest(minimumPasswordValid, numberPasswordValid, symbolPasswordValid, uppercasePasswordValid, lowercasePasswordValid) { $0 && $1 && $2 && $3 && $4 }
        let isAllValid:Observable<Bool> = Observable.combineLatest(isPasswordValid, isConfirmPasswordValid, areBothPasswordsValid, isPasswordValidationValid) {$0 && $1 && $2 && $3}
        isAllValid.bind(to: isValid).disposed(by: disposeBag)
    }
}
