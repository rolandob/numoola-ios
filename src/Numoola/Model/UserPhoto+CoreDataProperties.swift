//
//  UserPhoto+CoreDataProperties.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/31/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//
//

import Foundation
import CoreData


extension UserPhoto {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserPhoto> {
        return NSFetchRequest<UserPhoto>(entityName: "UserPhoto")
    }

    @NSManaged public var key: String?
    @NSManaged public var content: String?

}
