//
//  RecurrentDate.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/11/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation

class RecurrentDate: NSCopying {
    
    var date: Date
    var occurrence: LookupType
    var title: String
    
    init(_ date: Date, _ occurrence: LookupType, _ title: String) {
        self.date = date
        self.occurrence = occurrence
        self.title = title
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return RecurrentDate(date, occurrence.copy() as! LookupType, title)
    }
}
