//
//  NumoolaSettings.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/3/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation

class NumoolaSettings {
    public static func get() -> Any? {
        if let path = Bundle.main.path(forResource: "nmlconfiguration", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            } catch {
                // handle error
            }
        }
        return nil
    }
}
