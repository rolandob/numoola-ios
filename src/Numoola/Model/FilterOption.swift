//
//  FilterOption.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/6/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation

class FilterOption: NSCopying {
    
    var key: String
    var title: String
    var selected: Bool
    
    init(identifier key: String, label title: String, selected sel: Bool = false) {
        self.key = key
        self.title = title
        self.selected = sel
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
       return FilterOption(identifier: self.key, label: self.title, selected: self.selected)
    }
}

class FilterOptionGroup: NSCopying {
    
    var section: Int
    var options: [FilterOption] = []
    var key: String
    var title: String
    
    init(in section: Int, identifier key: String, label title: String, items options: [FilterOption]) {
        self.section = section
        self.key = key
        self.title = title
        self.options = options
    }
    
    func getSelectedOption() -> FilterOption! {
        if let option = options.filter({optionInstance in
            return optionInstance.selected == true
        }).first {
            return option
        }
        return nil
    }
    
    func selectOption(with key: String) -> Void {
        for option in options {
            if option.key == key {
                option.selected = true
            } else {
                option.selected = false
            }
        }
    }
    
    func getOption(inPos position: Int) -> FilterOption! {
        if position < options.count {
            return options[position]
        }
        return nil
    }
    
    func getIndexForOption(withKey key: String) -> Int {
        var index:Int = 0
        for option in options {
            if option.key == key {
                return index
            }
            index = index + 1
        }
        return -1
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        var copyOptions:[FilterOption] = []
        for option in options {
            copyOptions.append(option.copy() as! FilterOption)
        }
        return FilterOptionGroup(in: self.section, identifier: self.key, label: self.title, items: copyOptions)
    }
}

class FilterOptions: NSCopying {
    
    var groups: [FilterOptionGroup]
    
    init(_ groups: [FilterOptionGroup]) {
        self.groups = groups
    }
    
    func getSelectedOption(inGroup group: String) -> FilterOption! {
        if let groupFound = groups.filter({groupInstance in
            return groupInstance.key == group
        }).first {
            return groupFound.getSelectedOption()
        }
        return nil
    }
    
    func getGroup(inSection section: Int) -> FilterOptionGroup! {
        return groups.filter({group in
            return group.section == section
        }).first
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        var copyGroups:[FilterOptionGroup] = []
        for group in groups {
            copyGroups.append(group.copy() as! FilterOptionGroup)
        }
        return FilterOptions(copyGroups)
    }
}
