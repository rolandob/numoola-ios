//
//  DataMessage.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/28/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import SwiftyJSON

class DataMessage: NSCopying {
    
    var key: String
    var data: JSON!
    
    init(with data: JSON!, for key: String? = nil) {
        self.data = data
        self.key = key != nil ? key! : ""
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return DataMessage(with: data, for: key)
    }
    
}
