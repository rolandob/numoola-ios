//
//  Validators.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/18/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class ValidationResult:NSObject {
    var valid:Bool
    var message:String
    
    init(_ valid:Bool, _ message:String) {
        self.valid = valid
        self.message = message
    }
}

@objc protocol FieldValidator:class {
    func validate(_ name: String, _ text: String) -> ValidationResult
    
    
}

public class BankRoutingNumberValidator: NSObject, FieldValidator {
    
    @objc func validate(_ name: String, _ value: String) -> ValidationResult {
        if value.count != 9 {
            return ValidationResult(false, "The \(name) must be 9 digits")
        } else {
            if CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: value)) {
                let sum = (value[value.index(value.startIndex, offsetBy: 0)].wholeNumberValue!) * 3 + (value[value.index(value.startIndex, offsetBy: 1)].wholeNumberValue!) * 7 + (value[value.index(value.startIndex, offsetBy: 2)].wholeNumberValue!) + (value[value.index(value.startIndex, offsetBy: 3)].wholeNumberValue!) * 3 + (value[value.index(value.startIndex, offsetBy: 4)].wholeNumberValue!) * 7 + (value[value.index(value.startIndex, offsetBy: 5)].wholeNumberValue!) + (value[value.index(value.startIndex, offsetBy: 6)].wholeNumberValue!) * 3 + (value[value.index(value.startIndex, offsetBy: 7)].wholeNumberValue!) * 7 + (value[value.index(value.startIndex, offsetBy: 8)].wholeNumberValue!)
                print(sum)
                if sum % 10 != 0 {
                    return ValidationResult(false, "The \(name) does not conform to the bank routing number format")
                }
            } else {
                return ValidationResult(false, "The \(name) must contain only digits")
            }
        }
        return ValidationResult(true, "")
    }
}


public class PasswordMatchValidator: NSObject, FieldValidator {
    
    @IBOutlet var passwordField: NMLTextField!
    @IBOutlet var confirmPasswordField: NMLTextField!
    var setup:Bool = false
    var disposeBag = DisposeBag()
    
    func validate(_ name: String, _ text: String) -> ValidationResult {
        if setup == false {
            setup = true
            Observable.combineLatest(passwordField.value, confirmPasswordField.value).subscribe(onNext: { [self] _ in
                self.passwordField.validate()
                self.confirmPasswordField.validate()
            }).disposed(by: disposeBag)
        }
        if passwordField != nil {
            let passwordValue = passwordField.value.value ?? ""
            if passwordValue != text {
                return ValidationResult(false, "The passwords do not match")
            }
        }
        return ValidationResult(true, "")
    }
}
