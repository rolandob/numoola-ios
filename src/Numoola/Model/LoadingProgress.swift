//
//  LoadingProgress.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/4/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation

class LoadingProcess {
    
    let minValue: Float
    let maxValue: Float
    var currentValue: Float
    
    private let progressQueue = DispatchQueue(label: "ProgressView")
    private let semaphore = DispatchSemaphore(value: 1)
    
    init (minValue: Float, maxValue: Float) {
        self.minValue = minValue
        self.currentValue = minValue
        self.maxValue = maxValue
    }
    
    private func delay(stepDelayUsec: useconds_t, completion: @escaping ()->()) {
        usleep(stepDelayUsec)
        DispatchQueue.main.async {
            completion()
        }
    }
    
    func simulateLoading(toValue: Float, step: Float = 1, stepDelayUsec: useconds_t? = 10_000,
                         valueChanged: @escaping (_ currentValue: Float)->(),
                         completion: ((_ currentValue: Float)->())? = nil) {
        
        semaphore.wait()
        progressQueue.sync {
            if currentValue <= toValue && currentValue <= maxValue {
                usleep(stepDelayUsec!)
                DispatchQueue.main.async {
                    valueChanged(self.currentValue)
                    self.currentValue += step
                    self.semaphore.signal()
                    self.simulateLoading(toValue: toValue, step: step, stepDelayUsec: stepDelayUsec, valueChanged: valueChanged, completion: completion)
                }
                
            } else {
                self.semaphore.signal()
                completion?(currentValue)
            }
        }
    }
    
    func finish(step: Float = 1, stepDelayUsec: useconds_t? = 10_000,
                valueChanged: @escaping (_ currentValue: Float)->(),
                completion: ((_ currentValue: Float)->())? = nil) {
        simulateLoading(toValue: maxValue, step: step, stepDelayUsec: stepDelayUsec, valueChanged: valueChanged, completion: completion)
    }
}
