//
//  KeyChain.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/21/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//
import Foundation
import Security

class KeyChain {
    
    class func updatePassword(service: String, account: String, data: String) {
        guard let dataFromString = data.data(using: .utf8, allowLossyConversion: false) else {
            return
        }
        
        let status = SecItemUpdate(modifierQuery(service: service, account: account), [kSecValueData: dataFromString] as CFDictionary)
        
        checkError(status)
    }
    
    class func removePassword(service: String, account: String) {
        let status = SecItemDelete(modifierQuery(service: service, account: account))
        
        checkError(status)
    }
    
    class func savePassword(service: String, account: String, data: String) {
        guard let dataFromString = data.data(using: .utf8, allowLossyConversion: false) else {
            return
        }
        let password = loadPassword(service: service, account: account)
        if password != nil {
            print("Item found in keychain, updating...")
            let dataToUpdate: [CFString: Any] = [kSecValueData: dataFromString]
            let keychainQuery: [CFString: Any] = [kSecClass: kSecClassGenericPassword,
                                                  kSecAttrService: service,
                                                  kSecAttrAccount: account]
            let status = SecItemUpdate(keychainQuery as CFDictionary, dataToUpdate as CFDictionary)
            
            checkError(status)
        } else {
            print("Item not found in keychain, inserting...")
            let dataToInsert: [CFString: Any] = [kSecClass: kSecClassGenericPassword,
                                                  kSecAttrService: service,
                                                  kSecAttrAccount: account,
                                                  kSecValueData: dataFromString]
            let status = SecItemAdd(dataToInsert as CFDictionary, nil)
            checkError(status)
        }
    }
    
    class func loadPassword(service: String, account: String) -> String? {
        var dataTypeRef: CFTypeRef?
        
        let status = SecItemCopyMatching(modifierQuery(service: service, account: account), &dataTypeRef)
        
        if status == errSecSuccess,
            let retrievedData = dataTypeRef as? Data {
            return String(data: retrievedData, encoding: .utf8)
        } else {
            checkError(status)
            
            return nil
        }
    }
    
    fileprivate static func modifierQuery(service: String, account: String) -> CFDictionary {
        let keychainQuery: [CFString: Any] = [kSecClass: kSecClassGenericPassword,
                                              kSecAttrService: service,
                                              kSecAttrAccount: account,
                                              kSecReturnData: kCFBooleanTrue!]
        
        return keychainQuery as CFDictionary
    }
    
    fileprivate static func checkError(_ status: OSStatus) {
        if status != errSecSuccess {
            if #available(iOS 11.3, *),
                let err = SecCopyErrorMessageString(status, nil) {
                print("Operation failed: \(err)")
            } else {
                print("Operation failed: \(status). Check the error message through https://osstatus.com.")
            }
        }
    }
}
