//
//  Lookup.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/14/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation

class LookupType: NSCopying {
    
    var value: String
    var description: String
    
    init(_ value: String, _ description: String) {
        self.value = value
        self.description = description
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return LookupType(value, description)
    }
}
