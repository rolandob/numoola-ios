//
//  ViewControllerData.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/13/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class DataHub {
    
    private var behaviorRelayMap:[String:BehaviorRelay<JSON?>] = [:]
    
    static var _instance: DataHub = DataHub()
    
    public class func instance() -> DataHub {
        return _instance
    }
    
    public func getBehaviorRelay(for key: String) -> BehaviorRelay<JSON?> {
        
        if let entry = behaviorRelayMap.first(where: { valueKey, value in
            return key == valueKey
        }) {
            return entry.value
        }
        let behavior:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
        behaviorRelayMap.updateValue(behavior, forKey: key)
        return behavior
    }
    
    public func publishData(for key: String, with message: JSON) {
        if let entry = behaviorRelayMap.first(where: { valueKey, value in
            return key == valueKey
        }) {
            entry.value.accept(message)
        } else {
            let behavior:BehaviorRelay<JSON?> = BehaviorRelay(value: message)
            behaviorRelayMap.updateValue(behavior, forKey: key)
        }
    }
}
