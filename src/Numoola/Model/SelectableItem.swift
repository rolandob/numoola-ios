//
//  SelectableItem.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/10/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import SwiftyJSON

class SelectableItem: NSCopying {
    
    var item: JSON
    var selected: Bool
    
    init(with item: JSON, isSelected selected: Bool) {
        self.item = item
        self.selected = selected
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return SelectableItem(with: self.item, isSelected: self.selected)
    }
}

class SelectableItemCollection: NSCopying {
    
    var items: [SelectableItem]
    
    init(_ items: [SelectableItem]) {
        self.items = items
    }
    
    func setSelected(at index: Int, with value: Bool) {
        if index >= 0 && index < items.count {
            items[index].selected = value
        }
    }
    
    func getSelectedItemsJson() -> [JSON] {
        var itemArray:[JSON] = []
        items.filter({item in
            return item.selected == true
        }).forEach({item in
            itemArray.append(item.item)
        })
        return itemArray
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        var itemsArray: [SelectableItem] = []
        for item in items {
            itemsArray.append(item.copy() as! SelectableItem)
        }
        return SelectableItemCollection(itemsArray)
    }
    
}
