//
//  UIController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/21/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import RxSwift
import RxCocoa

class UIController {
    
    static var _instance: UIController = UIController()
    // fields
    let tabBarSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "TabBar")
    
    public class func instance() -> UIController {
        return _instance
    }
    
    func hideTabBar() {
        var tabBarConfig = JSON(parseJSON: "{}")
        tabBarConfig["hidden"].bool = true
        tabBarSubject.accept(tabBarConfig)
    }
    
    func showTabBar() {
        var tabBarConfig = JSON(parseJSON: "{}")
        tabBarConfig["hidden"].bool = false
        tabBarSubject.accept(tabBarConfig)
    }
}
