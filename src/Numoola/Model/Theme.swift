//
//  Theme.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/25/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class ThemeAttribute<T> {
    var state:UIControl.State
    var value:T
    
    init(_ value: T, _ state: UIControl.State) {
        self.value = value
        self.state = state
    }
}

public enum AvatarStyles: String {
    case BorderColor = "AvatarBorderColor", ImageBackgroundColor = "AvatarImageBackgroundColor", BackgroundColor = "AvatarBackgroundColor", TextColor = "AvatarTextColor", TextFont = "AvatarTextFont", BorderWidth = "AvatarBorderWidth", CornerRadius = "AvatarCornerRadius", CircledImage = "AvatarCircledImage"
}

public enum SegmentedControlStyles:String {
    case TintColor = "SegmentedControlTintColor", BackgroundColor = "SegmentedControlBackgroundColor", TitleFont = "SegmentedControlTitleFont"
}

public enum TextFieldStyles:String {
    case TextFont = "TextFieldTextFont", IconColor = "TextFieldIconColor", PlaceholderTextColor = "TextFieldPlaceholderTextColor", TextColor = "TextFieldNormalTextColor"
}

public enum BarButtonItemStyles:String {
    case TintColor = "BarButtonItemTintColor", Size = "BarButtonItemSize"
}

public enum TabBarStyles:String {
    case TintColor = "TabBarTintColor", UnselectedTintColor = "TabBarUnselectedTintColor"
}

public enum ViewStyles:String {
    case BackgroundImageName = "ViewBackgroundImageName", NavigationBarForegroundColor = "ViewNavigationBarForegroundColor", MainViewCornerRadius = "ViewMainViewCornerRadius"
}

public enum ButtonStyles:String {
    case BackgroundColor = "ButtonBackgroundColor", FontColor = "ButtonFontColor", CornerRadius = "ButtonCornerRadius", BorderColor = "ButtonBorderColor", BorderWith = "ButtonBorderWith", Font = "ButtonFont"
}

public enum PageTitleStyles:String {
    case BackgroundColor = "PageTitleBackgroundColor", CornerRadius = "PageTitleCornerRadius", TitleColor = "PageTitleTitleColor", AvatarBackgroundColor = "PageTitleAvatarBackgroundColor"
}

class Theme {
    var colors:[String:[ThemeAttribute<UIColor>]] = [:]
    var sizes:[String:[ThemeAttribute<Float>]] = [:]
    var fonts:[String:[ThemeAttribute<UIFont>]] = [:]
    var strings:[String:[ThemeAttribute<String>]] = [:]
    var booleans:[String:[ThemeAttribute<Bool>]] = [:]
    
    public func addColor(for colorKey: String, with themeColor: ThemeAttribute<UIColor>, using prefix: String = "") {
        // color attribute collection
        var colorCollection:[ThemeAttribute<UIColor>] = []
        let fullKey = "\(prefix)\(colorKey)"
        if let colorEntry = colors.first(where: { (key, value) -> Bool in key == fullKey }) {
            colorCollection = colorEntry.value
        }
        colorCollection.append(themeColor)
        colors.updateValue(colorCollection, forKey: fullKey)
    }
    
    public func getColor(with colorKey: String,for state: UIControl.State, using prefix: String = "") -> UIColor? {
        let fullKey = "\(prefix)\(colorKey)"
        if let colorEntry = colors.first(where: { (key, value) -> Bool in key == fullKey }) {
            let colorArray = colorEntry.value
            if let colorAttribute = colorArray.first(where: { attribute in attribute.state == state }) {
                return colorAttribute.value
            }
        } else if let colorEntry = colors.first(where: { (key, value) -> Bool in key == colorKey }) {
            let colorArray = colorEntry.value
            if let colorAttribute = colorArray.first(where: { attribute in attribute.state == state }) {
                return colorAttribute.value
            }
        }
        return nil
    }
    
    public func addFont(for fontKey: String, with themeFont: ThemeAttribute<UIFont>, using prefix: String = "") {
        // font attribute collection
        var fontCollection:[ThemeAttribute<UIFont>] = []
        let fullKey = "\(prefix)\(fontKey)"
        if let fontEntry = fonts.first(where: { (key, value) -> Bool in key == fullKey }) {
            fontCollection = fontEntry.value
        }
        fontCollection.append(themeFont)
        fonts.updateValue(fontCollection, forKey: fullKey)
    }
    
    public func getFont(with fontKey: String,for state: UIControl.State, using prefix: String = "") -> UIFont? {
        let fullKey = "\(prefix)\(fontKey)"
        if let fontEntry = fonts.first(where: { (key, value) -> Bool in key == fullKey }) {
            let fontArray = fontEntry.value
            if let fontAttribute = fontArray.first(where: { attribute in attribute.state == state }) {
                return fontAttribute.value
            }
        } else if let fontEntry = fonts.first(where: { (key, value) -> Bool in key == fontKey }) {
            let fontArray = fontEntry.value
            if let fontAttribute = fontArray.first(where: { attribute in attribute.state == state }) {
                return fontAttribute.value
            }
        }
        
        return nil
    }
    
    public func addSize(for sizeKey: String, with themeSize: ThemeAttribute<Float>, using prefix: String = "") {
        // size attribute collection
        var sizeCollection:[ThemeAttribute<Float>] = []
        let fullKey = "\(prefix)\(sizeKey)"
        if let sizeEntry = sizes.first(where: { (key, value) -> Bool in key == fullKey }) {
            sizeCollection = sizeEntry.value
        }
        sizeCollection.append(themeSize)
        sizes.updateValue(sizeCollection, forKey: fullKey)
    }
    
    public func getSize(with sizeKey: String,for state: UIControl.State, using prefix: String = "") -> Float? {
        let fullKey = "\(prefix)\(sizeKey)"
        if let sizeEntry = sizes.first(where: { (key, value) -> Bool in key == fullKey }) {
            let sizeArray = sizeEntry.value
            if let sizeAttribute = sizeArray.first(where: { attribute in attribute.state == state }) {
                return sizeAttribute.value
            }
        } else if let sizeEntry = sizes.first(where: { (key, value) -> Bool in key == sizeKey }) {
            let sizeArray = sizeEntry.value
            if let sizeAttribute = sizeArray.first(where: { attribute in attribute.state == state }) {
                return sizeAttribute.value
            }
        }
        return nil
    }
    
    public func addString(for stringKey: String, with themeString: ThemeAttribute<String>, using prefix: String = "") {
        // string attribute collection
        var stringCollection:[ThemeAttribute<String>] = []
        let fullKey = "\(prefix)\(stringKey)"
        if let stringEntry = strings.first(where: { (key, value) -> Bool in key == fullKey }) {
            stringCollection = stringEntry.value
        }
        stringCollection.append(themeString)
        strings.updateValue(stringCollection, forKey: fullKey)
    }
    
    public func getString(with stringKey: String,for state: UIControl.State, using prefix: String = "") -> String? {
        let fullKey = "\(prefix)\(stringKey)"
        if let stringEntry = strings.first(where: { (key, value) -> Bool in key == fullKey }) {
            let stringArray = stringEntry.value
            if let stringAttribute = stringArray.first(where: { attribute in attribute.state == state }) {
                return stringAttribute.value
            }
        } else if let stringEntry = strings.first(where: { (key, value) -> Bool in key == stringKey }) {
            let stringArray = stringEntry.value
            if let stringAttribute = stringArray.first(where: { attribute in attribute.state == state }) {
                return stringAttribute.value
            }
        }
        return nil
    }
    
    public func addBool(for boolKey: String, with themeString: ThemeAttribute<Bool>, using prefix: String = "") {
        // string attribute collection
        var boolCollection:[ThemeAttribute<Bool>] = []
        let fullKey = "\(prefix)\(boolKey)"
        if let boolEntry = booleans.first(where: { (key, value) -> Bool in key == fullKey }) {
            boolCollection = boolEntry.value
        }
        boolCollection.append(themeString)
        booleans.updateValue(boolCollection, forKey: fullKey)
    }
    
    public func getBool(with boolKey: String,for state: UIControl.State, using prefix: String = "") -> Bool? {
        let fullKey = "\(prefix)\(boolKey)"
        if let boolEntry = booleans.first(where: { (key, value) -> Bool in key == fullKey }) {
            let boolArray = boolEntry.value
            if let boolAttribute = boolArray.first(where: { attribute in attribute.state == state }) {
                return boolAttribute.value
            }
        } else if let boolEntry = booleans.first(where: { (key, value) -> Bool in key == boolKey }) {
            let boolArray = boolEntry.value
            if let boolAttribute = boolArray.first(where: { attribute in attribute.state == state }) {
                return boolAttribute.value
            }
        }
        return nil
    }
    
    public func getColor(with colorKey: AvatarStyles, for state: UIControl.State, using prefix: String = "") -> UIColor? {
        return self.getColor(with: colorKey.rawValue, for: state, using: prefix)
    }
    
    public func getFont(with fontKey: AvatarStyles, for state: UIControl.State, using prefix: String = "") -> UIFont? {
        return self.getFont(with: fontKey.rawValue, for: state, using: prefix)
    }
    
    public func getSize(with sizeKey: AvatarStyles, for state: UIControl.State, using prefix: String = "") -> Float? {
        return self.getSize(with: sizeKey.rawValue, for: state, using: prefix)
    }
    
    public func getString(with stringKey: AvatarStyles, for state: UIControl.State, using prefix: String = "") -> String? {
        return self.getString(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getBool(with stringKey: AvatarStyles, for state: UIControl.State, using prefix: String = "") -> Bool? {
        return self.getBool(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getColor(with colorKey: SegmentedControlStyles, for state: UIControl.State, using prefix: String = "") -> UIColor? {
        return self.getColor(with: colorKey.rawValue, for: state, using: prefix)
    }
    
    public func getFont(with fontKey: SegmentedControlStyles, for state: UIControl.State, using prefix: String = "") -> UIFont? {
        return self.getFont(with: fontKey.rawValue, for: state, using: prefix)
    }
    
    public func getSize(with sizeKey: SegmentedControlStyles, for state: UIControl.State, using prefix: String = "") -> Float? {
        return self.getSize(with: sizeKey.rawValue, for: state, using: prefix)
    }
    
    public func getString(with stringKey: SegmentedControlStyles, for state: UIControl.State, using prefix: String = "") -> String? {
        return self.getString(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getBool(with stringKey: SegmentedControlStyles, for state: UIControl.State, using prefix: String = "") -> Bool? {
        return self.getBool(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getColor(with colorKey: TextFieldStyles, for state: UIControl.State, using prefix: String = "") -> UIColor? {
        return self.getColor(with: colorKey.rawValue, for: state, using: prefix)
    }
    
    public func getFont(with fontKey: TextFieldStyles, for state: UIControl.State, using prefix: String = "") -> UIFont? {
        return self.getFont(with: fontKey.rawValue, for: state, using: prefix)
    }
    
    public func getSize(with sizeKey: TextFieldStyles, for state: UIControl.State, using prefix: String = "") -> Float? {
        return self.getSize(with: sizeKey.rawValue, for: state, using: prefix)
    }
    
    public func getString(with stringKey: TextFieldStyles, for state: UIControl.State, using prefix: String = "") -> String? {
        return self.getString(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getBool(with stringKey: TextFieldStyles, for state: UIControl.State, using prefix: String = "") -> Bool? {
        return self.getBool(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getColor(with colorKey: BarButtonItemStyles, for state: UIControl.State, using prefix: String = "") -> UIColor? {
        return self.getColor(with: colorKey.rawValue, for: state, using: prefix)
    }
    
    public func getFont(with fontKey: BarButtonItemStyles, for state: UIControl.State, using prefix: String = "") -> UIFont? {
        return self.getFont(with: fontKey.rawValue, for: state, using: prefix)
    }
    
    public func getSize(with sizeKey: BarButtonItemStyles, for state: UIControl.State, using prefix: String = "") -> Float? {
        return self.getSize(with: sizeKey.rawValue, for: state, using: prefix)
    }
    
    public func getString(with stringKey: BarButtonItemStyles, for state: UIControl.State, using prefix: String = "") -> String? {
        return self.getString(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getBool(with stringKey: BarButtonItemStyles, for state: UIControl.State, using prefix: String = "") -> Bool? {
        return self.getBool(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getColor(with colorKey: TabBarStyles, for state: UIControl.State, using prefix: String = "") -> UIColor? {
        return self.getColor(with: colorKey.rawValue, for: state, using: prefix)
    }
    
    public func getFont(with fontKey: TabBarStyles, for state: UIControl.State, using prefix: String = "") -> UIFont? {
        return self.getFont(with: fontKey.rawValue, for: state, using: prefix)
    }
    
    public func getSize(with sizeKey: TabBarStyles, for state: UIControl.State, using prefix: String = "") -> Float? {
        return self.getSize(with: sizeKey.rawValue, for: state, using: prefix)
    }
    
    public func getString(with stringKey: TabBarStyles, for state: UIControl.State, using prefix: String = "") -> String? {
        return self.getString(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getBool(with stringKey: TabBarStyles, for state: UIControl.State, using prefix: String = "") -> Bool? {
        return self.getBool(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getColor(with colorKey: ViewStyles, for state: UIControl.State, using prefix: String = "") -> UIColor? {
        return self.getColor(with: colorKey.rawValue, for: state, using: prefix)
    }
    
    public func getFont(with fontKey: ViewStyles, for state: UIControl.State, using prefix: String = "") -> UIFont? {
        return self.getFont(with: fontKey.rawValue, for: state, using: prefix)
    }
    
    public func getSize(with sizeKey: ViewStyles, for state: UIControl.State, using prefix: String = "") -> Float? {
        return self.getSize(with: sizeKey.rawValue, for: state, using: prefix)
    }
    
    public func getString(with stringKey: ViewStyles, for state: UIControl.State, using prefix: String = "") -> String? {
        return self.getString(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getBool(with stringKey: ViewStyles, for state: UIControl.State, using prefix: String = "") -> Bool? {
        return self.getBool(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getColor(with colorKey: ButtonStyles, for state: UIControl.State, using prefix: String = "") -> UIColor? {
        return self.getColor(with: colorKey.rawValue, for: state, using: prefix)
    }
    
    public func getFont(with fontKey: ButtonStyles, for state: UIControl.State, using prefix: String = "") -> UIFont? {
        return self.getFont(with: fontKey.rawValue, for: state, using: prefix)
    }
    
    public func getSize(with sizeKey: ButtonStyles, for state: UIControl.State, using prefix: String = "") -> Float? {
        return self.getSize(with: sizeKey.rawValue, for: state, using: prefix)
    }
    
    public func getString(with stringKey: ButtonStyles, for state: UIControl.State, using prefix: String = "") -> String? {
        return self.getString(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getBool(with stringKey: ButtonStyles, for state: UIControl.State, using prefix: String = "") -> Bool? {
        return self.getBool(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getColor(with colorKey: PageTitleStyles, for state: UIControl.State, using prefix: String = "") -> UIColor? {
        return self.getColor(with: colorKey.rawValue, for: state, using: prefix)
    }
    
    public func getFont(with fontKey: PageTitleStyles, for state: UIControl.State, using prefix: String = "") -> UIFont? {
        return self.getFont(with: fontKey.rawValue, for: state, using: prefix)
    }
    
    public func getSize(with sizeKey: PageTitleStyles, for state: UIControl.State, using prefix: String = "") -> Float? {
        return self.getSize(with: sizeKey.rawValue, for: state, using: prefix)
    }
    
    public func getString(with stringKey: PageTitleStyles, for state: UIControl.State, using prefix: String = "") -> String? {
        return self.getString(with: stringKey.rawValue, for: state, using: prefix)
    }
    
    public func getBool(with stringKey: PageTitleStyles, for state: UIControl.State, using prefix: String = "") -> Bool? {
        return self.getBool(with: stringKey.rawValue, for: state, using: prefix)
    }
}

class UITheme {
    static var _instance: UITheme = UITheme()
    var themes:[String:Theme] = [:]
    // current theme
    let current:BehaviorRelay<Theme?> = BehaviorRelay(value: nil)
    
    public class func instance() -> UITheme {
        return _instance
    }
    
    init() {
        let defaultTheme = Theme() // default-theme
        // set avatar styles
        defaultTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        defaultTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .selected))
        defaultTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .application))
        defaultTheme.addColor(for: AvatarStyles.ImageBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        defaultTheme.addColor(for: AvatarStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .selected))
        defaultTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .selected))
        defaultTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        defaultTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.boldSystemFont(ofSize: 14), .selected))
        defaultTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 12), .normal))
        defaultTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(0.0, .selected))
        defaultTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(0.0, .normal))
        defaultTheme.addSize(for: AvatarStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(10, .normal))
        defaultTheme.addBool(for: AvatarStyles.CircledImage.rawValue, with: ThemeAttribute<Bool>(true, .normal))
        // set segmented control styles
        defaultTheme.addColor(for: SegmentedControlStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        defaultTheme.addColor(for: SegmentedControlStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        defaultTheme.addFont(for: SegmentedControlStyles.TitleFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.boldSystemFont(ofSize: 15), .selected))
        defaultTheme.addColor(for: SegmentedControlStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "clear")
        defaultTheme.addColor(for: SegmentedControlStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal), using: "clear")
        // text field styles
        defaultTheme.addFont(for: TextFieldStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(ThemeTextFieldFont, .normal))
        defaultTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        defaultTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "highlight")
        defaultTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(.gray, .normal))
        defaultTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "highlight")
        defaultTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.black, .normal))
        defaultTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "highlight")
        // bar button item styles
        defaultTheme.addColor(for: BarButtonItemStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        defaultTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(20, .normal))
        defaultTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(40, .normal), using: "medium")
        // tab bar styles
        defaultTheme.addColor(for: TabBarStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        defaultTheme.addColor(for: TabBarStyles.UnselectedTintColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        // view styles
        defaultTheme.addString(for: ViewStyles.BackgroundImageName.rawValue, with: ThemeAttribute<String>("BlueClouds", .normal))
        defaultTheme.addColor(for: ViewStyles.NavigationBarForegroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        defaultTheme.addSize(for: ViewStyles.MainViewCornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        // button styles
        // default
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal))
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal))
        defaultTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(ThemeButtonTitleFont, .normal))
        // primary
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        // primary_rounded
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_rounded")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_rounded")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded")
        // primary_rounded_small
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_rounded_small")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_rounded_small")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded_small")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded_small")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded_small")
        defaultTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_rounded_small")
        // secondary
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        // secondary_rounded
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_rounded")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_rounded")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_rounded")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_rounded")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_rounded")
        // primary_reversed
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_reversed")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed")
        // primary_reversed_rounded
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_rounded")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_rounded")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed_rounded")
        // secondary_reversed
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_reversed")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_reversed")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_reversed")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed")
        // secondary_reversed_rounded
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed_rounded")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_reversed_rounded")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_reversed_rounded")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_reversed_rounded")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed_rounded")
        // primary_reversed_rounded_noborders_small
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded_noborders_small")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded_noborders_small")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_rounded_noborders_small")
        defaultTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_rounded_noborders_small")
        // primary_reversed_noborders_small
        defaultTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_noborders_small")
        defaultTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_noborders_small")
        defaultTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_noborders_small")
        defaultTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        defaultTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        defaultTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_noborders_small")
        // page title styles
        defaultTheme.addColor(for: PageTitleStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        defaultTheme.addSize(for: PageTitleStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        defaultTheme.addColor(for: PageTitleStyles.TitleColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        defaultTheme.addColor(for: PageTitleStyles.AvatarBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        
        let parentTheme = Theme() // parent-theme
        // set avatar styles
        parentTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        parentTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .selected))
        parentTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .application))
        parentTheme.addColor(for: AvatarStyles.ImageBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        parentTheme.addColor(for: AvatarStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .selected))
        parentTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .selected))
        parentTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        parentTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.boldSystemFont(ofSize: 14), .selected))
        parentTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 12), .normal))
        parentTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(0.0, .selected))
        parentTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(0.0, .normal))
        parentTheme.addSize(for: AvatarStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(10, .normal))
        parentTheme.addBool(for: AvatarStyles.CircledImage.rawValue, with: ThemeAttribute<Bool>(true, .normal))
        // set segmented control styles
        parentTheme.addColor(for: SegmentedControlStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        parentTheme.addColor(for: SegmentedControlStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        parentTheme.addFont(for: SegmentedControlStyles.TitleFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.boldSystemFont(ofSize: 15), .selected))
        parentTheme.addColor(for: SegmentedControlStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "clear")
        parentTheme.addColor(for: SegmentedControlStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal), using: "clear")
        // text field styles
        parentTheme.addFont(for: TextFieldStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(ThemeTextFieldFont, .normal))
        parentTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        parentTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "highlight")
        parentTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(.gray, .normal))
        parentTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "highlight")
        parentTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.black, .normal))
        parentTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "highlight")
        // bar button item styles
        parentTheme.addColor(for: BarButtonItemStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        parentTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(20, .normal))
        parentTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(40, .normal), using: "medium")
        // tab bar styles
        parentTheme.addColor(for: TabBarStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        parentTheme.addColor(for: TabBarStyles.UnselectedTintColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        // view styles
        parentTheme.addString(for: ViewStyles.BackgroundImageName.rawValue, with: ThemeAttribute<String>("BlueClouds", .normal))
        parentTheme.addColor(for: ViewStyles.NavigationBarForegroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        parentTheme.addSize(for: ViewStyles.MainViewCornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        // button styles
        // default
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal))
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal))
        // primary
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        // primary_rounded
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_rounded")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_rounded")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded")
        // primary_rounded_small
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_rounded_small")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_rounded_small")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded_small")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded_small")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded_small")
        parentTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_rounded_small")
        // secondary
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        // secondary_rounded
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_rounded")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_rounded")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_rounded")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_rounded")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_rounded")
        // primary_reversed
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_reversed")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed")
        // primary_reversed_rounded
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_rounded")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_rounded")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed_rounded")
        // secondary_reversed
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_reversed")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_reversed")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_reversed")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed")
        // secondary_reversed_rounded
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed_rounded")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_reversed_rounded")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "secondary_reversed_rounded")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_reversed_rounded")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed_rounded")
        // primary_reversed_rounded_noborders_small
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded_noborders_small")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded_noborders_small")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_rounded_noborders_small")
        parentTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_rounded_noborders_small")
        // primary_reversed_noborders_small
        parentTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_noborders_small")
        parentTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_noborders_small")
        parentTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "primary_reversed_noborders_small")
        parentTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        parentTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        parentTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_noborders_small")
        // page title styles
        parentTheme.addColor(for: PageTitleStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        parentTheme.addSize(for: PageTitleStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        parentTheme.addColor(for: PageTitleStyles.TitleColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        parentTheme.addColor(for: PageTitleStyles.AvatarBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        
        let youngTheme = Theme() // young-theme
        // set avatar styles
        youngTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        youngTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .selected))
        youngTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .application))
        youngTheme.addColor(for: AvatarStyles.ImageBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        youngTheme.addColor(for: AvatarStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .selected))
        youngTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .selected))
        youngTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        youngTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 13), .selected))
        youngTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 13), .normal))
        youngTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(1.0, .selected))
        youngTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(0.0, .normal))
        youngTheme.addSize(for: AvatarStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        youngTheme.addBool(for: AvatarStyles.CircledImage.rawValue, with: ThemeAttribute<Bool>(false, .normal))
        // set segmented control styles
        youngTheme.addColor(for: SegmentedControlStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        youngTheme.addColor(for: SegmentedControlStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        youngTheme.addFont(for: SegmentedControlStyles.TitleFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.boldSystemFont(ofSize: 15), .selected))
        // text field styles
        youngTheme.addFont(for: TextFieldStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(ThemeTextFieldFont, .normal))
        youngTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        youngTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "highlight")
        youngTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(.gray, .normal))
        youngTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(.gray, .normal), using: "highlight")
        youngTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.black, .normal))
        youngTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "highlight")
        // bar button item styles
        youngTheme.addColor(for: BarButtonItemStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        youngTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(20, .normal))
        youngTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(40, .normal), using: "medium")
        // tab bar styles
        youngTheme.addColor(for: TabBarStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        youngTheme.addColor(for: TabBarStyles.UnselectedTintColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        // view styles
        youngTheme.addString(for: ViewStyles.BackgroundImageName.rawValue, with: ThemeAttribute<String>("Space", .normal))
        youngTheme.addColor(for: ViewStyles.NavigationBarForegroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        youngTheme.addSize(for: ViewStyles.MainViewCornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        youngTheme.addString(for: ViewStyles.BackgroundImageName.rawValue, with: ThemeAttribute<String>("RocketSpace", .normal), using: "rewards")
        // button styles
        // default
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal))
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal))
        // primary
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        // primary_rounded
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded")
        // primary_rounded_small
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded_small")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded_small")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded_small")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded_small")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded_small")
        youngTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_rounded_small")
        // secondary
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        // secondary_rounded
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_rounded")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_rounded")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_rounded")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_rounded")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_rounded")
        // primary_reversed
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_reversed")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed")
        // primary_reversed_rounded
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed_rounded")
        // secondary_reversed
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_reversed")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed")
        // secondary_reversed_rounded
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed_rounded")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed_rounded")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed_rounded")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_reversed_rounded")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed_rounded")
        // primary_reversed_rounded_noborders_small
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded_noborders_small")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded_noborders_small")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_rounded_noborders_small")
        youngTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_rounded_noborders_small")
        // primary_reversed_noborders_small
        youngTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_noborders_small")
        youngTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_noborders_small")
        youngTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_noborders_small")
        youngTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        youngTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        youngTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_noborders_small")
        // page title styles
        youngTheme.addColor(for: PageTitleStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        youngTheme.addSize(for: PageTitleStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        youngTheme.addColor(for: PageTitleStyles.TitleColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        youngTheme.addColor(for: PageTitleStyles.AvatarBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        
        let teenTheme = Theme() // teen-theme
        // set avatar styles
        teenTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        teenTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .selected))
        teenTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .application))
        teenTheme.addColor(for: AvatarStyles.ImageBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        teenTheme.addColor(for: AvatarStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .selected))
        teenTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .selected))
        teenTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        teenTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 13), .selected))
        teenTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 13), .normal))
        teenTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(1.0, .selected))
        teenTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(0.0, .normal))
        teenTheme.addSize(for: AvatarStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        teenTheme.addBool(for: AvatarStyles.CircledImage.rawValue, with: ThemeAttribute<Bool>(false, .normal))
        // set segmented control styles
        teenTheme.addColor(for: SegmentedControlStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        teenTheme.addColor(for: SegmentedControlStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        teenTheme.addFont(for: SegmentedControlStyles.TitleFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.boldSystemFont(ofSize: 15), .selected))
        // text field styles
        teenTheme.addFont(for: TextFieldStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(ThemeTextFieldFont, .normal))
        teenTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        teenTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "highlight")
        teenTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(.gray, .normal))
        teenTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(.gray, .normal), using: "highlight")
        teenTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.black, .normal))
        teenTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "highlight")
        // bar button item styles
        teenTheme.addColor(for: BarButtonItemStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        teenTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(20, .normal))
        teenTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(40, .normal), using: "medium")
        // tab bar styles
        teenTheme.addColor(for: TabBarStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        teenTheme.addColor(for: TabBarStyles.UnselectedTintColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        // view styles
        teenTheme.addString(for: ViewStyles.BackgroundImageName.rawValue, with: ThemeAttribute<String>("Space", .normal))
        teenTheme.addColor(for: ViewStyles.NavigationBarForegroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        teenTheme.addSize(for: ViewStyles.MainViewCornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        teenTheme.addString(for: ViewStyles.BackgroundImageName.rawValue, with: ThemeAttribute<String>("RocketSpace", .normal), using: "rewards")
        // button styles
        // default
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal))
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal))
        // primary
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        // primary_rounded
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded")
        // primary_rounded_small
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded_small")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded_small")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded_small")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded_small")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded_small")
        teenTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_rounded_small")
        // secondary
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        // secondary_rounded
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_rounded")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_rounded")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_rounded")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_rounded")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_rounded")
        // primary_reversed
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_reversed")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed")
        // primary_reversed_rounded
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed_rounded")
        // secondary_reversed
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_reversed")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed")
        // secondary_reversed_rounded
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed_rounded")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed_rounded")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed_rounded")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_reversed_rounded")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed_rounded")
        // primary_reversed_rounded_noborders_small
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded_noborders_small")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded_noborders_small")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_rounded_noborders_small")
        teenTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_rounded_noborders_small")
        // primary_reversed_noborders_small
        teenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_noborders_small")
        teenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_noborders_small")
        teenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_noborders_small")
        teenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        teenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        teenTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_noborders_small")
        // page title styles
        teenTheme.addColor(for: PageTitleStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        teenTheme.addSize(for: PageTitleStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        teenTheme.addColor(for: PageTitleStyles.TitleColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        teenTheme.addColor(for: PageTitleStyles.AvatarBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        
        let tweenTheme = Theme() // tween-theme
        // set avatar styles
        tweenTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal))
        tweenTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .selected))
        tweenTheme.addColor(for: AvatarStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .application))
        tweenTheme.addColor(for: AvatarStyles.ImageBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        tweenTheme.addColor(for: AvatarStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .selected))
        tweenTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .selected))
        tweenTheme.addColor(for: AvatarStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        tweenTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 13), .selected))
        tweenTheme.addFont(for: AvatarStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 13), .normal))
        tweenTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(1.0, .selected))
        tweenTheme.addSize(for: AvatarStyles.BorderWidth.rawValue, with: ThemeAttribute<Float>(0.0, .normal))
        tweenTheme.addSize(for: AvatarStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        tweenTheme.addBool(for: AvatarStyles.CircledImage.rawValue, with: ThemeAttribute<Bool>(false, .normal))
        // set segmented control styles
        tweenTheme.addColor(for: SegmentedControlStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        tweenTheme.addColor(for: SegmentedControlStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        tweenTheme.addFont(for: SegmentedControlStyles.TitleFont.rawValue, with: ThemeAttribute<UIFont>(UIFont.boldSystemFont(ofSize: 15), .selected))
        // text field styles
        tweenTheme.addFont(for: TextFieldStyles.TextFont.rawValue, with: ThemeAttribute<UIFont>(ThemeTextFieldFont, .normal))
        tweenTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        tweenTheme.addColor(for: TextFieldStyles.IconColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "highlight")
        tweenTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(.gray, .normal))
        tweenTheme.addColor(for: TextFieldStyles.PlaceholderTextColor.rawValue, with: ThemeAttribute<UIColor>(.gray, .normal), using: "highlight")
        tweenTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.black, .normal))
        tweenTheme.addColor(for: TextFieldStyles.TextColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "highlight")
        // bar button item styles
        tweenTheme.addColor(for: BarButtonItemStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        tweenTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(20, .normal))
        tweenTheme.addSize(for: BarButtonItemStyles.Size.rawValue, with: ThemeAttribute<Float>(40, .normal), using: "medium")
        // tab bar styles
        tweenTheme.addColor(for: TabBarStyles.TintColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        tweenTheme.addColor(for: TabBarStyles.UnselectedTintColor.rawValue, with: ThemeAttribute<UIColor>(ThemeDisabledColor, .normal))
        // view styles
        tweenTheme.addString(for: ViewStyles.BackgroundImageName.rawValue, with: ThemeAttribute<String>("Space", .normal))
        tweenTheme.addColor(for: ViewStyles.NavigationBarForegroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        tweenTheme.addSize(for: ViewStyles.MainViewCornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        tweenTheme.addString(for: ViewStyles.BackgroundImageName.rawValue, with: ThemeAttribute<String>("RocketSpace", .normal), using: "rewards")
        // button styles
        // default
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal))
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal))
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal))
        // primary
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary")
        // primary_rounded
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded")
        // primary_rounded_small
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded_small")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_rounded_small")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_rounded_small")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_rounded_small")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_rounded_small")
        tweenTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_rounded_small")
        // secondary
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary")
        // secondary_rounded
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_rounded")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_rounded")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_rounded")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_rounded")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_rounded")
        // primary_reversed
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "primary_reversed")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed")
        // primary_reversed_rounded
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "primary_reversed_rounded")
        // secondary_reversed
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(0, .normal), using: "secondary_reversed")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed")
        // secondary_reversed_rounded
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "secondary_reversed_rounded")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed_rounded")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal), using: "secondary_reversed_rounded")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "secondary_reversed_rounded")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonBorderWidth), .normal), using: "secondary_reversed_rounded")
        // primary_reversed_rounded_noborders_small
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_rounded_noborders_small")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_rounded_noborders_small")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(ThemeButtonCornerRadius), .normal), using: "primary_reversed_rounded_noborders_small")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_rounded_noborders_small")
        tweenTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_rounded_noborders_small")
        // primary_reversed_noborders_small
        tweenTheme.addColor(for: ButtonStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.white, .normal), using: "primary_reversed_noborders_small")
        tweenTheme.addColor(for: ButtonStyles.BorderColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_noborders_small")
        tweenTheme.addColor(for: ButtonStyles.FontColor.rawValue, with: ThemeAttribute<UIColor>(ThemeSecondaryColor, .normal), using: "primary_reversed_noborders_small")
        tweenTheme.addSize(for: ButtonStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        tweenTheme.addSize(for: ButtonStyles.BorderWith.rawValue, with: ThemeAttribute<Float>(Float(0), .normal), using: "primary_reversed_noborders_small")
        tweenTheme.addFont(for: ButtonStyles.Font.rawValue, with: ThemeAttribute<UIFont>(UIFont.systemFont(ofSize: 15), .normal), using: "primary_reversed_noborders_small")
        // page title styles
        tweenTheme.addColor(for: PageTitleStyles.BackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        tweenTheme.addSize(for: PageTitleStyles.CornerRadius.rawValue, with: ThemeAttribute<Float>(20, .normal))
        tweenTheme.addColor(for: PageTitleStyles.TitleColor.rawValue, with: ThemeAttribute<UIColor>(ThemePrimaryColor, .normal))
        tweenTheme.addColor(for: PageTitleStyles.AvatarBackgroundColor.rawValue, with: ThemeAttribute<UIColor>(.clear, .normal))
        
        themes.updateValue(defaultTheme, forKey: "default-theme")
        themes.updateValue(parentTheme, forKey: "parent-theme")
        themes.updateValue(youngTheme, forKey: "young-theme")
        themes.updateValue(teenTheme, forKey: "teen-theme")
        themes.updateValue(tweenTheme, forKey: "tween-theme")
    }
    
    public func setCurrent(_ themeKey: String){
        if themes.keys.contains(themeKey) {
            current.accept(themes[themeKey])
        } else {
            current.accept(themes["default-theme"])
        }
    }
}
