//
//  Notification.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/17/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import SwiftEntryKit
import UIKit
import UserNotifications
import RxSwift
import RxCocoa

enum NotificationType:String {
    case Success = "success", Error = "error", Warning = "warning", Information = "information"
}

class Notification {
    static func show(title titleText: String, message messageText: String, type notificationType: NotificationType) {
        // Generate top floating entry and set some properties
        var attributes = EKAttributes.topFloat
        
        switch notificationType {
        case .Success:
            attributes.entryBackground = .color(color: ThemeSuccessColor)
        case .Error:
            attributes.entryBackground = .color(color: ThemeErrorColor)
        case .Warning:
            attributes.entryBackground = .color(color: ThemeWarningColor)
        default:
            attributes.entryBackground = .color(color: ThemeInfoColor)
        }
        
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
        attributes.statusBar = .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width), height: .intrinsic)
        attributes.displayDuration = 3
        let title = EKProperty.LabelContent(text: titleText, style: .init(font: UIFont.boldSystemFont(ofSize: 17), color: UIColor.black))
        let description = EKProperty.LabelContent(text: messageText, style: .init(font: UIFont.systemFont(ofSize: 15), color: UIColor.black))
        let image = EKProperty.ImageContent(image: UIImage(named: "Icon")!, size: CGSize(width: 35, height: 35), makesRound: true)
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    static func showError(with title: String = "Something went wrong", using error: Error) {
        show(title: title, message: Errors.getErrorMessage(for: error), type: .Error)
    }
    
    static func isPushNotificationsEnabled() -> Observable<Bool> {
        return Observable.create({ observer in
            UNUserNotificationCenter.current().getNotificationSettings { settings in
                print("Notification settings: \(settings)")
                guard settings.authorizationStatus == .authorized else {
                    observer.on(.next(false))
                    observer.on(.completed)
                    return
                }
                observer.on(.next(true))
                observer.on(.completed)
            }
            return Disposables.create()
        })
    }
    
    static func registerForRemoteNotifications() {
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    static func requestPushNotificationsPermission(in controller: UIViewController, withTitle title:String = "Push Notifications", withMessage message: String = "Do you want to receive push notifications from Numoola?") -> Observable<Bool> {
        
        return Observable.create({ observer in
            
            let okAction = UIAlertAction(title: "Ok",
                                         style: .default) { (action) in
                UNUserNotificationCenter.current()
                    .requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
                        print("Permission granted: \(granted)")
                        guard granted else {
                            observer.on(.next(false))
                            observer.on(.completed)
                            return
                        }
                        observer.on(.next(true))
                        observer.on(.completed)
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel",
                                             style: .cancel) { (action) in
                observer.on(.next(false))
                observer.on(.completed)
            }
            
            let alert = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle: .alert)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            controller.present(alert, animated: true, completion: nil)
            
            return Disposables.create()
        })
    }
}
