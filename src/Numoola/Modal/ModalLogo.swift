//
//  CustomAlert.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/23/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ModalLogo: UIView, Modal {
    var backgroundView = UIView()
    var dialogView = UIView()
    var iconView:UIImageView?
    
    convenience init(message:String) {
        self.init(frame: UIScreen.main.bounds)
        initialize(message: message)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getIconView() -> UIImageView? {
        return iconView
    }
    
    func initialize(message:String){
        dialogView.clipsToBounds = true
        
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        addSubview(backgroundView)
        
        iconView = UIImageView()
        let image = UIImage(named: "Icon")
        iconView!.frame.origin = CGPoint(x: 8, y: 8)
        iconView!.frame.size = CGSize(width: 100 + 8 , height: image!.size.height + 8)
        iconView!.image = image
        iconView!.layer.cornerRadius = 8
        iconView!.clipsToBounds = true

        iconView!.contentMode = .scaleAspectFit
        
        dialogView.addSubview(iconView!)
        
        let titleLabel = UILabel(frame: CGRect(x: 8, y: iconView!.frame.height + iconView!.frame.origin.y, width: iconView!.frame.size.width, height: 30))
        titleLabel.text = message
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        let fixedWidth = titleLabel.frame.size.width
        let newSize = titleLabel.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        titleLabel.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        dialogView.addSubview(titleLabel)
        
        dialogView.frame.origin = CGPoint(x: 0, y: frame.height)
        dialogView.frame.size = CGSize(width: iconView!.frame.size.width + 16, height: iconView!.frame.size.height + titleLabel.frame.size.height + 24)
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = 8
        addSubview(dialogView)
    }

    static func showModal<T>(with message: String, in observable: Observable<T>) -> Observable<T> {
        
        let modal = ModalLogo(message: message)
        
        DispatchQueue.main.async {
            modal.show(animated: true)
        }
        return Observable.create({internalObs in
            observable
                .observeOn(ConcurrentMainScheduler.instance)
                .subscribe(
                    onNext: { result in
                        internalObs.onNext(result)
                    },
                    onCompleted: {
                        modal.dismiss(animated: true)
                        internalObs.onCompleted()
                    }
                )
            }
        )
    }
    
    static func showModal(with message: String) -> ModalLogo {
        
        let modal = ModalLogo(message: message)
        
        DispatchQueue.main.async {
            modal.show(animated: true)
        }
        
        return modal
    }
}
