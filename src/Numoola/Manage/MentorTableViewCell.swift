//
//  MentorTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/24/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit

class MentorTableViewCell: UITableViewCell {
    // fields
    var mentorView: MentorView!
}

extension MentorTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tintColor = ThemePrimaryColor
        // create avatar view
        self.mentorView = UINib(nibName: "MentorView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? MentorView
        self.mentorView.frame.size.width = self.contentView.frame.size.width
        self.mentorView.frame.size.height = self.contentView.frame.size.height
        self.contentView.addSubview(self.mentorView)
    }
}
