//
//  InviteMentorViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/25/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

protocol InviteMentorViewControllerDelegate:class {
    func inviteMentorViewControllerDidCancel(_ controller: InviteMentorViewController)
    func inviteMentorViewControllerDidFinishAdding(_ controller: InviteMentorViewController)
    func inviteMentorViewControllerWillShow(_ controller: InviteMentorViewController)
}

class InviteMentorViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var firstNameTextField: NMLTextField!
    @IBOutlet var lastNameTextField: NMLTextField!
    @IBOutlet var relationshipTextField: NMLTextField!
    @IBOutlet var phoneTextField: NMLTextField!
    @IBOutlet var showGoalSwitch: UISwitch!
    @IBOutlet var childrenTextField: NMLTextField!
    @IBOutlet var selectExistingButton: NMLButton!
    @IBOutlet var existingButtonView: UIView!
    // delegate
    weak var delegate: InviteMentorViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let childrenArray:BehaviorRelay<[SelectableItem]?> = BehaviorRelay(value:nil)
    let relationship:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let existentMentors:BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
}

extension InviteMentorViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        showGoalSwitch.onTintColor = ThemeSecondaryColor
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.inviteMentorViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            
            var mentorRequests:[Observable<Result<JSON?, Error>>] = []
    
            for child in self.childrenArray.value! {
                if child.selected == true {
                    let childItem = child.item
                    let mentorInstance = JSON(dictionaryLiteral:
                          ("$class", "KidMentor"),
                          ("firstName", self.firstNameTextField.value.value!),
                          ("lastName", self.lastNameTextField.value.value!),
                          ("kidId", childItem["id"].stringValue),
                          ("showGoals", self.showGoalSwitch.isOn),
                          ("relationship", self.relationship.value!["value"]),
                          ("invitationMethod", "SMS"),
                          ("phone", self.phoneTextField.value.value!))
                    do {
                        try mentorRequests.append(Api.instance().rxInvoke(using: "Post", to: "userAccount/self/mentorInvitation", payload: mentorInstance.rawData()))
                    }
                    catch let error as NSError{
                        print(error.localizedDescription)
                    }
                }
            }
            ModalLogo.showModal(with: "Creating...", in: Observable.zip(mentorRequests))
                .subscribe({ result in
                    Notification.show(title: "Invitation Created", message: "The invitation to mentor \"\(self.childrenArray.value!.filter{ item in item.selected == true }.map({ child in child.item["firstName"].stringValue}).joined(separator: ","))\" was successfully sent to \"\(self.firstNameTextField.value.value!)\"", type: .Success)
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.inviteMentorViewControllerDidFinishAdding(self)
                }).disposed(by: self.disposeBag)
        }).disposed(by: disposeBag)
        
        childrenArray.subscribe(onNext: { [self] childrenItems in
            guard let childrenItems = childrenItems else { return }
            if childrenItems.filter({ $0.selected == true }).count > 0 {
                self.childrenTextField.setValue(childrenItems.filter({ $0.selected == true }).map({ $0.item["firstName"].stringValue }).joined(separator: ", "))
            } else {
                self.childrenTextField.setValue("")
            }
        }).disposed(by: disposeBag)
        
        relationship.subscribe(onNext: { [self] value in
            guard let value = value else { return }
            self.relationshipTextField.setValue(value["description"].stringValue)
        }).disposed(by: disposeBag)
        
        Observable.combineLatest(phoneTextField.valid, phoneTextField.value).subscribe(onNext: { [self] values in
            guard let value = values.1 else { return }
            let valid = values.0
            if valid == true {
                let payload = JSON(dictionaryLiteral: ("phone", value))
                do {
                    let data = try payload.rawData()
                    Api.instance().rxInvoke(using: "Post", to: "userAccount/self/kids/mentor", payload: data)
                        .observeOn(ConcurrentMainScheduler.instance)
                        .subscribe(onNext: { [self] result in
                            switch result {
                            case .success(let data):
                                if let json = data {
                                    var childItems:[SelectableItem] = []
                                    print("\(json["value"].arrayValue.count) children available")
                                    json["value"].arrayValue.forEach({json in
                                        childItems.append(SelectableItem(with: json, isSelected: false))
                                    })
                                    self.childrenArray.accept(childItems)
                                }
                            case .failure(let error):
                                Notification.showError(using: error)
                            }
                        }).disposed(by: self.disposeBag)
                } catch (let error) {
                    Notification.showError(using: error)
                }
            }
        }).disposed(by: disposeBag)
        
        // bind done isEnabled
        Observable.combineLatest(firstNameTextField.valid, lastNameTextField.valid, relationshipTextField.valid, phoneTextField.valid, childrenTextField.valid) { $0 && $1 && $2 && $3 && $4 }.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
        Api.instance().rxInvoke(using: "Get", to: "userAccount/self/mentors")
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.existentMentors.accept(json["value"].arrayValue)
                    }
                case .failure(let error):
                    Notification.showError(using: error)
                }
            }).disposed(by: disposeBag)
        // hide/show existing button selector based on existing mentors count
        existentMentors.map{ items in items.count == 0}.bind(to: existingButtonView.rx.isHidden).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        delegate?.inviteMentorViewControllerWillShow(self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectChildrenSegue" {
            if let controller = segue.destination as? SelectAvatarViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "SelectRelationshipSegue" {
            if let controller = segue.destination as? SelectLookupViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "SelectMentorSegue" {
            if let controller = segue.destination as? SelectItemViewController {
                controller.delegate = self
            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if self.phoneTextField.valid.value == false && identifier == "SelectChildrenSegue" {
            return false
        }
        return true
    }
}

extension InviteMentorViewController: SelectAvatarViewControllerDelegate {
    
    func selectAvatarViewControllerDidCancel(_ controller: SelectAvatarViewController) {}
    
    func selectAvatarViewController(_ controller: SelectAvatarViewController, didFinishSelecting children: SelectableItemCollection) {
        childrenArray.accept(children.items)
    }
    
    func selectAvatarViewControllerItemsForView(_ controller: SelectAvatarViewController) -> SelectableItemCollection {
        return SelectableItemCollection(childrenArray.value!)
    }
}

extension InviteMentorViewController: SelectLookupViewControllerDelegate {
    func selectLookupViewControllerDidCancel(_ controller: SelectLookupViewController) {}
    
    func selectLookupViewController(_ controller: SelectLookupViewController, didFinishSelecting item: JSON) {
        self.relationship.accept(item)
    }
    
    func selectLookupViewControllerDefaultType(_ controller: SelectLookupViewController) -> JSON? {
        return relationship.value
    }
    
    func selectLookupViewControllerLookupType(_ controller: SelectLookupViewController) -> String {
        return "FamilyRelationship"
    }
    
    func selectLookupViewControllerTitle(_ controller: SelectLookupViewController) -> String {
        return "Select family relationship"
    }
}

extension InviteMentorViewController: SelectItemViewControllerDelegate {
    func selectItemViewControllerGetValue(_ controller: SelectItemViewController, for item: JSON) -> String {
        return item["phone"].stringValue
    }
    
    func selectItemViewControllerGetDescription(_ controller: SelectItemViewController, for item: JSON) -> String {
        return "\(item["firstName"].stringValue) - \(item["phone"].stringValue)"
    }
    
    func selectItemViewControllerDidCancel(_ controller: SelectItemViewController) {}
    
    func selectItemViewController(_ controller: SelectItemViewController, didFinishSelecting item: JSON) {
        self.firstNameTextField.setValue(item["firstName"].stringValue)
        self.lastNameTextField.setValue(item["lastName"].stringValue)
        self.phoneTextField.setValue(item["phone"].stringValue)

    }
    
    func selectItemViewControllerDefault(_ controller: SelectItemViewController) -> JSON? {
        return nil
    }
    
    func selectItemViewControllerItems(_ controller: SelectItemViewController) -> [JSON] {
        return existentMentors.value
    }
    
    func selectItemViewControllerTitle(_ controller: SelectItemViewController) -> String {
        return "Select Mentor"
    }
}
