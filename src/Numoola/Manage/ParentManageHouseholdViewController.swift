//
//  ParentManageHouseholdViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/19/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa
import FontAwesome_swift

class ParentManageHouseholdViewController: UIViewController {
    // controls
    @IBOutlet var kidsView: UIView!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var titleMainLabel: UILabel!
    @IBOutlet var titleEditButton: FontAwesomeImageView!
    @IBOutlet var titleUsernameLabel: UILabel!
    @IBOutlet var noChildrenStackView: UIStackView!
    @IBOutlet var summaryView: UIView!
    @IBOutlet var allocationEditButton: FontAwesomeImageView!
    @IBOutlet var allowanceEditButton: FontAwesomeImageView!
    @IBOutlet var dateEditButton: FontAwesomeImageView!
    @IBOutlet var allowanceLabel: UILabel!
    @IBOutlet var allocationLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    var avatarListView: AvatarListView!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let kid:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let havePhoto:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let allowance:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let allocation:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
}

extension ParentManageHouseholdViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        summaryView.backgroundColor = .white
        summaryView.layer.cornerRadius = 20
        
        titleEditButton.imageColor = ThemePrimaryColor
        titleEditButton.awakeFromNib()
        
        allocationEditButton.imageColor = ThemePrimaryColor
        allocationEditButton.awakeFromNib()
        
        allowanceEditButton.imageColor = ThemePrimaryColor
        allowanceEditButton.awakeFromNib()
        
        dateEditButton.imageColor = ThemePrimaryColor
        dateEditButton.awakeFromNib()
        
        avatarImageView.backgroundColor = ThemePrimaryColor
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height/2
        
        parentSubject
            .subscribe(onNext:{ [self] parent in
                guard let parent = parent else { return }
                if self.avatarListView != nil {
                    self.avatarListView.removeFromSuperview()
                }
                self.avatarListView = UINib(nibName: "AvatarListView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? AvatarListView
                self.avatarListView.frame.size.width = self.kidsView.frame.size.width
                self.avatarListView.frame.size.height = self.kidsView.frame.size.height
                
                self.avatarListView.handleAvatarTap = self.handleAvatarTap
                self.avatarListView.kids = parent["kids"]
                if self.kid.value != nil {
                    self.avatarListView.selectedKid = self.kid.value
                }
                self.kidsView.addSubview(self.avatarListView)
            }).disposed(by: disposeBag)
        
        kid.subscribe(onNext: { [self] kid in
            guard let kid = kid else { return }
            self.setSummary(for: kid)
            self.setAvatarImage(for: kid)
            self.setDate(for: kid)
            self.setAllowance(for: kid)
            self.setAllocation(for: kid)
        }).disposed(by: disposeBag)
        
        havePhoto.subscribe(onNext: { [self] value in
            self.avatarImageView.backgroundColor = value ? .white : ThemePrimaryColor
        }).disposed(by: disposeBag)
        
        allocation.observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] allocation in
                guard let allocation = allocation else { return }
                self.allocationLabel.text = "Saving: \(allocation["saving"].intValue)% Spending: \(allocation["spending"].intValue)% Giving: \(allocation["giving"].intValue)%"
            }).disposed(by: disposeBag)
        
        allowance.observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] allowance in
            self.allowanceLabel.text = Allowance.getAllowanceLabels(for: allowance).label
        }).disposed(by: disposeBag)
        
        let noChildren: Observable<Bool> = parentSubject.map { parent -> Bool in parent!["kids"].arrayValue.count == 0 }.share(replay: 1)
        
        noChildren.bind(to: kidsView.rx.isHidden).disposed(by: disposeBag)
        noChildren.bind(to: summaryView.rx.isHidden).disposed(by: disposeBag)
        noChildren.map{ value in !value }.bind(to: noChildrenStackView.rx.isHidden).disposed(by: disposeBag)
    }
    
    private func setSummary(for kid: JSON){
        titleMainLabel.text = "\(kid["firstName"].stringValue)'s Overview"
        titleUsernameLabel.text = "@\(kid["loginUsername"].stringValue)"
    }
    private func setDate(for kid: JSON){
        dateLabel.text = "\(Date.from(string: kid["dateOfBirth"].stringValue, format: .Persistent)!.to(format: .DisplayLong)!)"
    }
    private func setAllocation(for kid: JSON){
        allocation.accept(kid["transactionAllocation"])
    }
    private func setAvatarImage(for kid: JSON) {
        if kid["photo"].exists() {
            Api.instance().downloadPhoto(for: kid, withField: "photo", completionHandler: {
                data, error in
                if let error = error as NSError? {
                    print(error.localizedDescription)
                    self.havePhoto.accept(false)
                } else {
                    DispatchQueue.main.async {
                        if let base64Data = data {
                            let decodedData = Data(base64Encoded: base64Data)
                            self.avatarImageView.image = UIImage(data: decodedData!)
                            self.havePhoto.accept(true)
                        }
                    }
                }
            })
        } else {
            avatarImageView.image = UIImage(named: "Avatar")
            self.havePhoto.accept(false)
        }
    }
    private func setAllowance(for kid: JSON) {
        // get allowance for selected kid
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/kids/\(kid["id"].string!)/allowance")
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.allowance.accept(json)
                    }
                case .failure(let error as NSError):
                    print(error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    private func handleAvatarTap(user: JSON!) -> Void {
        if (kid.value != nil && kid.value!["id"].stringValue != user!["id"].stringValue) || kid.value == nil {
            kid.accept(user)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AllowanceSegue" {
            if let controller = segue.destination as? AllowanceViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "AllocationSegue" {
            if let controller = segue.destination as? AllocationViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "DateOfBirthSegue" {
            if let controller = segue.destination as? SelectDateViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "AddChildSegue" {
            if let controller = segue.destination as? SignupChildViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "EditProfileSegue" {
            if let controller = segue.destination as? EditKidProfileViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentManageHouseholdViewController: AllowanceViewControllerDelegate {
    
    func allowanceViewControllerWillShow(_ controller: AllowanceViewController) {
        UIController.instance().hideTabBar()
    }
    
    func allowanceViewControllerDidCancel(_ controller: AllowanceViewController) {
        UIController.instance().showTabBar()
    }
    
    func allowanceViewController(_ controller: AllowanceViewController, didFinishEditing allowance: AllowanceModel) {
        UIController.instance().showTabBar()
        
        var payload = JSON(parseJSON: "{}")
        payload["amount"].double = Double(allowance.amount)
        payload["startDate"].string = allowance.startDate.to(format: .DateTime)
        payload["recurrenceType"].string = allowance.occurrence.value
        do {
            try Api.instance().rxInvoke(using: "Put", to: "userAccount/self/kids/\(self.kid.value!["id"].stringValue)/allowance", payload: payload.rawData())
                .observeOn(ConcurrentMainScheduler.instance)
                .subscribe(onNext: { [self] result in
                    switch result {
                    case .success(let data):
                        if let json = data {
                            self.allowance.accept(json)
                        }
                        Notification.show(title: "Allowance Updated", message: "The Allowance configuration for \"\(self.kid.value!["firstName"].stringValue)\" was updated successfully", type: .Success)
                    case .failure(let error as NSError):
                        if let errorMessage = error.userInfo["message"] as? String {
                            Notification.show(title: "Something went wrong", message: errorMessage, type: .Error)
                        }
                    }
                }).disposed(by: disposeBag)
        } catch(let error as NSError) {
            print(error.localizedDescription)
        }
    }
    
    func allowanceViewControllerAllowanceForEdition(_ controller: AllowanceViewController) -> AllowanceModel {
        if self.allowance.value != nil && self.allowance.value!["id"].exists() {
            return AllowanceModel(self.allowance.value!["amount"].stringValue, Date.from(string: self.allowance.value!["startDate"].stringValue, format: .DateTime)!, LookupType(self.allowance.value!["recurrenceType"]["value"].stringValue, self.allowance.value!["recurrenceType"]["description"].stringValue))
        }
        return AllowanceModel("0", Date(), LookupType("OneTime", "One Time"))
    }
}

extension ParentManageHouseholdViewController: AllocationViewControllerDelegate {
    func allocationViewControllerDidCancel(_ controller: AllocationViewController) {
        UIController.instance().showTabBar()
    }
    
    func allocationViewController(_ controller: AllocationViewController, didFinishEditing allocation: AllocationModel) {
        UIController.instance().showTabBar()
        
        var payload = JSON(parseJSON: "{}")
        payload["saving"].float = allocation.saving
        payload["spending"].float = allocation.spending
        payload["giving"].float = allocation.giving
        payload["id"].string = kid.value!["transactionAllocation"]["id"].string
        payload["$class"].string = "TransactionAllocation"
        do {
            try Api.instance().rxInvoke(using: "Put", to: "userAccount/self/kids/\(self.kid.value!["id"].stringValue)/allocation", payload: payload.rawData(), with: RequestOptions(fetch: "rewardsAccount"))
                .observeOn(ConcurrentMainScheduler.instance)
                .subscribe(onNext: { [self] result in
                    switch result {
                    case .success(let data):
                        if let json = data {
                            self.kid.accept(json)
                            var parent = self.parentSubject.value
                            let kidIndex = parent!["kids"].array?.firstIndex(where: { kid in kid["id"].stringValue == json["id"].stringValue })
                            parent!["kids"].arrayObject![kidIndex!] = json
                            DataHub.instance().publishData(for: "parent", with: parent!)
                        }
                        Notification.show(title: "Transaction Allocation Updated", message: "The transaction allocation for \"\(self.kid.value!["firstName"].stringValue)\" was updated successfully", type: .Success)
                    case .failure(let error as NSError):
                        if let errorMessage = error.userInfo["message"] as? String {
                            Notification.show(title: "Something went wrong", message: errorMessage, type: .Error)
                        }
                    }
                }).disposed(by: disposeBag)
        } catch(let error as NSError) {
            print(error.localizedDescription)
        }
    }
    
    func allocationViewControllerAllocationForEdition(_ controller: AllocationViewController) -> AllocationModel {
        return AllocationModel(kid.value!["transactionAllocation"]["spending"].floatValue,kid.value!["transactionAllocation"]["saving"].floatValue,kid.value!["transactionAllocation"]["giving"].floatValue)
    }
    
    func allocationViewControllerWillShow(_ controller: AllocationViewController) {
         UIController.instance().hideTabBar()
    }
}

extension ParentManageHouseholdViewController: SelectDateViewControllerDelegate {
    
    func selectDateViewControllerWillShow(_ controller: SelectDateViewController) {
        UIController.instance().hideTabBar()
    }
    
    func selectDateViewControllerDidCancel(_ controller: SelectDateViewController) {
        UIController.instance().showTabBar()
    }
    
    func selectDateViewController(_ controller: SelectDateViewController, didFinishEditing date: DateModel) {
        UIController.instance().showTabBar()
        
        Api.instance().rxInvoke(using: "Get", to: "userAccount/self/kids/\(self.kid.value!["id"].stringValue)")
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if var json = data {
                        json["dateOfBirth"].string = date.value.to(format: .Persistent)
                         do {
                        try Api.instance().rxInvoke(using: "Put", to: "userAccount/self/kids/\(self.kid.value!["id"].stringValue)", payload: json.rawData(), with: RequestOptions(fetch: "rewardsAccount"))
                            .observeOn(ConcurrentMainScheduler.instance)
                            .subscribe(onNext: { [self] result in
                                switch result {
                                case .success(let data):
                                    if let updatedKid = data {
                                        self.kid.accept(updatedKid)
                                        var parent = self.parentSubject.value
                                        let kidIndex = parent!["kids"].array?.firstIndex(where: { kid in kid["id"].stringValue == updatedKid["id"].stringValue })
                                        parent!["kids"].arrayObject![kidIndex!] = updatedKid
                                        DataHub.instance().publishData(for: "parent", with: parent!)
                                        Notification.show(title: "Date of Birth Updated", message: "The date of birth for \"\(self.kid.value!["firstName"].stringValue)\" was updated successfully", type: .Success)
                                    }
                                case .failure(let error as NSError):
                                    if let errorMessage = error.userInfo["message"] as? String {
                                        Notification.show(title: "Something went wrong", message: errorMessage, type: .Error)
                                    }
                                }
                            }).disposed(by: self.disposeBag)
                         } catch(let error as NSError) {
                            print(error.localizedDescription)
                        }
                    }
                case .failure(let error as NSError):
                    if let errorMessage = error.userInfo["message"] as? String {
                        Notification.show(title: "Something went wrong", message: errorMessage, type: .Error)
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    func selectDateViewControllerModelForEdition(_ controller: SelectDateViewController) -> DateModel {
        return DateModel(.date, Date.from(string: kid.value!["dateOfBirth"].stringValue, format: .Persistent)!, "Edit Date of Birth")
    }
}

extension ParentManageHouseholdViewController: SignupChildViewControllerDelegate {
    func signupChildViewControllerDidCancel(_ controller: SignupChildViewController) {
        UIController.instance().showTabBar()
    }
    
    func signupChildViewController(_ controller: SignupChildViewController, didFinishAdding kid: JSON) {
        UIController.instance().showTabBar()
    }
    
    func signupChildViewControllerWillShow(_ controller: SignupChildViewController) {
        UIController.instance().hideTabBar()
    }
}

extension ParentManageHouseholdViewController: EditKidProfileViewControllerDelegate {
    func editKidProfileViewControllerWillShow(_ controller: EditKidProfileViewController) {
        UIController.instance().hideTabBar()
    }
    
    func editKidProfileViewControllerDidCancel(_ controller: EditKidProfileViewController) {
        UIController.instance().showTabBar()
    }
    
    func editKidProfileViewController(_ controller: EditKidProfileViewController, didFinishEditing kid: JSON) {
        UIController.instance().showTabBar()
        self.kid.accept(kid)
        var parent = self.parentSubject.value
        let kidIndex = parent!["kids"].array?.firstIndex(where: { kidItem in kidItem["id"].stringValue == kid["id"].stringValue })
        parent!["kids"].arrayObject![kidIndex!] = kid
        DataHub.instance().publishData(for: "parent", with: parent!)
        Notification.show(title: "Profile Updated", message: "\(kid["firstName"])'s profile was updated successfully", type: .Success)
    }
    
    func editKidProfileViewControllerForEdition(_ controller: EditKidProfileViewController) -> JSON {
        return kid.value!
    }
}
