//
//  ParentManageMentorsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/19/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class ParentManageMentorsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var summaryView: UIView!
    weak var titleView: PageTitleView!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let mentors:BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    let filters:BehaviorRelay<FilterOptions?> = BehaviorRelay(value: FilterOptions(
        [
            FilterOptionGroup(
                in: 0,
                identifier: "status",
                label: "Status",
                items: [
                    FilterOption(identifier: "Any", label: "Any", selected: true),
                    FilterOption(identifier: "Invited", label: "Invited"),
                    FilterOption(identifier: "Accepted", label: "Accepted")
                ]
            )
        ]))
    var selectedMentor:JSON? = nil
}

extension ParentManageMentorsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.tableView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        parentSubject
            .subscribe(onNext:{ parent in
                guard let parent = parent else { return }
                if self.titleView != nil {
                    self.titleView.removeFromSuperview()
                }
                self.titleView = UINib(nibName: "PageTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PageTitleView
                self.titleView.frame.size.width = self.summaryView.frame.size.width
                self.titleView.frame.size.height = self.summaryView.frame.size.height
                self.summaryView.addSubview(self.titleView)
                self.titleView.user.accept(parent)
                self.titleView.title.accept("Your children's mentors")
            }).disposed(by: disposeBag)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:  #selector(getMentors), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        
        filters.subscribe(onNext: { [self] filters in
            guard let _ = filters else { return }
            self.getMentors()
        }).disposed(by: disposeBag)
        
        // bind mentors to table view
        mentors.bind(to: tableView.rx.items(cellIdentifier: "MentorItem")) { (row, mentor, cell) in
            if let cell = cell as? MentorTableViewCell {
                cell.mentorView.mentor.accept(mentor)
            }
            }.disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // set default background when no goals are available
        mentors.asObservable()
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [unowned self] mentors in
                if mentors.count == 0 {
                    let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
                    noDataLabel.text = "No mentors added"
                    noDataLabel.textColor = ThemePrimaryColor
                    noDataLabel.textAlignment = NSTextAlignment.center
                    self.tableView.backgroundView = noDataLabel
                    self.tableView.separatorStyle = .none
                } else {
                    self.tableView.backgroundView = nil
                    self.tableView.separatorStyle = .singleLine
                }
            }).disposed(by: disposeBag)
    }
    
    @objc public func getMentors() {
        let statusOption = filters.value!.getSelectedOption(inGroup: "status")
        
        var statusFilter:String?
        
        switch statusOption?.key {
        case "Accepted":
            statusFilter = "status/value eq 'Accepted'"
        case "Invited":
            statusFilter = "status/value eq 'Invited'"
        default:
            statusFilter = "status/value eq 'Invited' or status/value eq 'Accepted'"
        }
        
        let filter = statusFilter != nil ? "(\(statusFilter!))" : ""
        
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/mentorInvitation/parent",
            with: RequestOptions(
                select: "id,status/value as 'status',mentorFirstName,mentorLastName,relationship/value as 'relationship',relationship/description as 'relationshipDescription',kid/id as 'kidId',kid/firstName as 'kidFirstName', kid/lastName as 'kidLastName',mentor/id as 'mentorId',mentor/firstName as 'currentMentorFirstName',mentor/lastName as 'currentMentorLastName',expirationDate,kid/photo/id as 'kidPhotoId',kid/photo/$modifiedOn as 'kidPhotoModifiedOn',mentor/photo/id as 'mentorPhotoId',mentor/photo/$modifiedOn as 'mentorPhotoModifiedOn'",
                filter: filter
            ))
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: {[self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.mentors.accept(json["value"].arrayValue)
                    }
                case .failure(let error):
                    Notification.showError(using: error)
                }
                self.tableView.refreshControl?.endRefreshing()
            }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ChangeMentorRelationshipSegue" {
            if let controller = segue.destination as? ChangeMentorRelationshipViewController {
                controller.delegate = self
            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "ChangeMentorRelationshipSegue" && selectedMentor == nil {
            return false
        }
        return true
    }
}

extension ParentManageMentorsViewController: UITableViewDelegate {
    
    func contextualRemoveAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive,title: "Remove Relationship", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let mentor = self.mentors.value[indexPath.row]
            do {
                let data = try JSON(parseJSON: "{}").rawData()
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/mentorInvitation/\(mentor["id"].stringValue)/parent/remove", payload: data)
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success:
                            Notification.show(title: "Mentor relationship removed", message: "The mentor \"\(mentor["mentorFirstName"].stringValue)\" relationship with \"\(mentor["kidFirstName"].stringValue)\" was removed sucessfully", type: .Success)
                            self.getMentors()
                            completionHandler(true)
                        case .failure(let error):
                            completionHandler(false)
                            Notification.showError(using: error)
                        }
                    }).disposed(by: self.disposeBag)
            } catch (let error) {
                Notification.showError(using: error)
            }
        })
        action.backgroundColor = ThemeErrorColor
        return action
    }
    
    func contextualUpdateRelationshipAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Change Relationship", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let mentor = self.mentors.value[indexPath.row]
            self.selectedMentor = mentor
            self.performSegue(withIdentifier: "ChangeMentorRelationshipSegue", sender: nil)
            completionHandler(true)
        })
        action.backgroundColor = ThemeAccentColor5
        return action
    }
    
    func contextualCancelAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive,title: "Cancel Invitation", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let mentor = self.mentors.value[indexPath.row]
            do {
                let data = try JSON(parseJSON: "{}").rawData()
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/mentorInvitation/\(mentor["id"].stringValue)/cancel", payload: data)
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success:
                            Notification.show(title: "Mentor invitation cancelled", message: "The invitation to \"\(mentor["mentorFirstName"].stringValue)\" to mentor \"\(mentor["kidFirstName"].stringValue)\" was cancelled sucessfully", type: .Success)
                            self.getMentors()
                            completionHandler(true)
                        case .failure(let error):
                            completionHandler(false)
                            Notification.showError(using: error)
                        }
                    }).disposed(by: self.disposeBag)
            } catch (let error) {
                Notification.showError(using: error)
            }
        })
        action.backgroundColor = ThemeErrorColor
        return action
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actions:[UIContextualAction] = []
        let mentor = self.mentors.value[indexPath.row]
        let status = mentor["status"].stringValue
        switch status {
        case "Invited":
            actions.append(self.contextualCancelAction(forRowAtIndexPath: indexPath))
        case "Accepted":
            actions.append(self.contextualRemoveAction(forRowAtIndexPath: indexPath))
            actions.append(self.contextualUpdateRelationshipAction(forRowAtIndexPath: indexPath))
        default:
            print("Unknow status")
        }
        let swipeConfig = UISwipeActionsConfiguration(actions: actions)
        swipeConfig.performsFirstActionWithFullSwipe = false
        return swipeConfig
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension ParentManageMentorsViewController: FilterViewControllerDelegate {
    
    func filterViewControllerWillShow(_ controller: FilterViewController) {
        UIController.instance().hideTabBar()
    }
    
    func filterViewControllerDidCancel(_ controller: FilterViewController) {
        UIController.instance().showTabBar()
    }
    
    func filterViewController(_ controller: FilterViewController, didFilter filters: FilterOptions!) {
        UIController.instance().showTabBar()
        self.filters.accept(filters)
    }
    
    func filterViewControllerFiltersToShow(_ controller: FilterViewController) -> FilterOptions! {
        return filters.value!.copy() as? FilterOptions
    }
    
    func filterViewControllerTitle(_ controller: FilterViewController) -> String {
        return "Filter Mentors"
    }
}

extension ParentManageMentorsViewController: ChangeMentorRelationshipViewControllerDelegate {
    func changeMentorRelationshipViewControllerDidCancel(_ controller: ChangeMentorRelationshipViewController) {
        UIController.instance().showTabBar()
    }
    
    func changeMentorRelationshipViewController(_ controller: ChangeMentorRelationshipViewController, didUpdateMentorRelationshipWith mentor: JSON) {
        UIController.instance().showTabBar()
        self.getMentors()
    }
    
    func changeMentorRelationshipViewControllerWillShow(_ controller: ChangeMentorRelationshipViewController) {
        UIController.instance().hideTabBar()
    }
    
    func changeMentorRelationshipViewControllerMentorForEdition(_ controller: ChangeMentorRelationshipViewController) -> JSON {
        return selectedMentor!
    }
    
    func changeMentorRelationshipViewControllerIsParent(_ controller: ChangeMentorRelationshipViewController) -> Bool {
        return true
    }
}
