//
//  ParentManageViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/19/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa
import BetterSegmentedControl

class ParentManageViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var householdContainer: UIView!
    @IBOutlet var mentorsContainer: UIView!
    private var householdController:ParentManageHouseholdViewController!
    private var mentorsController:ParentManageMentorsViewController!
    @IBOutlet var navigationBar: BetterSegmentedControl!
    @IBOutlet var addChildButton: NMLBarButtonItem!
    @IBOutlet var inviteButton: NMLBarButtonItem!
    @IBOutlet var filterMentorButton: NMLBarButtonItem!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
}

extension ParentManageViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
        }).disposed(by: disposeBag)
        
        navigationBar.segments = LabelSegment.segments(withTitles: ["My Household", "Our Village"], numberOfLines: 1, normalBackgroundColor: .clear, normalFont: UIFont.systemFont(ofSize: 15, weight: .light), normalTextColor: .white, selectedBackgroundColor: ThemeSecondaryColor, selectedFont: UIFont.systemFont(ofSize: 16, weight: .bold), selectedTextColor: .white)
        
        navigationBar.options = [.cornerRadius(20),.bouncesOnChange(true), .alwaysAnnouncesValue(true), .backgroundColor(.clear),.indicatorViewBorderWidth(CGFloat(5)),.indicatorViewBackgroundColor(ThemeSecondaryColor)]
        
        inviteButton.hide()
        filterMentorButton.hide()
        navigationBar.rx.controlEvent(.valueChanged).observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] _ in
            switch self.navigationBar.index {
            case 0:
                if self.householdContainer.alpha == 0 {
                    self.addChildButton.show()
                    self.inviteButton.hide()
                    self.filterMentorButton.hide()
                    UIView.animate(withDuration: 0.5, animations: {
                        self.householdContainer.alpha = 0
                        self.householdContainer.alpha = 1
                        self.mentorsContainer.alpha = 1
                        self.mentorsContainer.alpha = 0
                    })
                }
            case 1:
                if self.mentorsContainer.alpha == 0 {
                    self.addChildButton.hide()
                    self.inviteButton.show()
                    self.filterMentorButton.show()
                    UIView.animate(withDuration: 0.5, animations: {
                        self.mentorsContainer.alpha = 0
                        self.mentorsContainer.alpha = 1
                        self.householdContainer.alpha = 1
                        self.householdContainer.alpha = 0
                    })
                    self.mentorsController.getMentors()
                }
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        parentSubject.subscribe(onNext:{ message in
            guard let message = message else { return }
            print("Welcome Manage, \(message["firstName"].string!)")
        }).disposed(by: disposeBag)
    }
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        if self.mentorsController != nil {
            self.mentorsController.getMentors()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ParentManageHouseholdSegue" {
            if let householdController = segue.destination as? ParentManageHouseholdViewController {
                self.householdController = householdController
            }
        } else if segue.identifier == "ParentManageMentorsSegue" {
            if let mentorsController = segue.destination as? ParentManageMentorsViewController {
                self.mentorsController = mentorsController
            }
        } else if segue.identifier == "SignUpChildSegue" {
            if let controller = segue.destination as? SignupChildViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "FilterMentorsSegue" {
            if let controller = segue.destination as? FilterViewController {
                controller.delegate = self.mentorsController
            }
        } else if segue.identifier == "InviteMentorSegue" {
            if let controller = segue.destination as? InviteMentorViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentManageViewController: SignupChildViewControllerDelegate {
    func signupChildViewControllerDidCancel(_ controller: SignupChildViewController) {
        UIController.instance().showTabBar()
    }
    
    func signupChildViewController(_ controller: SignupChildViewController, didFinishAdding kid: JSON) {
        UIController.instance().showTabBar()
    }
    
    func signupChildViewControllerWillShow(_ controller: SignupChildViewController) {
        UIController.instance().hideTabBar()
    }
}

extension ParentManageViewController: InviteMentorViewControllerDelegate {
    func inviteMentorViewControllerDidFinishAdding(_ controller: InviteMentorViewController) {
        UIController.instance().showTabBar()
        self.mentorsController.getMentors()
    }
    
    func inviteMentorViewControllerDidCancel(_ controller: InviteMentorViewController) {
        UIController.instance().showTabBar()
    }
    
    func inviteMentorViewControllerWillShow(_ controller: InviteMentorViewController) {
        UIController.instance().hideTabBar()
    }
}
