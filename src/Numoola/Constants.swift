//
//  Constants.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/20/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import UIKit

let ThemeButtonCornerRadius = 20
let ThemeButtonBorderWidth = 2
let ThemePrimaryColor = UIColor(hue: 186/360, saturation: 0.85, brightness: 0.85, alpha: 1.0)
let ThemeSecondaryColor = UIColor(hue: 186/360, saturation: 0.95, brightness: 0.53, alpha: 1.0)
let ThemeDisabledColor = UIColor.gray
let ThemeButtonTitleFont = UIFont.systemFont(ofSize: 18)
let ThemeTextFieldFont = UIFont.systemFont(ofSize: 17)
let ThemeTextFieldIconSize = CGSize(width: 20, height: 20)

let ThemeAccentColor1 = UIColor(hue: 151/360, saturation: 0.62, brightness: 0.64, alpha: 1.0)
let ThemeAccentColor2 = UIColor(hue: 46/360, saturation: 1, brightness: 0.60, alpha: 1.0)
let ThemeAccentColor3 = UIColor(hue: 335/360, saturation: 0.85, brightness: 0.65, alpha: 1.0)
let ThemeAccentColor4 = UIColor(hue: 264/360, saturation: 0.46, brightness: 0.49, alpha: 1.0)
let ThemeAccentColor5 = UIColor(hue: 186/360, saturation: 0.75, brightness: 0.55, alpha: 1.0)

let ThemeSuccessColor = UIColor(hue: 151/360, saturation: 0.62, brightness: 0.64, alpha: 1.0)
let ThemeErrorColor = UIColor(hue: 0/360, saturation: 0.65, brightness: 0.90, alpha: 1.0)
let ThemeWarningColor = UIColor(hue: 46/360, saturation: 1, brightness: 0.50, alpha: 1.0)
let ThemeInfoColor = UIColor(hue: 186/360, saturation: 0.75, brightness: 0.45, alpha: 1.0)


let FaceIDEnabled = "FaceIDEnabled"
let FaceIDUserName = "FaceIDUserName"
let UserFetchOptions = "emailAddresses,kids.rewardsAccount,rewardsAccount,kids.emailAddresses,trophies"
let TouchIDEnabled = "TouchIDEnabled"
let TouchIDUserName = "TouchIDUserName"
let OnBoardingSeen = "OnBoardingSeen"
