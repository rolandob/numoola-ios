//
//  AllowanceViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/1/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import FontAwesome_swift

class AllowanceModel {
    var amount: String
    var startDate: Date
    var occurrence: LookupType
    
    init(_ amount: String, _ startDate: Date, _ occurrence: LookupType) {
        self.amount = amount
        self.startDate = startDate
        self.occurrence = occurrence
    }
}

protocol AllowanceViewControllerDelegate: class {
    func allowanceViewControllerDidCancel(_ controller: AllowanceViewController)
    func allowanceViewController(_ controller: AllowanceViewController, didFinishEditing allowance: AllowanceModel)
    func allowanceViewControllerAllowanceForEdition(_ controller: AllowanceViewController) -> AllowanceModel
    func allowanceViewControllerWillShow(_ controller: AllowanceViewController)
}

class AllowanceViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var amountTextField: NMLTextField!
    @IBOutlet var startDateTextField: NMLTextField!
    // delegate
    weak var delegate: AllowanceViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let startDate:BehaviorRelay<Date> = BehaviorRelay(value: Date())
    let occurrence:BehaviorRelay<LookupType> = BehaviorRelay(value: LookupType("OneTime", "One Time"))
    let allowance:BehaviorRelay<AllowanceModel?> = BehaviorRelay(value: nil)
}

extension AllowanceViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.allowanceViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.allowanceViewController(self, didFinishEditing: AllowanceModel(self.amountTextField.value.value!, self.startDate.value, self.occurrence.value))
        }).disposed(by: disposeBag)
        // bind done isEnabled
        amountTextField.valid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        // set initial allowance to edit
        allowance.accept(delegate?.allowanceViewControllerAllowanceForEdition(self))
        
        allowance.subscribe(onNext: { [self] allowance in
            guard let allowance = allowance else { return }
            self.amountTextField.setValue(allowance.amount)
            self.startDate.accept(allowance.startDate)
            self.occurrence.accept(allowance.occurrence)
            self.startDateTextField.setValue("\(allowance.startDate.to(format: .DisplayLong)!) - \(allowance.occurrence.description)")
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SetStartDateSegue" {
            if let controller = segue.destination as? StartDateViewController {
                controller.delegate = self
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegate?.allowanceViewControllerWillShow(self)
    }
}

extension AllowanceViewController: StartDateViewControllerDelegate {
    func startDateViewControllerDidCancel(_ controller: StartDateViewController) {}
    
    func startDateViewController(_ controller: StartDateViewController, didFinishEditing recurrentDate: RecurrentDate) {
        startDate.accept(recurrentDate.date)
        occurrence.accept(recurrentDate.occurrence)
        startDateTextField.setValue("\(recurrentDate.date.to(format: .DisplayLong)!) - \(recurrentDate.occurrence.description)")
    }
    
    func startDateViewControllerDateForEdition(_ controller: StartDateViewController) -> RecurrentDate {
        return RecurrentDate(startDate.value, occurrence.value, "Allowance Start Date")
    }
}
