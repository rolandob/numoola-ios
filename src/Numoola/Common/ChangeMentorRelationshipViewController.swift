//
//  ChangeMentorRelationshipViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 8/13/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

protocol ChangeMentorRelationshipViewControllerDelegate:class {
    func changeMentorRelationshipViewControllerDidCancel(_ controller: ChangeMentorRelationshipViewController)
    func changeMentorRelationshipViewController(_ controller: ChangeMentorRelationshipViewController, didUpdateMentorRelationshipWith mentor: JSON)
    func changeMentorRelationshipViewControllerWillShow(_ controller: ChangeMentorRelationshipViewController)
    func changeMentorRelationshipViewControllerMentorForEdition(_ controller: ChangeMentorRelationshipViewController) -> JSON
    func changeMentorRelationshipViewControllerIsParent(_ controller: ChangeMentorRelationshipViewController) -> Bool
}

class ChangeMentorRelationshipViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var mentorViewWrapper: UIView!
    weak var mentorView: MentorView?
    @IBOutlet var relationshipTextField: NMLTextField!
    // delegate
    weak var delegate: ChangeMentorRelationshipViewControllerDelegate?
    // fields
    let disposeBag = DisposeBag()
    let mentor:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let relationship:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
}

extension ChangeMentorRelationshipViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.changeMentorRelationshipViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            do {
                let data = try JSON(dictionaryLiteral: ("relationship", self.relationship.value!["value"].stringValue)).rawData()
                let mentorParentPath = self.delegate?.changeMentorRelationshipViewControllerIsParent(self) == true ? "parent" : "mentor"
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/mentorInvitation/\(self.mentor.value!["id"].stringValue)/\(mentorParentPath)/relationship", payload: data)
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { result in
                        switch result {
                        case .success(let data):
                            if let json = data {
                                Notification.show(title: "Mentor relationship updated", message: "The relationship of mentor \"\(self.mentor.value!["currentMentorFirstName"].stringValue)\" with \"\(self.mentor.value!["kidFirstName"])\" was updated sucessfully", type: .Success)
                                self.navigationController?.popViewController(animated: true)
                                self.delegate?.changeMentorRelationshipViewController(self, didUpdateMentorRelationshipWith: json)
                            }
                        case .failure(let error):
                            Notification.showError(using: error)
                        }
                    }).disposed(by: self.disposeBag)
            } catch (let error) {
                Notification.showError(using: error)
            }
        }).disposed(by: disposeBag)
        
        // create avatar view
        self.mentorView = UINib(nibName: "MentorView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? MentorView
        self.mentorView!.frame.size.width = self.mentorViewWrapper.frame.size.width
        self.mentorView!.frame.size.height = self.mentorViewWrapper.frame.size.height
        self.mentorViewWrapper.addSubview(self.mentorView!)
        
        mentor.accept(self.delegate?.changeMentorRelationshipViewControllerMentorForEdition(self))
        
        mentor.subscribe(onNext: { [self] mentor in
            guard let mentor = mentor else { return }
            self.mentorView!.mentor.accept(mentor)
            self.relationship.accept(JSON(dictionaryLiteral: ("value", mentor["relationship"].stringValue), ("description", mentor["relationshipDescription"].stringValue)))
        }).disposed(by: disposeBag)
        
        relationship.subscribe(onNext: { [self] value in
            guard let value = value else { return }
            self.relationshipTextField.setValue(value["description"].stringValue)
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectRelationshipSegue" {
            if let controller = segue.destination as? SelectLookupViewController {
                controller.delegate = self
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.delegate?.changeMentorRelationshipViewControllerWillShow(self)
    }
}

extension ChangeMentorRelationshipViewController: SelectLookupViewControllerDelegate {
    func selectLookupViewControllerDidCancel(_ controller: SelectLookupViewController) {}
    
    func selectLookupViewController(_ controller: SelectLookupViewController, didFinishSelecting item: JSON) {
        self.relationship.accept(item)
    }
    
    func selectLookupViewControllerDefaultType(_ controller: SelectLookupViewController) -> JSON? {
        return relationship.value
    }
    
    func selectLookupViewControllerLookupType(_ controller: SelectLookupViewController) -> String {
        return "FamilyRelationship"
    }
    
    func selectLookupViewControllerTitle(_ controller: SelectLookupViewController) -> String {
        return "Select family relationship"
    }
}
