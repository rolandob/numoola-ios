//
//  LookupTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/25/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LookupTableViewCell: UITableViewCell {
    // controls
    @IBOutlet var descriptionLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let itemDescription:BehaviorRelay<String?> = BehaviorRelay(value: "")
}

extension LookupTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // bindings
        itemDescription.subscribe(onNext: { [self] value in
            self.descriptionLabel.text = value
        }).disposed(by: disposeBag)
    }
}
