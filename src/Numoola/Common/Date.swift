//
//  Date.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/14/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation

enum DateFormatType:String {
    case Persistent = "persistent", Display = "display", DateTime = "iso8601", DisplayLong = "displayLong", Day = "day", WeekDay = "weekDay", Month = "month", Year = "year"
}

enum DateWeekDay:Int {
    case Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6, Saturday = 7
}

extension Date {
    
    func to(format type: DateFormatType) -> String? {
        let formatter = DateFormatter()
        switch type {
        case .Display:
            formatter.dateFormat = "EEEE, MMMM d"
        case .DisplayLong:
            formatter.dateFormat = "EEEE, MMMM d, yyyy"
        case .DateTime:
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        case .WeekDay:
            formatter.dateFormat = "EEEE"
        case .Day:
            formatter.dateFormat = "d"
        case .Month:
            formatter.dateFormat = "MMMM"
        case .Year:
            formatter.dateFormat = "yyyy"
        default:
            formatter.dateFormat = "yyyy-MM-dd"
        }
        return formatter.string(from: self)
    }
    
    func to(withFormat format: String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func toCurrentDateTime() -> Date? {
        
        var nowComponent = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: Date())
        var selfComponent = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        
        nowComponent.day = selfComponent.day
        nowComponent.month = selfComponent.month
        nowComponent.year = selfComponent.year
        
        return Calendar.current.date(from: nowComponent)
    }
    
    static func from(string value: String, format type: DateFormatType) -> Date? {
        let formatter = DateFormatter()
        switch type {
        case .Display:
            formatter.dateFormat = "EEEE, MMMM d"
        case .DisplayLong:
            formatter.dateFormat = "EEEE, MMMM d, yyyy"
        case .DateTime:
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        case .WeekDay:
            formatter.dateFormat = "EEEE"
        case .Day:
            formatter.dateFormat = "d"
        case .Month:
            formatter.dateFormat = "MMMM"
        case .Year:
            formatter.dateFormat = "yyyy"
        default:
            formatter.dateFormat = "yyyy-MM-dd"
        }
        
        if type == .DateTime {
            if let date = formatter.date(from: value) {
                return date
            } else {
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                return formatter.date(from: value)
            }
        }
        
        return formatter.date(from: value)
    }
    
    static func getWeekDate(for day: DateWeekDay, of date: Date = Date()) -> Date? {
        let weekDay = Calendar.current.component(.weekday, from: date)
        return Calendar.current.date(byAdding: .day, value: (day.rawValue - weekDay), to: date)
    }
}
