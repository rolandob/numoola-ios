//
//  Operators.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/12/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

infix operator <->

func <-> <T: Equatable>(lhs: BehaviorRelay<T>, rhs: BehaviorRelay<T>) -> Disposable {
    typealias ItemType = (current: T, previous: T)
    let bindToUIDisposable = rhs.bind(to: lhs)
    let bindToRelay = lhs
        .subscribe(onNext: { n in
            rhs.accept(n)
        }, onCompleted:  {
            bindToUIDisposable.dispose()
        })
    
    return Disposables.create(bindToUIDisposable, bindToRelay)
}

func <-> <T: Equatable>(lhs: ControlProperty<T>, rhs: BehaviorRelay<T>) -> Disposable {
    typealias ItemType = (current: T, previous: T)
    
    let bindToUIDisposable = rhs.bind(to: lhs)
    let bindToRelay = lhs
        .subscribe(onNext: { n in
            rhs.accept(n)
        }, onCompleted:  {
            bindToUIDisposable.dispose()
        })
    
    return Disposables.create(bindToUIDisposable, bindToRelay)
}
