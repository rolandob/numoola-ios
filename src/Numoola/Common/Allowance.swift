//
//  Allowance.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/28/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import SwiftyJSON

class Allowance {
    public static func getAllowanceLabels(for allowance: JSON?) -> (next: String, label: String) {
        var labels = (next: "Next: Not scheduled", label: "$0.0")
        
        if allowance != nil && allowance!["id"].exists() {
            let scheduledTask = allowance!["scheduledTasks"].exists() ? allowance!["scheduledTasks"].array!.filter({ item in
                let status = item["status"]["value"].string
                return status != nil && status! == "Pending"
            }).first : nil
            if scheduledTask != nil {
                labels.next = "Next: \(Date.from(string: scheduledTask!["effectiveDate"].string!, format: .Persistent)!.to(format: .DisplayLong)!)"
            }
            let occurenceType = allowance!["recurrenceType"]["value"].string!
            switch occurenceType {
            case "Daily":
                labels.label = "$\(allowance!["amount"].float!) daily"
            case "Weekly":
                labels.label = "$\(allowance!["amount"].float!) every week on \(Date.from(string: allowance!["startDate"].string!, format: .DateTime)!.to(format: .WeekDay)!)"
            case "BiWeekly":
                labels.label = "$\(allowance!["amount"].float!) every two weeks on \(Date.from(string: allowance!["startDate"].string!, format: .DateTime)!.to(format: .WeekDay)!)"
            case "Monthly":
                labels.label = "$\(allowance!["amount"].float!) every month on the \(Date.from(string: allowance!["startDate"].string!, format: .DateTime)!.to(format: .Day)!)"
            case "OneTime":
                labels.label = "$\(allowance!["amount"].float!) one time"
            default:
                labels.label = "$0.0"
            }
        }
        
        return labels
    }
}
