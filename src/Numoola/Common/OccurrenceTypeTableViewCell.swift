//
//  OccurrenceTypeTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/11/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class OccurrenceTypeTableViewCell: UITableViewCell {
    // controls
    @IBOutlet var descriptionLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let occurrence:BehaviorRelay<String?> = BehaviorRelay(value: "")
}

extension OccurrenceTypeTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // bindings
        occurrence.subscribe(onNext: { [self] value in
            self.descriptionLabel.text = value
        }).disposed(by: disposeBag)
    }
}
