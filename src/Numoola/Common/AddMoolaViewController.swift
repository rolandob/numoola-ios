//
//  AddMoolaViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/8/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import FontAwesome_swift

protocol AddMoolaViewControllerDelegate:class {
    func addMoolaViewControllerDidCancel(_ controller: AddMoolaViewController)
    func addMoolaViewController(_ controller: AddMoolaViewController, didFinishAdding amount: Double)
    func addMoolaViewControllerWillShow(_ controller: AddMoolaViewController)
}


class AddMoolaViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var fundingSourceTextField: NMLTextField!
    @IBOutlet var amountTextField: NMLTextField!
    // delegate
    weak var delegate: AddMoolaViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let bank:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
}

extension AddMoolaViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.addMoolaViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            // TODO: Add Moola using new API
            self.delegate?.addMoolaViewController(self, didFinishAdding: (self.amountTextField.value.value! as NSString).doubleValue)
        }).disposed(by: disposeBag)
        
        bank.subscribe(onNext: { [self] bank in
            guard let bank = bank else { return }
            self.fundingSourceTextField.setValue("\(bank["alias"].stringValue) - \(bank["firstName"].stringValue) \(bank["lastName"].stringValue)")
        }).disposed(by: disposeBag)
        
        let bankValid: Observable<Bool> = bank.map{ item -> Bool in item != nil }.share(replay: 1)
        let allValid: Observable<Bool> = Observable.combineLatest(bankValid, amountTextField.valid) { $0 && $1 }
        // bind done isEnabled
        allValid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegate?.addMoolaViewControllerWillShow(self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectBankSegue" {
            if let controller = segue.destination as? SelectBankAccountViewController {
                controller.delegate = self
            }
        }
    }
}

extension AddMoolaViewController: SelectBankAccountViewControllerDelegate {
    func SelectBankAccountViewControllerDidCancel(_ controller: SelectBankAccountViewController) {}
    
    func SelectBankAccountViewController(_ controller: SelectBankAccountViewController, didSelect bank: JSON) {
        self.bank.accept(bank)
    }
}
