//
//  ParentTabBarController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/21/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class ParentTabBarController: UITabBarController {
    // fields
    var disposeBag = DisposeBag()
    let tabBarSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "TabBar")
}

extension ParentTabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.tabBar.clipsToBounds = true
        self.tabBar.backgroundImage = UIImage()
        
        tabBarSubject.subscribe(onNext: { [self] value in
            guard let value = value else { return }
            self.tabBar.isHidden = value["hidden"].bool == true ? true : false
        }).disposed(by: disposeBag)
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            
            self.tabBar.tintColor = theme.getColor(with: TabBarStyles.TintColor, for: .normal)
            self.tabBar.unselectedItemTintColor = theme.getColor(with: TabBarStyles.UnselectedTintColor, for: .normal)
        }).disposed(by: disposeBag)
    }
    
    @IBAction func handleTabBarRightSwipe(_ gesture: UISwipeGestureRecognizer) {
        print(self.selectedIndex)
        if self.selectedIndex < 4 {
            self.selectedIndex += 1
        }
    }
    
    @IBAction func handleTabBarLeftSwipe(_ gesture: UISwipeGestureRecognizer) {
        print(self.selectedIndex)
        if self.selectedIndex > 0 {
            self.selectedIndex -= 1
        }
    }
}
