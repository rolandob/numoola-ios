//
//  StartDateViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/1/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import FontAwesome_swift

protocol StartDateViewControllerDelegate:class {
    func startDateViewControllerDidCancel(_ controller: StartDateViewController)
    func startDateViewController(_ controller: StartDateViewController, didFinishEditing recurrentDate: RecurrentDate)
    func startDateViewControllerDateForEdition(_ controller: StartDateViewController) -> RecurrentDate
}

class StartDateViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var startDateIcon: FontAwesomeImageView!
    @IBOutlet var startDateLabel: UILabel!
    @IBOutlet var recurrenceIcon: FontAwesomeImageView!
    @IBOutlet var recurrenceLabel: UILabel!
    // delegate
    weak var delegate: StartDateViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let date:BehaviorRelay<Date> = BehaviorRelay(value: Date())
    let recurrentDate:BehaviorRelay<RecurrentDate> = BehaviorRelay(value: RecurrentDate(Date(), LookupType("OneTime", "One Time"), "Date"))
}

extension StartDateViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.startDateViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.startDateViewController(self, didFinishEditing: self.recurrentDate.value)
        }).disposed(by: disposeBag)
        
        startDateIcon.imageColor = ThemeSecondaryColor
        startDateIcon.awakeFromNib()
        self.startDateLabel.textColor = .darkGray
        
        recurrenceIcon.imageColor = ThemeSecondaryColor
        recurrenceIcon.awakeFromNib()
        self.recurrenceLabel.textColor = .darkGray
        
        self.recurrentDate.accept((delegate?.startDateViewControllerDateForEdition(self))!)
        
        recurrentDate.subscribe(onNext: { [self] value in
            self.navigationItem.title = value.title
            self.recurrenceLabel.text = value.occurrence.description
            self.startDateLabel.text = value.date.to(format: .DisplayLong)
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SetOccurrenceSegue" {
            if let controller = segue.destination as? SelectLookupViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "SetStartDateSegue" {
            if let controller = segue.destination as? SelectDateViewController {
                controller.delegate = self
            }
        }
    }
}

extension StartDateViewController: SelectLookupViewControllerDelegate {
    func selectLookupViewControllerDidCancel(_ controller: SelectLookupViewController) {}
    
    func selectLookupViewController(_ controller: SelectLookupViewController, didFinishSelecting item: JSON) {
        recurrentDate.value.occurrence = LookupType(item["value"].stringValue, item["description"].stringValue)
        recurrenceLabel.text = recurrentDate.value.occurrence.description
    }
    
    func selectLookupViewControllerDefaultType(_ controller: SelectLookupViewController) -> JSON? {
        return JSON(dictionaryLiteral:
            ("value", recurrentDate.value.occurrence.value),
                    ("description", recurrentDate.value.occurrence.description)
        )
    }
    
    func selectLookupViewControllerLookupType(_ controller: SelectLookupViewController) -> String {
        return "RecurrenceType"
    }
    
    func selectLookupViewControllerTitle(_ controller: SelectLookupViewController) -> String {
        return "Select Occurrence"
    }
}

extension StartDateViewController: SelectDateViewControllerDelegate {
    func selectDateViewControllerWillShow(_ controller: SelectDateViewController) {}
    
    func selectDateViewControllerDidCancel(_ controller: SelectDateViewController) {}
    
    func selectDateViewController(_ controller: SelectDateViewController, didFinishEditing date: DateModel) {
        recurrentDate.value.date = date.value
        startDateLabel.text = date.value.to(format: .DisplayLong)
    }
    
    func selectDateViewControllerModelForEdition(_ controller: SelectDateViewController) -> DateModel {
        return DateModel(.date, recurrentDate.value.date, recurrentDate.value.title)
    }
}
