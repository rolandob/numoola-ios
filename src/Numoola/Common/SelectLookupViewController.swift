//
//  SelectLookupViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/25/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

protocol SelectLookupViewControllerDelegate:class {
    func selectLookupViewControllerDidCancel(_ controller: SelectLookupViewController)
    func selectLookupViewController(_ controller: SelectLookupViewController, didFinishSelecting item: JSON)
    func selectLookupViewControllerDefaultType(_ controller: SelectLookupViewController) -> JSON?
    func selectLookupViewControllerLookupType(_ controller: SelectLookupViewController) -> String
    func selectLookupViewControllerTitle(_ controller: SelectLookupViewController) -> String
}

class SelectLookupViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    // delegate
    weak var delegate: SelectLookupViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let items:BehaviorRelay<[SelectableItem]> = BehaviorRelay(value: [])
}

extension SelectLookupViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        if delegate != nil {
            navigationItem.title = delegate!.selectLookupViewControllerTitle(self)
            
            let lookupClass = delegate!.selectLookupViewControllerLookupType(self)
            Api.instance().rxInvokeCbeEntity(
                using: "Get",
                to: "entity/\(lookupClass)"
                ).observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] result in
                    switch result {
                    case .success(let data):
                        if let json = data {
                            var items:[SelectableItem] = []
                            let defaultItem = self.delegate?.selectLookupViewControllerDefaultType(self)
                            json["value"].arrayValue.forEach({item in
                                let selectableItem = SelectableItem(with: item, isSelected: defaultItem != nil && item["value"].stringValue == defaultItem!["value"].stringValue)
                                items.append(selectableItem)
                            })
                            self.items.accept(items)
                            
                        }
                    case .failure(let error):
                        Notification.showError(using: error)
                    }
                }).disposed(by: disposeBag)
        }
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // bind items to table view
        items.bind(to: tableView.rx.items(cellIdentifier: "LookupCell")) { (row, item, cell) in
            if let cell = cell as? LookupTableViewCell {
                cell.itemDescription.accept(item.item["description"].stringValue)
                if item.selected == true {
                    cell.accessoryType = .checkmark
                } else {
                    cell.accessoryType = .none
                }
            }
            }.disposed(by: disposeBag)
        // on name selected fill up the text field
        Observable.zip(tableView.rx.itemSelected, tableView.rx.modelSelected(SelectableItem.self)).bind { [unowned self] indexPath, item in
            if let cell = self.tableView.cellForRow(at: indexPath) {
                var selectedIndex = 0
                for otherItem in self.items.value {
                    if otherItem.selected == true {
                        otherItem.selected = false
                        break
                    }
                    selectedIndex = selectedIndex + 1
                }
                
                if selectedIndex < self.items.value.count && selectedIndex != indexPath.row {
                    if let selectedCell = self.tableView.cellForRow(at: IndexPath(row: selectedIndex, section: 0)) {
                        print(selectedCell)
                        selectedCell.accessoryType = .none
                    }
                }
                cell.accessoryType = .checkmark
                item.selected = true
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
            }.disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.selectLookupViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            if let selectedItem = self.items.value.filter({ item in item.selected == true }).first {
                self.delegate?.selectLookupViewController(self, didFinishSelecting: selectedItem.item)
            }
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
}

extension SelectLookupViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.prepareDisclosureIndicator()
        cell.tintColor = ThemeSecondaryColor
    }
}
