//
//  Error.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/5/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import AWSMobileClient

class Errors {
    public static func getErrorMessage(for error: Error, withDefault msg: String = "An error has occurred") -> String {
        if type(of: error) == AWSMobileClientError.self {
            return AWSMobile.getErrorMessage(for: error as! AWSMobileClientError)
        } else {
            let nsError = error as NSError
            if let message = nsError.userInfo.first(where: { (key:String,value:Any) -> Bool in
                return key == "message"
            }) {
                return message.value as! String
            }
        }
        print(error)
        return msg
    }
}
