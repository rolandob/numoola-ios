//
//  PageViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/15/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

protocol PageViewControllerDelegate:class {
    func pageViewControllerPageController(_ controller: PageViewController) -> WizardPageViewController
}

class PageViewController: UIViewController {
    // controls
    @IBOutlet var fields:[NMLTextField] = []
    @IBOutlet var nextActionButton: NMLButton!
    @IBOutlet var backActionButton: NMLButton!
    @IBOutlet var doneActionButton: NMLButton!
    // delegate
    weak var delegate: PageViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    var wizardIdentifier: String = ""
    let valid: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    var _pattern:String = ""
    
    @IBInspectable
    var pageId: String {
        get {
            return wizardIdentifier
        }
        set {
            wizardIdentifier = newValue
        }
    }
}

extension PageViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if nextActionButton != nil {
            nextActionButton.rx.tap.subscribe(onNext: { [self] _ in
                self.nextPage()
            }).disposed(by: disposeBag)
        }
        
        if backActionButton != nil {
            backActionButton.rx.tap.subscribe(onNext: { [self] _ in
                self.previousPage()
            }).disposed(by: disposeBag)
        }
        
        if doneActionButton != nil {
            doneActionButton.rx.tap.subscribe(onNext: { [self] _ in
                self.delegate?.pageViewControllerPageController(self).finish()
            }).disposed(by: disposeBag)
        }
    }
    
    func nextPage() {
        delegate?.pageViewControllerPageController(self).next(from: wizardIdentifier)
    }
    
    func previousPage(){
        delegate?.pageViewControllerPageController(self).previous(from: wizardIdentifier)
    }
    
    func canGoNext() -> Bool {
        return delegate?.pageViewControllerPageController(self).canGoNext(from: wizardIdentifier) ?? false
    }
    
    func canGoPrevious() -> Bool {
        return delegate?.pageViewControllerPageController(self).canGoPrevius(from: wizardIdentifier) ?? false
    }
    
    @objc func getData() -> WizardPageData? {
        if isViewLoaded == true {
            fields.forEach({ field in
                field.validate()
            })
            return getData(with: fields, valid: true)
        } else {
            return getData(with: [], valid: false)
        }
    }
    
    func getData(with fields:[NMLTextField?], valid validValue:Bool) -> WizardPageData? {
        var dictionary:[String:Any] = [:]
        var errors:[String] = []
        for field in fields {
            if let field = field {
                dictionary.updateValue(field.value.value ?? "", forKey: field.name)
                errors = errors + (field.errors.value != nil ? field.errors.value! : [])
            }
        }
        let fields = JSON(dictionary)
        return WizardPageData(wizardIdentifier, errors.count > 0 ? false : validValue, errors , data: fields)
    }
}
