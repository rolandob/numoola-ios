//
//  AvatarCollectionViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/11/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON

class AvatarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    var avatarView: AvatarView!
    var item: SelectableItem! {
        didSet {
            DispatchQueue.main.async {
                if self.avatarView != nil {
                    self.avatarView.removeFromSuperview()
                }
                self.avatarView = (UINib(nibName: "AvatarView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AvatarView)
                self.avatarView.kid = self.item.item
                
                self.avatarView.handleAvatarTap = {json in
                    self.avatarView.avatarSelected.accept(!(self.avatarView.avatarSelected.value))
                    self.item.selected = self.avatarView.avatarSelected.value
                }
                self.mainView.addSubview(self.avatarView)
                self.avatarView.avatarSelected.accept(self.item.selected)
            }
        }
    }
}
