//
//  AllocationViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/1/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class AllocationModel {
    var spending: Float
    var saving:Float
    var giving:Float
    
    init(_ spending: Float, _ saving:Float, _ giving: Float) {
        self.saving = saving
        self.spending = spending
        self.giving = giving
    }
}

protocol AllocationViewControllerDelegate: class {
    func allocationViewControllerDidCancel(_ controller: AllocationViewController)
    func allocationViewController(_ controller: AllocationViewController, didFinishEditing allocation: AllocationModel)
    func allocationViewControllerAllocationForEdition(_ controller: AllocationViewController) -> AllocationModel
    func allocationViewControllerWillShow(_ controller: AllocationViewController)
}

class AllocationViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var savingLabel: UILabel!
    @IBOutlet var spendingLabel: UILabel!
    @IBOutlet var givingLabel: UILabel!
    @IBOutlet var savingSlider: UISlider!
    @IBOutlet var spendingSlider: UISlider!
    @IBOutlet var givingSlider: UISlider!
    // delegate
    weak var delegate: AllocationViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let allocation:BehaviorRelay<AllocationModel?> = BehaviorRelay(value: nil)
    let saving:BehaviorRelay<Float> = BehaviorRelay(value: 0.0)
    let spending:BehaviorRelay<Float> = BehaviorRelay(value: 0.0)
    let giving:BehaviorRelay<Float> = BehaviorRelay(value: 0.0)
}

extension AllocationViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        savingSlider.tintColor = ThemeSecondaryColor
        spendingSlider.tintColor = ThemeSecondaryColor
        givingSlider.tintColor = ThemeSecondaryColor
        
        savingSlider.rx.value.subscribe(onNext: { [self] value in
            self.savingSlider.setValue(round(self.savingSlider.value / 5) * 5, animated: false)
        }).disposed(by: disposeBag)
        
        spendingSlider.rx.value.subscribe(onNext: { [self] value in
            self.spendingSlider.setValue(round(self.spendingSlider.value / 5) * 5, animated: false)
        }).disposed(by: disposeBag)
        
        givingSlider.rx.value.subscribe(onNext: { [self] value in
            self.givingSlider.setValue(round(self.givingSlider.value / 5) * 5, animated: false)
        }).disposed(by: disposeBag)
        
        let isValid:Observable<Bool> = Observable.combineLatest(saving.share(replay: 1), spending.share(replay:1), giving.share(replay: 1))
        { $0 + $1 + $2 }.map({ total in total == Float(100)})
        
        isValid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
        (savingSlider.rx.value <-> saving).disposed(by: disposeBag)
        (spendingSlider.rx.value <-> spending).disposed(by: disposeBag)
        (givingSlider.rx.value <-> giving).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.allocationViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.allocationViewController(self, didFinishEditing: AllocationModel(self.spending.value, self.saving.value, self.giving.value))
        }).disposed(by: disposeBag)
        // set initial allocation for edition
        allocation.accept(delegate?.allocationViewControllerAllocationForEdition(self))
        
        allocation.subscribe(onNext: { [self] allocation in
            guard let allocation = allocation else { return }
            self.saving.accept(allocation.saving)
            self.spending.accept(allocation.spending)
            self.giving.accept(allocation.giving)
        }).disposed(by: disposeBag)
        
        saving.subscribe(onNext: { [self] saving in
            self.savingLabel.text = "Saving: \(saving) %"
        }).disposed(by: disposeBag)
        
        spending.subscribe(onNext: { [self] spending in
            self.spendingLabel.text = "Spending: \(spending) %"
        }).disposed(by: disposeBag)
        
        giving.subscribe(onNext: { [self] giving in
            self.givingLabel.text = "Giving: \(giving) %"
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegate?.allocationViewControllerWillShow(self)
    }
}
