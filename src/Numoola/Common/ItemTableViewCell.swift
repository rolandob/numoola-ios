//
//  ItemTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/26/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ItemTableViewCell: UITableViewCell {
    // controls
    @IBOutlet var descriptionLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let itemDescription:BehaviorRelay<String?> = BehaviorRelay(value: "")
}

extension ItemTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // bindings
        itemDescription.subscribe(onNext: { [self] value in
            self.descriptionLabel.text = value
        }).disposed(by: disposeBag)
    }
}
