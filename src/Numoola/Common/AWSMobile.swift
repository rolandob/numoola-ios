//
//  AWSMobile.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/3/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import AWSMobileClient

class AWSMobile {
    public static func getErrorMessage(for error: AWSMobileClientError) -> String {
    switch error {
    case .invalidParameter(let message):
        return message
    case .aliasExists(let message):
        return message
    case .codeDeliveryFailure(let message):
        return message
    case .codeMismatch(let message):
        return message
    case .expiredCode(let message):
        return message
    case .groupExists(let message):
        return message
    case .internalError(let message):
        return message
    case .invalidLambdaResponse(let message):
        return message
    case .invalidOAuthFlow(let message):
        return message
    case .invalidPassword(let message):
        return message
    case .invalidUserPoolConfiguration(let message):
        return message
    case .limitExceeded(let message):
        return message
    case .mfaMethodNotFound(let message):
        return message
    case .notAuthorized(let message):
        return message
    case .passwordResetRequired(let message):
        return message
    case .resourceNotFound(let message):
        return message
    case .scopeDoesNotExist(let message):
        return message
    case .softwareTokenMFANotFound(let message):
        return message
    case .tooManyFailedAttempts(let message):
        return message
    case .tooManyRequests(let message):
        return message
    case .unexpectedLambda(let message):
        return message
    case .userLambdaValidation(let message):
        return message
    case .userNotConfirmed(let message):
        return message
    case .userNotFound(let message):
        return message
    case .usernameExists(let message):
        return message
    case .unknown(let message):
        return message
    case .notSignedIn(let message):
        return message
    case .identityIdUnavailable(let message):
        return message
    case .guestAccessNotAllowed(let message):
        return message
    case .federationProviderExists(let message):
        return message
    case .cognitoIdentityPoolNotConfigured(let message):
        return message
    case .unableToSignIn(let message):
        return message
    case .invalidState(let message):
        return message
    case .userPoolNotConfigured(let message):
        return message
    case .userCancelledSignIn(let message):
        return message
    case .badRequest(let message):
        return message
    case .expiredRefreshToken(let message):
        return message
    case .errorLoadingPage(let message):
        return message
    case .securityFailed(let message):
        return message
    case .idTokenNotIssued(let message):
        return message
    case .idTokenAndAcceessTokenNotIssued(let message):
        return message
    case .invalidConfiguration(let message):
        return message
    case .deviceNotRemembered(let message):
        return message
    }
    }
}
