//
//  SelectBankAccountViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/8/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxCocoa
import RxSwift

protocol SelectBankAccountViewControllerDelegate:class {
    func SelectBankAccountViewControllerDidCancel(_ controller: SelectBankAccountViewController)
    func SelectBankAccountViewController(_ controller: SelectBankAccountViewController, didSelect bank: JSON)
}

class SelectBankAccountViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    // delegate
    weak var delegate: SelectBankAccountViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let items:BehaviorRelay<[SelectableItem]> = BehaviorRelay(value: [])
}

extension SelectBankAccountViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        Api.instance().rxInvoke(using: "Get", to: "userAccount/self/bankAccount", with: RequestOptions(select: "id,alias,firstName, lastName, accountNumber, type, status", filter: "status eq 'Verified' or status eq 'Unverified'"))
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let banks = data {
                        var items:[SelectableItem] = []
                        banks["value"].arrayValue.forEach({item in
                            let selectableItem = SelectableItem(with: item, isSelected: false)
                            items.append(selectableItem)
                        })
                        self.items.accept(items)
                    }
                case .failure(let error):
                    Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
                }
            }).disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // bind items to table view
        items.bind(to: tableView.rx.items(cellIdentifier: "BankAccountCell")) { (row, item, cell) in
            if let cell = cell as? BankAccountTableViewCell {
                cell.bankView.bank.accept(item.item)
                if item.selected == true {
                    cell.accessoryType = .checkmark
                } else {
                    cell.accessoryType = .none
                }
            }
            }.disposed(by: disposeBag)
        
        self.doneButton.isEnabled = false
        
        // on name selected fill up the text field
        Observable.zip(tableView.rx.itemSelected, tableView.rx.modelSelected(SelectableItem.self)).bind { [unowned self] indexPath, item in
            if let cell = self.tableView.cellForRow(at: indexPath) {
                var selectedIndex = 0
                for otherItem in self.items.value {
                    if otherItem.selected == true {
                        otherItem.selected = false
                        break
                    }
                    selectedIndex = selectedIndex + 1
                }
                
                if selectedIndex < self.items.value.count && selectedIndex != indexPath.row {
                    if let selectedCell = self.tableView.cellForRow(at: IndexPath(row: selectedIndex, section: 0)) {
                        selectedCell.accessoryType = .none
                    }
                }
                cell.accessoryType = .checkmark
                item.selected = true
                self.tableView.deselectRow(at: indexPath, animated: true)
                self.doneButton.isEnabled = true
            }
            }.disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.SelectBankAccountViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            if let selectedItem = self.items.value.filter({ item in item.selected == true }).first {
                self.delegate?.SelectBankAccountViewController(self, didSelect: selectedItem.item)
            }
        }).disposed(by: disposeBag)
        
        // set default background when no bank accounts are available
        items.asObservable()
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [unowned self] missions in
                if missions.count == 0 {
                    let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
                    noDataLabel.text = "No bank accounts available"
                    noDataLabel.textColor = ThemePrimaryColor
                    noDataLabel.textAlignment = NSTextAlignment.center
                    self.tableView.backgroundView = noDataLabel
                    self.tableView.separatorStyle = .none
                } else {
                    self.tableView.backgroundView = nil
                    self.tableView.separatorStyle = .singleLine
                }
            }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
}

extension SelectBankAccountViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.prepareDisclosureIndicator()
        cell.tintColor = ThemeSecondaryColor
    }
}
