//
//  WizardPageViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/15/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

enum WizardNavigationDirection:String {
    case Next = "Next", Previous = "Previous"
}

class WizardPageData:NSObject {
    var identifier:String
    var valid:Bool
    var errors:[String]
    var data:JSON?
    
    init(_ identifier:String, _ valid:Bool, _ errors:[String], data: JSON?) {
        self.identifier = identifier
        self.valid = valid
        self.errors = errors
        self.data = data
    }
}

protocol WizardPageViewControllerDelegate:class {
    func wizardPageViewControllerNavigationListener(_ controller: WizardPageViewController) -> BehaviorRelay<String>
    func wizardPageViewControllerPageIdentifiers(_ controller: WizardPageViewController) -> [String]
    func wizardPageViewControllerDidFinishNavigating(_ controller: WizardPageViewController, to identifier: String, with index: Int, withController controllerInstance: UIViewController) -> Void
}

class WizardPageViewController: UIPageViewController {
    // controls
    private(set) lazy var orderedViewControllers: [String:UIViewController] = {
        return [:]
    }()
    // delegate
    weak var customDelegate: WizardPageViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    var currentIdentifier:String = ""
    var handleFinish: ( () -> Void )?
}

extension WizardPageViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        if customDelegate != nil {
            let identifiers:[String] = (customDelegate?.wizardPageViewControllerPageIdentifiers(self))!
            for index in 1...identifiers.count {
                let identifier = identifiers[index - 1]
                if let controller = self.newViewController(identifier) {
                    orderedViewControllers.updateValue(controller, forKey: identifier)
                }
            }
            customDelegate!.wizardPageViewControllerNavigationListener(self).subscribe(onNext: { [self] identifier in
                self.navigate(to: identifier, with: .forward)
            }).disposed(by: disposeBag)
        }
    }
    
    private func newViewController(_ identifier: String) -> UIViewController? {
        if let controller = UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: identifier) as? PageViewController {
            controller.delegate = self
            return controller
        }
        return nil
    }
    
    private func navigate(to identifier:String, with direction: UIPageViewController.NavigationDirection){
        if let entry = self.orderedViewControllers.first(where: { (key,_) in key == identifier }) {
            if identifier != currentIdentifier {
                self.setViewControllers([entry.value], direction: direction, animated: true, completion: nil)
                self.currentIdentifier = identifier
                let identifiers:[String] = (customDelegate?.wizardPageViewControllerPageIdentifiers(self))!
                self.customDelegate?.wizardPageViewControllerDidFinishNavigating(self, to: identifier, with: identifiers.firstIndex(where: { theKey -> Bool in
                    return theKey == identifier
                })!, withController: entry.value)
            }
        }
    }
    
    private func getNavigationInfo(from identifier: String, to direction: WizardNavigationDirection) -> (canNavigate:Bool, toIndex:Int) {
        let identifiers:[String] = (customDelegate?.wizardPageViewControllerPageIdentifiers(self))!
        if let index = identifiers.firstIndex(where: { id -> Bool in id == identifier }) {
            let newIndex = index + (direction == .Next ? 1 : -1)
            
            if newIndex >= 0 && newIndex < identifiers.count {
                return (canNavigate: true, toIndex: newIndex)
            }
        }
        return (canNavigate: false, toIndex: -1)
    }
    
    func navigate(from identifier: String, to direction: WizardNavigationDirection) {
        let navInfo = getNavigationInfo(from: identifier, to: direction)
        if navInfo.canNavigate == true {
            let identifiers:[String] = (customDelegate?.wizardPageViewControllerPageIdentifiers(self))!
            let toIdentifier = identifiers[navInfo.toIndex]
            navigate(to: toIdentifier, with: direction.rawValue == "Next" ? .forward : .reverse)
            customDelegate!.wizardPageViewControllerNavigationListener(self).accept(toIdentifier)
        }
    }
    
    func navigate(to index: Int) {
        let identifiers:[String] = (customDelegate?.wizardPageViewControllerPageIdentifiers(self))!
        let currentIndex = identifiers.firstIndex(of: currentIdentifier)
        if index == currentIndex! || index < 0 || index > identifiers.count {
             return
        }
        let identifier = identifiers[index]
        
        if index > currentIndex! {
            navigate(to: identifier, with: .forward)
        } else if index < currentIndex! {
            navigate(to: identifier, with: .reverse)
        }
    }
    
    func next(from identifier: String) {
        navigate(from: identifier, to: .Next)
    }
    
    func previous(from identifier: String) {
        navigate(from: identifier, to: .Previous)
    }
    
    func canGoNext(from identifier: String) -> Bool {
        return getNavigationInfo(from: identifier, to: .Next).canNavigate
    }
    
    func canGoPrevius(from identifier: String) -> Bool {
        return getNavigationInfo(from: identifier, to: .Previous).canNavigate
    }
    
    func getData() -> [WizardPageData?] {
        
        return orderedViewControllers.map({ (key: String, value: UIViewController) -> WizardPageData? in
            if let controller = value as? PageViewController {
                return controller.getData()
            }
            return nil
        })
    }
    
    func finish() {
        if handleFinish != nil {
            self.handleFinish!()
        }
    }
}

extension WizardPageViewController: PageViewControllerDelegate {
    func pageViewControllerPageController(_ controller: PageViewController) -> WizardPageViewController {
        return self
    }
}
