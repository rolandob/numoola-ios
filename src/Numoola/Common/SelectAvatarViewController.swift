//
//  MissionCollectionViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/10/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

protocol SelectAvatarViewControllerDelegate:class {
    func selectAvatarViewControllerDidCancel(_ controller: SelectAvatarViewController)
    func selectAvatarViewController(_ controller: SelectAvatarViewController, didFinishSelecting children: SelectableItemCollection)
    func selectAvatarViewControllerItemsForView(_ controller: SelectAvatarViewController) -> SelectableItemCollection
}

class SelectAvatarViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var mainView: UIView!
    // delegate
    weak var delegate: SelectAvatarViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let kids:BehaviorRelay<SelectableItemCollection?> = BehaviorRelay(value: nil)
}

extension SelectAvatarViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        kids.accept(delegate?.selectAvatarViewControllerItemsForView(self))
        kids.observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] kids in
            guard let _ = kids else { return }
            self.collectionView.reloadData()
            }
        ).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.selectAvatarViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.selectAvatarViewController(self, didFinishSelecting: self.kids.value!)
        }).disposed(by: disposeBag)
    }
}

extension SelectAvatarViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let kids = kids.value else { return 0 }
        return kids.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCell", for: indexPath) as! AvatarCollectionViewCell
        guard let kids = kids.value else { return cell }
        let item = kids.items[indexPath.row]
        cell.item = item
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .clear
    }
}
