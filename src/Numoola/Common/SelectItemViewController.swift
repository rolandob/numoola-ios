//
//  SelectItemViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/26/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

protocol SelectItemViewControllerDelegate:class {
    func selectItemViewControllerDidCancel(_ controller: SelectItemViewController)
    func selectItemViewController(_ controller: SelectItemViewController, didFinishSelecting item: JSON)
    func selectItemViewControllerDefault(_ controller: SelectItemViewController) -> JSON?
    func selectItemViewControllerItems(_ controller: SelectItemViewController) -> [JSON]
    func selectItemViewControllerTitle(_ controller: SelectItemViewController) -> String
    func selectItemViewControllerGetValue(_ controller: SelectItemViewController, for item: JSON) -> String
    func selectItemViewControllerGetDescription(_ controller: SelectItemViewController, for item: JSON) -> String
}

class SelectItemViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    // delegate
    weak var delegate: SelectItemViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let items:BehaviorRelay<[SelectableItem]> = BehaviorRelay(value: [])
}

extension SelectItemViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        if delegate != nil {
            navigationItem.title = delegate!.selectItemViewControllerTitle(self)
            
            let jsonItems = delegate!.selectItemViewControllerItems(self)
            let defaultItem = self.delegate?.selectItemViewControllerDefault(self)
            var selectableItems:[SelectableItem] = []
            jsonItems.forEach({item in
                let itemValue = delegate!.selectItemViewControllerGetValue(self, for: item)
                let defaultItemValue = defaultItem != nil ? delegate!.selectItemViewControllerGetValue(self, for: defaultItem!) : ""
                let selectableItem = SelectableItem(with: item, isSelected: itemValue == defaultItemValue)
                selectableItems.append(selectableItem)
            })
            self.items.accept(selectableItems)
        }
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // bind items to table view
        items.bind(to: tableView.rx.items(cellIdentifier: "ItemCell")) { (row, item, cell) in
            if let cell = cell as? ItemTableViewCell {
                cell.itemDescription.accept(self.delegate?.selectItemViewControllerGetDescription(self, for: item.item) ?? "")
                if item.selected == true {
                    cell.accessoryType = .checkmark
                } else {
                    cell.accessoryType = .none
                }
            }
            }.disposed(by: disposeBag)
        // on name selected fill up the text field
        Observable.zip(tableView.rx.itemSelected, tableView.rx.modelSelected(SelectableItem.self)).bind { [unowned self] indexPath, item in
            if let cell = self.tableView.cellForRow(at: indexPath) {
                var selectedIndex = 0
                for otherItem in self.items.value {
                    if otherItem.selected == true {
                        otherItem.selected = false
                        break
                    }
                    selectedIndex = selectedIndex + 1
                }
                
                if selectedIndex < self.items.value.count && selectedIndex != indexPath.row {
                    if let selectedCell = self.tableView.cellForRow(at: IndexPath(row: selectedIndex, section: 0)) {
                        print(selectedCell)
                        selectedCell.accessoryType = .none
                    }
                }
                cell.accessoryType = .checkmark
                item.selected = true
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
            }.disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.selectItemViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            if let selectedItem = self.items.value.filter({ item in item.selected == true }).first {
                self.delegate?.selectItemViewController(self, didFinishSelecting: selectedItem.item)
            }
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
}

extension SelectItemViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.prepareDisclosureIndicator()
        cell.tintColor = ThemeSecondaryColor
    }
}
