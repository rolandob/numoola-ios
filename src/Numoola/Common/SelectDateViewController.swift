//
//  SelectDateViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/20/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DateModel {
    var mode:UIDatePicker.Mode
    var value:Date
    var title:String
    
    init(_ mode: UIDatePicker.Mode, _ value: Date, _ title: String) {
        self.mode = mode
        self.value = value
        self.title = title
    }
}

protocol SelectDateViewControllerDelegate: class {
    func selectDateViewControllerDidCancel(_ controller: SelectDateViewController)
    func selectDateViewController(_ controller: SelectDateViewController, didFinishEditing date: DateModel)
    func selectDateViewControllerModelForEdition(_ controller: SelectDateViewController) -> DateModel
    func selectDateViewControllerWillShow(_ controller: SelectDateViewController)
}

class SelectDateViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    // delegate
    weak var delegate: SelectDateViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
}

extension SelectDateViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.selectDateViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.selectDateViewController(self, didFinishEditing: DateModel(self.datePicker.datePickerMode, self.datePicker.date, self.navigationItem.title!))
        }).disposed(by: disposeBag)
        
        if let model = delegate?.selectDateViewControllerModelForEdition(self) {
            datePicker.date = model.value
            datePicker.datePickerMode = model.mode
            navigationItem.title = model.title
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegate?.selectDateViewControllerWillShow(self)
    }
}
