//
//  FilterViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/6/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa
import RxDataSources

protocol FilterViewControllerDelegate: class {
    func filterViewController(_ controller: FilterViewController, didFilter filters: FilterOptions!) -> Void
    func filterViewControllerFiltersToShow(_ controller: FilterViewController) -> FilterOptions!
    func filterViewControllerTitle(_ controller: FilterViewController ) -> String
    func filterViewControllerDidCancel(_ controller: FilterViewController)
    func filterViewControllerWillShow(_ controller: FilterViewController)
}

class FilterViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var filterTableView: UITableView!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    // delegate
    weak var delegate: FilterViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let filters:BehaviorRelay<FilterOptions?> = BehaviorRelay(value: nil)
    let sections:BehaviorRelay<[SectionModel<String, FilterOption>]> = BehaviorRelay(value: [])
}

extension FilterViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        navigationItem.title = delegate?.filterViewControllerTitle(self)
        // bindings
        doneButton.rx.tap.subscribe(onNext: {[self] _ in
            if self.delegate != nil {
                self.delegate?.filterViewController(self, didFilter: self.filters.value)
            }
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: {[self] _ in
            self.delegate?.filterViewControllerDidCancel(self)
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        filters.subscribe(onNext: { [self] filters in
            guard let filters = filters else { return }
            var sections:[SectionModel<String, FilterOption>] = []
            for group in filters.groups {
                let sectionModel = SectionModel(model: group.title, items: group.options)
                sections.append(sectionModel)
            }
            self.sections.accept(sections)
        }).disposed(by: disposeBag)
        
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, FilterOption>>(
            configureCell: { dataSource, table, indexPath, item in
                let cell = table.dequeueReusableCell(withIdentifier: "FilterItem")!
                if let label = cell.viewWithTag(1000) as? UILabel {
                    label.text = item.title
                    if item.selected == true {
                        cell.accessoryType = .checkmark
                    }
                }
                cell.tintColor = ThemePrimaryColor
                return cell
            },
            titleForHeaderInSection:{ dataSource, index in
                return dataSource.sectionModels[index].model
            }
        )
        
        filterTableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        sections.bind(to: filterTableView.rx.items(dataSource: dataSource)).disposed(by: disposeBag)
        
        // on name selected fill up the text field
        Observable.zip(filterTableView.rx.itemSelected, filterTableView.rx.modelSelected(FilterOption.self)).bind { [unowned self] indexPath, name in
            
            if let cell = self.filterTableView.cellForRow(at: indexPath) {
                if let group = self.filters.value!.getGroup(inSection: indexPath.section) {
                    if let option = group.getOption(inPos: indexPath.row) {
                        if let selectedOption = group.getSelectedOption() {
                            if selectedOption.key != option.key {
                                let selectedRow = group.getIndexForOption(withKey: selectedOption.key)
                                if let selectedCell = self.filterTableView.cellForRow(at: IndexPath(row: selectedRow, section: indexPath.section)) {
                                    selectedCell.accessoryType = .none
                                }
                                group.selectOption(with: option.key)
                                cell.accessoryType = .checkmark
                                self.filterTableView.deselectRow(at: indexPath, animated: true)
                            }
                        }
                    }
                }
            }
            }.disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        if delegate != nil {
            delegate?.filterViewControllerWillShow(self)
            filters.accept(delegate?.filterViewControllerFiltersToShow(self))
        }
    }
}

extension FilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .clear
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.tintColor = ThemeSecondaryColor
    }
}
