//
//  ParentFeedViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/17/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa
import RxDataSources

class ParentFeedViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var usersView: UIView!
    @IBOutlet var feedTableView: UITableView!
    @IBOutlet var summaryView: UIView!
    weak var titleView: PageTitleView!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let user:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let events:BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    let sections:BehaviorRelay<[SectionModel<String, JSON>]> = BehaviorRelay(value: [])
    let filters:BehaviorRelay<FilterOptions?> = BehaviorRelay(value: FilterOptions(
        [
            FilterOptionGroup(
                in: 0,
                identifier: "type",
                label: "Type",
                items: [
                    FilterOption(identifier: "Any", label: "Any", selected: true),
                ]
            )
        ]))
    var avatarListView: AvatarListView!
}

extension ParentFeedViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.feedTableView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:  #selector(getFeedOfCurrentUser), for: .valueChanged)
        self.feedTableView.refreshControl = refreshControl
        
        parentSubject
            .subscribe(onNext:{ parent in
                guard let parent = parent else { return }
                if self.titleView != nil {
                    self.titleView.removeFromSuperview()
                }
                self.titleView = UINib(nibName: "PageTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PageTitleView
                self.titleView.frame.size.width = self.summaryView.frame.size.width
                self.titleView.frame.size.height = self.summaryView.frame.size.height
                self.summaryView.addSubview(self.titleView)
                
                if self.avatarListView != nil {
                    self.avatarListView.removeFromSuperview()
                }
                
                self.avatarListView = UINib(nibName: "AvatarListView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? AvatarListView
                self.avatarListView.frame.size.width = self.usersView.frame.size.width
                self.avatarListView.frame.size.height = self.usersView.frame.size.height
                do {
                    var items:JSON = [parent.object]
                    try items.merge(with: parent["kids"])
                    self.avatarListView.handleAvatarTap = self.handleAvatarTap
                    self.avatarListView.kids = items
                    if self.user.value != nil {
                        self.avatarListView.selectedKid = self.user.value
                    }
                    self.usersView.addSubview(self.avatarListView)
                } catch let error as NSError{
                    print(error.localizedDescription)
                }
            }).disposed(by: disposeBag)
        
        user.subscribe(onNext: { [self] user in
            guard let user = user else { return }
            self.getFeed(for: user)
            self.titleView.user.accept(user)
            self.titleView.title.accept("\(user["firstName"].stringValue)'s Feed")
        }).disposed(by: disposeBag)
        
        events.subscribe(onNext: { events in
            var weeks:[(order:Int, key:String, item:JSON)] = []
            var sections:[SectionModel<String, JSON>] = []
            var index = 0
            for event in events {
                if let createdOnString = event["createdOn"].string {
                    if let createdOn = Date.from(string: createdOnString, format: .DateTime) {
                        var dateComponents = Calendar.current.dateComponents([.year, .weekOfYear], from: createdOn)
                        let weekKey = "\(dateComponents.year!)-\(dateComponents.weekOfYear!)"
                        if weeks.contains(where: { item in item.key == weekKey }) == false {
                            let weekLabel = "\(createdOn.to(format: .Month)!) \(Date.getWeekDate(for: .Sunday, of: createdOn)!.to(format: .Day)!) - \(Date.getWeekDate(for: .Saturday, of: createdOn)!.to(format: .Day)!), \(createdOn.to(format: .Year)!)"
                            var weekJson = JSON(parseJSON: "{\"events\":[]}")
                            weekJson["label"].string = weekLabel
                            weekJson["events"].arrayObject!.append(event)
                            weeks.append((order: index, key: weekKey, item: weekJson))
                            index = index + 1
                        } else {
                            if let weekIndex = weeks.firstIndex(where: { item in item.key == weekKey }) {
                                var weekJson = weeks[weekIndex].item
                                weekJson["events"].arrayObject!.append(event)
                                weeks[weekIndex].item = weekJson
                            }
                        }
                    }
                }
            }
            weeks.sort(by: { item1, item2 in item1.order < item2.order })
            for (_,_,item) in weeks {
                let sectionModel = SectionModel(model: item["label"].stringValue, items: item["events"].arrayValue)
                sections.append(sectionModel)
            }
            self.sections.accept(sections)
        }).disposed(by: disposeBag)
        
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, JSON>>(
            configureCell: { dataSource, table, indexPath, item in
                let cell = table.dequeueReusableCell(withIdentifier: "EventItem")!
                if let cell = cell as? FeedEventTableViewCell {
                    cell.event.accept(item)
                }
                return cell
            },
            titleForHeaderInSection:{ dataSource, index in
                return dataSource.sectionModels[index].model
            }
        )
        sections.bind(to: feedTableView.rx.items(dataSource: dataSource)).disposed(by: disposeBag)
        
        // set default background when no events are available
        events.asObservable()
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [unowned self] missions in
                if missions.count == 0 {
                    let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.feedTableView.bounds.size.width, height: self.feedTableView.bounds.size.height))
                    noDataLabel.text = "No events to show"
                    noDataLabel.textColor = ThemePrimaryColor
                    noDataLabel.textAlignment = NSTextAlignment.center
                    self.feedTableView.backgroundView = noDataLabel
                } else {
                    self.feedTableView.backgroundView = nil
                }
            }).disposed(by: disposeBag)
        
        feedTableView.rowHeight = UITableView.automaticDimension
        
        Api.instance().rxInvokeCbeEntity(
            using: "Get",
            to: "entity/EventType"
            ).subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        json["value"].arrayValue.forEach({item in
                            self.filters.value!.getGroup(inSection: 0)?.options.append(FilterOption(identifier: item["value"].stringValue, label: item["description"].stringValue))
                        })
                    }
                case .failure(let error as NSError):
                    print(error.localizedDescription)
                }
            }).disposed(by: disposeBag)
        
        filters.subscribe(onNext: { [self] filters in
            guard let _ = filters, let user = self.user.value else { return }
            self.getFeed(for: user)
        }).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    private func handleAvatarTap(user: JSON!) -> Void {
        if (self.user.value != nil && self.user.value!["id"].stringValue != user!["id"].stringValue) || self.user.value == nil {
            self.user.accept(user)
        }
    }
    
    @objc func getFeedOfCurrentUser() {
        if let user = self.user.value {
            self.getFeed(for: user)
        }
    }
    
    private func getFeed(for user: JSON) {
        
        let path = user["$class"].stringValue == "Parent" ? "userAccount/self/feed" : "userAccount/self/feed/\(user["id"].stringValue)"
        
        let typeFilter = filters.value!.getSelectedOption(inGroup: "type")!.key
        
        let filter = typeFilter == "Any" ? "" : "type/value eq '\(typeFilter)'"
        
        ModalLogo.showModal(with: "Loading...", in: Api.instance().rxInvoke(
            using: "Get",
            to: path,
            with: RequestOptions(
                select: "id,title,description,$createdOn as 'createdOn',type/value as 'type',sourceId, type/description as 'typeDescription'",
                filter: filter,
                orderBy: "$createdOn desc",
                count: true
        )))
        .observeOn(ConcurrentMainScheduler.instance)
        .subscribe(onNext: { result in
            switch result {
            case .success(let data):
                if let json = data {
                    self.events.accept(json["value"].arrayValue)
                }
            case .failure(let error):
                Notification.showError(using: error)
            }
            self.feedTableView.refreshControl?.endRefreshing()
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FilterFeedSegue" {
            if let controller = segue.destination as? FilterViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentFeedViewController: FilterViewControllerDelegate {
    
    func filterViewControllerWillShow(_ controller: FilterViewController) {
        UIController.instance().hideTabBar()
    }
    
    func filterViewControllerDidCancel(_ controller: FilterViewController) {
        UIController.instance().showTabBar()
    }
    
    func filterViewController(_ controller: FilterViewController, didFilter filters: FilterOptions!) {
        UIController.instance().showTabBar()
        self.filters.accept(filters)
    }
    
    func filterViewControllerFiltersToShow(_ controller: FilterViewController) -> FilterOptions! {
        return filters.value!.copy() as? FilterOptions
    }
    
    func filterViewControllerTitle(_ controller: FilterViewController) -> String {
        return "Filter Feed"
    }
}
