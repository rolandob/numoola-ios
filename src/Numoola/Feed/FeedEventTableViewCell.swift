//
//  FeedEventTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/18/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class FeedEventTableViewCell: UITableViewCell {
    // controls
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subTitleLabel: UILabel!
    @IBOutlet var contentLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let event:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
}

extension FeedEventTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        event.subscribe(onNext: { [self] event in
            guard let event = event else { return }
            self.titleLabel.text = event["typeDescription"].stringValue
            self.subTitleLabel.text = "\(Date.from(string: event["createdOn"].stringValue, format: .DateTime)!.to(withFormat: "MMMM dd, yyyy")!) - \(event["title"].stringValue)"
            self.contentLabel.text = event["description"].stringValue
        }).disposed(by: disposeBag)
    }
}
