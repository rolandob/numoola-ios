//
//  SignInViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/20/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import AWSMobileClient
import RxSwift
import RxCocoa
import SwiftyJSON
import LocalAuthentication

class SignInViewController: UIViewController {
    // controls
    @IBOutlet weak var usernameTextField: NMLTextField!
    @IBOutlet weak var passwordTextField: NMLTextField!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var signUpButton: NMLButton!
    @IBOutlet var faceIdImageView: UIImageView!
    @IBOutlet var touchIdImageView: UIImageView!
    // fields
    var disposeBag = DisposeBag()
    let userConfirmedSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "user-confirmed-data")
    let userFaceIdSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "use-face-id")
    let userTouchIdSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "use-touch-id")
    let faceIdEnabled:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let touchIdEnabled:BehaviorRelay<Bool> = BehaviorRelay(value: false)
}

extension SignInViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // set labels
        submitButton.setTitle(NSLocalizedString("SignIn", comment: ""), for: .normal)
        signUpButton.setTitle(NSLocalizedString("SignUp", comment: ""), for: .normal)
        // bind submit button isEnabled
        Observable.combineLatest(usernameTextField.valid, passwordTextField.valid) { $0 && $1 }.bind(to: submitButton.rx.isEnabled).disposed(by: disposeBag)
        // submit tap
        // button bindings
        submitButton.rx.tap.subscribe(onNext: {[self] _ in
            self.login(with: self.usernameTextField.value.value!, andPassword: self.passwordTextField.value.value!)
        }).disposed(by: disposeBag)
        
        userConfirmedSubject.subscribe(onNext: { [self] data in
            guard let data = data, let username = data["username"].string, let password = data["password"].string else { return }
            self.login(with: username , andPassword: password)
        }).disposed(by: disposeBag)
        
        userFaceIdSubject.subscribe(onNext: { [self] value in
            guard let value = value else { return }
            if let username = value["username"].string {
                self.faceIdLogin(with: username)
            }
        }).disposed(by: disposeBag)
        
        userTouchIdSubject.subscribe(onNext: { [self] value in
            guard let value = value else { return }
            if let username = value["username"].string {
                self.touchIdLogin(with: username)
            }
        }).disposed(by: disposeBag)
        
        faceIdEnabled.asObservable().map({ value in !value}).bind(to: faceIdImageView.rx.isHidden).disposed(by: disposeBag)
        touchIdEnabled.asObservable().map({ value in !value}).bind(to: touchIdImageView.rx.isHidden).disposed(by: disposeBag)
    }
    
    @IBAction func faceIdAction(_ sender: UITapGestureRecognizer) {
        if UserDefaults.standard.bool(forKey: FaceIDEnabled) == true {
            if let username = UserDefaults.standard.object(forKey: FaceIDUserName) {
                self.faceIdLogin(with: username as! String)
            }
        }
    }
    
    @IBAction func touchIdAction(_ sender: UITapGestureRecognizer) {
        if UserDefaults.standard.bool(forKey: TouchIDEnabled) == true {
            if let username = UserDefaults.standard.object(forKey: TouchIDUserName) {
                self.touchIdLogin(with: username as! String)
            }
        }
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if UserDefaults.standard.bool(forKey: FaceIDEnabled) == true {
            if let _ = UserDefaults.standard.object(forKey: FaceIDUserName) {
                faceIdEnabled.accept(true)
            }
        } else {
            faceIdEnabled.accept(false)
        }
        
        if UserDefaults.standard.bool(forKey: TouchIDEnabled) == true {
            if let _ = UserDefaults.standard.object(forKey: TouchIDUserName) {
                touchIdEnabled.accept(true)
            }
        } else {
            touchIdEnabled.accept(false)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ParentOnBoardingSegue" {
            if let controller = segue.destination as? ParentOnBoardingViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "KidOnBoardingSegue" {
            if let controller = segue.destination as? KidOnBoardingViewController {
                controller.delegate = self
            }
        }
    }
    
    private func faceIdLogin(with username: String) {
        let context = LAContext()
        var error: NSError?
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            error: &error) {
            context.evaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                localizedReason: "Sign In as \"\(username)\"",
                reply: { [unowned self] (success, error) -> Void in
                    DispatchQueue.main.async {
                        if( success ) {
                            if let password = KeyChain.loadPassword(service: "www.numoola.com", account: username) {
                                self.login(with: username, andPassword: password)
                            } else {
                                Notification.show(title: "Something went wrong", message: "Failed to obtain your credentials, please enter your username and password", type: .Error)
                            }
                        } else {
                            Notification.show(title: "Something went wrong", message: "Failed to verify your identity, please enter your username and password", type: .Error)
                        }
                    }
            })
        }
    }
    
    private func touchIdLogin(with username: String) {
        let context = LAContext()
        var error: NSError?
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            error: &error) {
            context.evaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                localizedReason: "Sign In as \"\(username)\"",
                reply: { [unowned self] (success, error) -> Void in
                    DispatchQueue.main.async {
                        if( success ) {
                            if let password = KeyChain.loadPassword(service: "www.numoola.com", account: username) {
                                self.login(with: username, andPassword: password)
                            } else {
                                Notification.show(title: "Something went wrong", message: "Failed to obtain your credentials, please enter your username and password", type: .Error)
                            }
                        } else {
                            Notification.show(title: "Something went wrong", message: "Failed to verify your identity, please enter your username and password", type: .Error)
                        }
                    }
            })
        }
    }
    
    private func login(with username: String, andPassword password: String) {
        ModalLogo.showModal(with: NSLocalizedString("SignIn-Message", comment: ""), in: Observable<Any?>.create({observer in
            Api.instance().loginUser(username, password, UserFetchOptions)
                .observeOn(ConcurrentMainScheduler.instance)
                .subscribe(onNext: { [self] result in
                    observer.on(.completed)
                    // save password in keychain
                    KeyChain.savePassword(service: "www.numoola.com", account: username, data: password)
                    // clear the fields
                    self.usernameTextField.reset()
                    self.passwordTextField.reset()
                    Notification.isPushNotificationsEnabled().subscribe(onNext: { enabled in
                        guard enabled else { return }
                        Notification.registerForRemoteNotifications()
                    }).disposed(by: self.disposeBag)
                    switch result {
                    case .success(let data):
                        // modal.dismiss(animated: true)
                        if let json = data {
                            // get theme information
                            Api.instance().rxInvoke(using: "Get", to: "userAccount/self/theme")
                                .map({ themeResult -> String in
                                    switch themeResult {
                                    case .success(let data):
                                        if let themeJson = data {
                                            return themeJson["value"].stringValue
                                        }
                                    case .failure(let error as NSError):
                                        print("Could not retrieve user theme error: \(error.localizedDescription)")
                                    }
                                    return "default-theme"
                                })
                                .observeOn(ConcurrentMainScheduler.instance)
                                .subscribe(onNext: { themeKey in
                                    print("Set current user theme to \(themeKey)")
                                    // set current user theme
                                    UITheme.instance().setCurrent(themeKey)
                                    DataHub.instance().publishData(for: "userLoggedIn", with: json)
                                    if json["$class"] == "Parent" {
                                        // publish parent json
                                        DataHub.instance().publishData(for: "parent", with: json)   
                                        if UserDefaults.standard.bool(forKey: "\(OnBoardingSeen)-\(username)") == false {
                                            UserDefaults.standard.set(true, forKey: "\(OnBoardingSeen)-\(username)")
                                            self.performSegue(withIdentifier: "ParentOnBoardingSegue", sender: nil)
                                        } else {
                                            self.performSegue(withIdentifier: "parentHomeSegue", sender: nil)
                                        }
                                    } else {
                                        // publish parent json
                                        DataHub.instance().publishData(for: "kid", with: json)
                                        if UserDefaults.standard.bool(forKey: "\(OnBoardingSeen)-\(username)") == false {
                                            UserDefaults.standard.set(true, forKey: "\(OnBoardingSeen)-\(username)")
                                            self.performSegue(withIdentifier: "KidOnBoardingSegue", sender: nil)
                                        } else {
                                            self.performSegue(withIdentifier: "kidHomeSegue", sender: nil)
                                        }
                                    }
                                }).disposed(by: self.disposeBag)
                            // get membership information
                            Api.instance().rxInvoke(using: "Get", to: "userAccount/self/membership")
                                .subscribe(onNext: { result in
                                    switch result {
                                    case .success(let data):
                                        if let membership = data {
                                            DataHub.instance().publishData(for: "membership", with: membership)
                                        }
                                    case .failure:
                                        DataHub.instance().publishData(for: "membership", with: JSON(parseJSON: "{}"))
                                    }
                                }).disposed(by: self.disposeBag)
                            // get membership default configuration
                            Api.instance().rxInvoke(using: "Get", to: "configuration/membership")
                                .subscribe(onNext: { result in
                                    switch result {
                                    case .success(let data):
                                        if let config = data {
                                            DataHub.instance().publishData(for: "membership-config", with: config)
                                        }
                                    case .failure:
                                        if let settings = NumoolaSettings.get() {
                                            if let jsonResult = settings as? Dictionary<String, AnyObject> {
                                                let membershipTrialDuration = (jsonResult["MembershipTrialDuration"] as? Int)!
                                                let membershipMonthlyAmount = (jsonResult["MembershipMonthlyAmount"] as? Double)!
                                                DataHub.instance().publishData(for: "membership-config", with: JSON(dictionaryLiteral: ("membershipTrialDuration", membershipTrialDuration),("membershipMonthlyAmount", membershipMonthlyAmount)))
                                            }
                                        }
                                    }
                                }).disposed(by: self.disposeBag)
                        }
                    case .failure(let error):
                        Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
                    }
                }).disposed(by: self.disposeBag)
            
            return Disposables.create()
        })).subscribe({ result in }).disposed(by: self.disposeBag)
    }
    
    @IBAction func openPrivacyPolicy() {
        if let url = URL(string: "http://www.numoola.com/terms-of-use/") {
            UIApplication.shared.open(url)
        }
    }
}

extension SignInViewController: ParentOnBoardingViewControllerDelegate {
    func parentOnBoardingViewControllerDidFinish(_ controller: ParentOnBoardingViewController) {
        self.performSegue(withIdentifier: "parentHomeSegue", sender: nil)
    }
    
    func parentOnBoardingViewControllerDidSkip(_ controller: ParentOnBoardingViewController) {
        self.performSegue(withIdentifier: "parentHomeSegue", sender: nil)
    }
}

extension SignInViewController: KidOnBoardingViewControllerDelegate {
    func kidOnBoardingViewControllerDidFinish(_ controller: KidOnBoardingViewController) {
        self.performSegue(withIdentifier: "kidHomeSegue", sender: nil)
    }
    
    func kidOnBoardingViewControllerDidSkip(_ controller: KidOnBoardingViewController) {
        self.performSegue(withIdentifier: "kidHomeSegue", sender: nil)
    }
}
