//
//  KidOnBoardningViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/29/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol KidOnBoardingViewControllerDelegate:class {
    func kidOnBoardingViewControllerDidFinish(_ controller: KidOnBoardingViewController)
    func kidOnBoardingViewControllerDidSkip(_ controller: KidOnBoardingViewController)
}

class KidOnBoardingViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var skipButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var pageControl: UIPageControl!
    // delegate
    weak var delegate: KidOnBoardingViewControllerDelegate?
    // fields
    let disposeBag = DisposeBag()
    weak var wizardController:WizardPageViewController?
    let navigationSubject:BehaviorRelay<String> = BehaviorRelay(value: "KidOnBoardingHome")
}

extension KidOnBoardingViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        navigationItem.setHidesBackButton(true, animated: false)
        
        pageControl.currentPageIndicatorTintColor = ThemeSecondaryColor
        pageControl.pageIndicatorTintColor = ThemePrimaryColor
        pageControl.numberOfPages = 3
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        skipButton.rx.tap.subscribe(onNext: { [self] _ in
            self.delegate?.kidOnBoardingViewControllerDidSkip(self)
        }).disposed(by: disposeBag)
        
        pageControl.rx.controlEvent(.valueChanged).subscribe(onNext: { [self] _ in
            self.wizardController?.navigate(to: self.pageControl.currentPage)
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WizardSegue" {
            if let controller = segue.destination as? WizardPageViewController {
                controller.customDelegate = self
                controller.dataSource = self
                self.wizardController = controller
                self.wizardController?.handleFinish = self.handleFinish
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func handleFinish() {
        self.delegate?.kidOnBoardingViewControllerDidFinish(self)
    }
}

extension KidOnBoardingViewController: WizardPageViewControllerDelegate {
    func wizardPageViewControllerDidFinishNavigating(_ controller: WizardPageViewController, to identifier: String, with index: Int, withController controllerInstance: UIViewController) {
        print(index)
        self.pageControl.currentPage = index
    }
    
    func wizardPageViewControllerNavigationListener(_ controller: WizardPageViewController) -> BehaviorRelay<String> {
        return navigationSubject
    }
    
    func wizardPageViewControllerPageIdentifiers(_ controller: WizardPageViewController) -> [String] {
        return ["KidOnBoardingHome", "KidOnBoardingMissions", "KidOnBoardingGoals"]
    }
}

extension KidOnBoardingViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let controller = viewController as? PageViewController else {
            return nil
        }
        wizardController?.previous(from: controller.wizardIdentifier)
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let controller = viewController as? PageViewController else {
            return nil
        }
        wizardController?.next(from: controller.wizardIdentifier)
        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 3
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
