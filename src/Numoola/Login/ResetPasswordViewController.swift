//
//  ResetPasswordViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/8/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AWSMobileClient

class ResetPasswordViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var userNameTextField: NMLTextField!
    @IBOutlet var newpasswordTextField: NMLTextField!
    @IBOutlet var confirmNewPasswordTextField: NMLTextField!
    @IBOutlet var codeTextField: NMLTextField!
    @IBOutlet var codeButton: NMLButton!
    // fields
    var disposeBag = DisposeBag()
}

extension ResetPasswordViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            AWSMobileClient.sharedInstance().confirmForgotPassword(username: self.userNameTextField.value.value!, newPassword: self.newpasswordTextField.value.value!, confirmationCode: self.codeTextField.value.value!, completionHandler: { (result, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
                    } else {
                        Notification.show(title: "Password was reset", message: "Congratulations! Your password was successfully reset, you can now login into your Numoola account", type: .Success)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            })
        }).disposed(by: disposeBag)
        
        userNameTextField.valid.bind(to: codeButton.rx.isEnabled).disposed(by: disposeBag)
        
        let isAllValid: Observable<Bool> = Observable.combineLatest(userNameTextField.valid, newpasswordTextField.valid, confirmNewPasswordTextField.valid, codeTextField.valid) { $0 && $1 && $2 && $3 }
        isAllValid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
        codeButton.rx.tap.subscribe(onNext: { _ in
            AWSMobileClient.sharedInstance().forgotPassword(username: self.userNameTextField.value.value!, completionHandler: { (result, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
                    } else {
                        Notification.show(title: "Reset Code Sent", message: "An sms was sent to your configured phone number with a code, please enter the code and your new password to complete resetting your password", type: .Success)
                    }
                }
                })
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
