//
//  TrophyCollectionViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/23/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxCocoa
import RxSwift

class TrophyCollectionViewCell: UICollectionViewCell {
    // controls
    @IBOutlet weak var mainView: UIView!
    var trophyView: TrophyView!
    // fields
    var disposeBag = DisposeBag()
    let trophy:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
}

extension TrophyCollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        trophy.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] item in
            guard let item = item else { return }
            if self.trophyView != nil {
                self.trophyView.removeFromSuperview()
            }
            self.trophyView = (UINib(nibName: "TrophyView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TrophyView)
            self.trophyView.trophy.accept(item)
            self.mainView.addSubview(self.trophyView)
        }).disposed(by: disposeBag)
    }
}
