//
//  TrophyDetailsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/23/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

protocol TrophyDetailsViewControllerDelegate:class {
    func trophyDetailsViewControllerTrophyToShow(_ controller: TrophyDetailsViewController) -> JSON?
}

class TrophyDetailsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var backButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var progressDescription: UILabel!
    // delegate
    weak var delegate: TrophyDetailsViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
}

extension TrophyDetailsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.progressView.tintColor = ThemePrimaryColor
        self.progressView.transform = CGAffineTransform(scaleX: 1.0, y: 2.0)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal, using: "rewards")!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal, using: "rewards")!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal, using: "rewards")!)
        }).disposed(by: disposeBag)
        
        backButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        if let trophy = delegate?.trophyDetailsViewControllerTrophyToShow(self) {
            if trophy["level"].exists() == false || trophy["level"].stringValue == "" {
                self.imageView.image = UIImage(named: "reward-\(trophy["type"].stringValue)-default")
            } else {
                self.imageView.image = UIImage(named: "reward-\(trophy["type"].stringValue)-\(trophy["level"].stringValue)")
            }
            if trophy["points"].exists() == false {
                self.progressView.isHidden = true
                self.progressDescription.isHidden = true
            } else {
                self.progressView.isHidden = false
                self.progressDescription.isHidden = false
                let maxPoints = trophy["maxPoints"].doubleValue
                let points = trophy["points"].doubleValue
                self.progressView.setProgress(Float(points / maxPoints), animated: true)
            }
            // set descriptions
            self.descriptionLabel.text = trophy["typeDescription"].stringValue
            if trophy["description"].exists() == true {
                self.progressDescription.text = trophy["description"].stringValue.replacingOccurrences(of: "{points}", with: trophy["points"].stringValue)
            }
        }
    }
}
