//
//  KidTrophiesViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/23/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class KidTrophiesViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var backButton: NMLBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    // fields
    var disposeBag = DisposeBag()
    let kidSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "kid")
    let trophies:BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    var tappedTrophy:JSON? = nil
}

extension KidTrophiesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal, using: "rewards")!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal, using: "rewards")!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal, using: "rewards")!)
        }).disposed(by: disposeBag)
        
        backButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        Api.instance().rxInvoke(using: "Get", to: "userAccount/self/trophies")
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.trophies.accept(json["value"].arrayValue)
                    }
                case .failure(let error):
                    Notification.showError(using: error)
                }
            }).disposed(by: disposeBag)
        
        trophies.observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] trophies in self.collectionView.reloadData()}).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TrophySegue" {
            if let controller = segue.destination as? TrophyDetailsViewController {
                controller.delegate = self
            }
        }
    }
}

extension KidTrophiesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trophies.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrophyCell", for: indexPath) as! TrophyCollectionViewCell
        let item = trophies.value[indexPath.row]
        cell.trophy.accept(item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .clear
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? TrophyCollectionViewCell {
            if let trophy = cell.trophy.value {
                self.tappedTrophy = trophy
                self.performSegue(withIdentifier: "TrophySegue", sender: nil)
            }
        }
    }
}

extension KidTrophiesViewController: TrophyDetailsViewControllerDelegate {
    func trophyDetailsViewControllerTrophyToShow(_ controller: TrophyDetailsViewController) -> JSON? {
        return tappedTrophy
    }
}
