//
//  KidHomeSummaryViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/1/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import FontAwesome_swift
import RxSwift
import RxCocoa

class ParentHomeKidSummaryViewController: UIViewController {
    // controls
    @IBOutlet var summaryView: UIView!
    @IBOutlet var dollarIcon: FontAwesomeImageView!
    @IBOutlet var missionIcon: FontAwesomeImageView!
    @IBOutlet var allowanceIcon: FontAwesomeImageView!
    @IBOutlet var goalsIcon: FontAwesomeImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var missionProgressBar: UIProgressView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var missionProgressLabel: UILabel!
    @IBOutlet var moolaBalanceLabel: UILabel!
    @IBOutlet var pointsIcon: FontAwesomeImageView!
    @IBOutlet var allowanceAmountLabel: UILabel!
    @IBOutlet var allowanceOccurrenceLabel: UILabel!
    @IBOutlet var spendingGoalsLabel: UILabel!
    @IBOutlet var charityGoalsLabel: UILabel!
    @IBOutlet var pointsLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let kidSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "summary-selected-kid")
    let goals: BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    let missions: BehaviorRelay<JSON?> = BehaviorRelay(value:nil)
    let allowance: BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let balances: BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let havePhoto: BehaviorRelay<Bool> = BehaviorRelay(value: false)
}

extension ParentHomeKidSummaryViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        summaryView.backgroundColor = .white
        summaryView.layer.cornerRadius = 20
        
        dollarIcon.imageColor = ThemePrimaryColor
        dollarIcon.awakeFromNib()
        
        missionIcon.imageColor = ThemePrimaryColor
        missionIcon.awakeFromNib()
        
        allowanceIcon.imageColor = ThemePrimaryColor
        allowanceIcon.awakeFromNib()
        
        goalsIcon.imageColor = ThemePrimaryColor
        goalsIcon.awakeFromNib()
        
        pointsIcon.imageColor = ThemePrimaryColor
        pointsIcon.awakeFromNib()
        
        avatarImageView.backgroundColor = ThemePrimaryColor
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height/2
        
        missionProgressBar.progressTintColor = ThemePrimaryColor
        
        let progressViewHeight: CGFloat = 4.0
        
        // Set progress view height
        let transformScale = CGAffineTransform(scaleX: 1.0, y: progressViewHeight)
        self.missionProgressBar.transform = transformScale
        
        // Set progress round corners
        self.missionProgressBar.layer.cornerRadius = progressViewHeight
        self.missionProgressBar.clipsToBounds = true
        
        kidSubject
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext:{ [self] kid in
                guard let kid = kid else { return }
                self.setTitleLabel(for: kid)
                self.setAvatarImage(for: kid)
                self.getBalances(for: kid)
                self.getMissionsProgress(for: kid)
                self.getAllowance(for: kid)
                self.getGoals(for: kid)
                self.getPointsEarned(for: kid)
            }).disposed(by: disposeBag)
        
        goals.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] goals in
            self.spendingGoalsLabel.text = "\(goals.filter({goal in goal["type"].stringValue == "Spending"}).count) Spending Goals"
            self.charityGoalsLabel.text = "\(goals.filter({goal in goal["type"].stringValue == "Charity"}).count) Charity Goals"
        }).disposed(by: disposeBag)
        
        missions.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] missions in
            guard let missions = missions else { return }
            self.missionProgressLabel.text = "\(missions["completedMissions"].int!)/\(missions["missions"].arrayValue.count) Completed"
            let totalMissions = missions["missions"].array!.count
            let completedMissions = missions["completedMissions"].int!
            self.missionProgressBar.setProgress(totalMissions > 0 ? Float(Float(completedMissions) / Float(totalMissions)) : 0, animated: true)
        }).disposed(by: disposeBag)
        
        allowance.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] allowance in
            guard let allowance = allowance else { return }
            let labels = Allowance.getAllowanceLabels(for: allowance)
            self.allowanceAmountLabel.text = labels.label
            self.allowanceOccurrenceLabel.text = labels.next
        }).disposed(by: disposeBag)
        
        balances.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] balances in
            guard let balances = balances else { return }
            let savingsBalance:Double = balances["saving"]["availableBalance"].double!
            let spendingBalance:Double = balances["spending"]["availableBalance"].double!
            let givingBalance:Double = balances["giving"]["availableBalance"].double!
            let spendingGoalsBalance: Double = balances["spendingGoals"]["availableBalance"].double!
            let givingGoalsBalance: Double = balances["givingGoals"]["availableBalance"].double!
            let totalBalance:Double = savingsBalance + spendingBalance + givingBalance + spendingGoalsBalance + givingGoalsBalance
            let behavior = NSDecimalNumberHandler(roundingMode: .plain, scale: 2, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: true)
            let totalBalanceRounded = NSDecimalNumber(value: totalBalance).rounding(accordingToBehavior: behavior)
            
            let loadingProcess = LoadingProcess(minValue: 0, maxValue: totalBalanceRounded.floatValue)
            
            loadingProcess.simulateLoading(
                toValue: totalBalanceRounded.floatValue,
                step: totalBalanceRounded.floatValue / Float(40),
                valueChanged: { currentValue in
                    let roundedValue = NSDecimalNumber(value: currentValue).rounding(accordingToBehavior: behavior)
                    self.moolaBalanceLabel.text = "$ \(roundedValue)"
            },
                completion: { value in
                    self.moolaBalanceLabel.text = "$ \(totalBalanceRounded)"
            }
            )
        }).disposed(by: disposeBag)
        
        havePhoto.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] havePhoto in
            self.avatarImageView.backgroundColor = havePhoto ? .white : ThemePrimaryColor
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // refresh data
        if let kid = kidSubject.value {
            DataHub.instance().publishData(for: "summary-selected-kid", with: kid)
        }
    }
    
    private func setTitleLabel(for kid:JSON){
        titleLabel.text = "\(kid["firstName"].stringValue)'s Overview"
        usernameLabel.text = "@\(kid["loginUsername"].stringValue)"
    }
    
    private func setAvatarImage(for kid:JSON) {
        if kid["photo"].exists() {
            Api.instance().downloadPhoto(for: kidSubject.value!, withField: "photo", completionHandler: {
                data, error in
                if let error = error as NSError? {
                    print(error.localizedDescription)
                    self.havePhoto.accept(false)
                } else {
                    DispatchQueue.main.async {
                        if let base64Data = data {
                            let decodedData = Data(base64Encoded: base64Data)
                            self.avatarImageView.image = UIImage(data: decodedData!)
                            self.havePhoto.accept(true)
                        }
                    }
                }
            })
        } else {
            avatarImageView.image = UIImage(named: "Avatar")
            havePhoto.accept(false)
        }
    }
    
    private func getBalances(for kid: JSON) {
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/kids/\(kidSubject.value!["id"].string!)/balance",
            api: "numoola-q2")
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.balances.accept(json)
                    }
                case .failure(let error as NSError):
                    print(error.userInfo)
                }
            }).disposed(by: disposeBag)
    }
    
    private func getMissionsProgress(for kid: JSON) {
        let startDate = Date.getWeekDate(for: .Sunday)!.to(format: .Persistent)!
        let endDate = Date.getWeekDate(for: .Saturday)!.to(format: .Persistent)!
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/chores/\(kid["id"].stringValue)",
            with: RequestOptions(filter: "startDate ge \(startDate) and startDate le \(endDate)", orderBy: "startDate asc"))
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        Api.instance().rxInvoke(
                            using: "Get",
                            to: "userAccount/self/chores/\(kid["id"].stringValue)",
                            with: RequestOptions(filter: "status/value eq 'Completed' and startDate ge \(startDate) and startDate le \(endDate)", orderBy: "startDate asc"))
                            .subscribe(onNext: { [self] result2 in
                                switch result2 {
                                case .success(let data2):
                                    if let json2 = data2 {
                                        var missions = JSON.init(parseJSON: "{}")
                                        missions["completedMissions"].int = json2["value"].arrayValue.count
                                        missions["missions"].arrayObject = json["value"].arrayValue
                                        self.missions.accept(missions)
                                    }
                                case .failure(let error2 as NSError):
                                    print(error2.userInfo)
                                }
                            }).disposed(by: self.disposeBag)
                    }
                case .failure(let error as NSError):
                    print(error.userInfo)
                }
            }).disposed(by: disposeBag)
    }
    
    private func getAllowance(for kid: JSON) {
        // get allowance for selected kid
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/kids/\(kidSubject.value!["id"].string!)/allowance")
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.allowance.accept(json)
                    }
                case .failure(let error as NSError):
                    print(error.userInfo)
                }
            }).disposed(by: disposeBag)
    }
    
    private func getGoals(for kid: JSON) {
        // get spending goals for selected kid
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/kids/\(kid["id"].string!)/goals",
            with: RequestOptions(
                select: "id,type/value as 'type'",
                filter: "status/value ne 'Done' and status/value ne 'Deleted'"
        )).subscribe(onNext: { [self] result in
            switch result {
            case .success(let data):
                if let json = data {
                    self.goals.accept(json["value"].arrayValue)
                }
            case .failure(let error as NSError):
                print(error.userInfo)
            }
        }).disposed(by: disposeBag)
    }
    
    private func getPointsEarned(for kid: JSON) {
        pointsLabel.text = "\(kid["rewardsAccount"]["balance"].int!)"
    }
    
    @IBAction func handleMoolaStack(recognizer:UITapGestureRecognizer) {
        print("Show moola details")
    }
    
    @IBAction func handleMissionsStack(recognizer: UITapGestureRecognizer){
        print("Missions details")
    }
    
    @IBAction func handleAllowanceStack(recognizer: UITapGestureRecognizer){
        print("Allowance details")
    }
    
    @IBAction func handleGoalsStack(recognizer: UITapGestureRecognizer){
        print("Goals details")
    }
    
    @IBAction func handlePointsStack(recognizer: UITapGestureRecognizer){
        print("Points details")
    }
    
    @IBAction func handleSendMoola(_ sender: Any) {
        print("Send Moola")
    }
}
