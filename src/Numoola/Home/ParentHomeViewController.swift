//
//  HomeViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/21/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa
import BetterSegmentedControl
import AWSMobileClient

class ParentHomeViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var householdContainer: UIView!
    @IBOutlet var navigationBar: BetterSegmentedControl!
    @IBOutlet var mentorsContainer: UIView!
    private var householdController:ParentHomeHouseholdViewController!
    private var mentorsController:ParentHomeMentorsViewController!
    @IBOutlet var filterMentorButton: NMLBarButtonItem!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let parentPhoneVerifiedSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "userPhoneVerified")
}

extension ParentHomeViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
        }).disposed(by: disposeBag)
        
        navigationBar.segments = LabelSegment.segments(withTitles: ["My Household", "Our Village"], numberOfLines: 1, normalBackgroundColor: .clear, normalFont: UIFont.systemFont(ofSize: 15, weight: .light), normalTextColor: .white, selectedBackgroundColor: ThemeSecondaryColor, selectedFont: UIFont.systemFont(ofSize: 16, weight: .bold), selectedTextColor: .white)
        
        navigationBar.options = [.cornerRadius(20),.bouncesOnChange(true), .alwaysAnnouncesValue(true), .backgroundColor(.clear),.indicatorViewBorderWidth(CGFloat(5)),.indicatorViewBackgroundColor(ThemeSecondaryColor)]
        
        filterMentorButton.hide()
        
        navigationBar.rx.controlEvent(.valueChanged).observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] _ in
            switch self.navigationBar.index {
            case 0:
                if self.householdContainer.alpha == 0 {
                    self.filterMentorButton.hide()
                    UIView.animate(withDuration: 0.5, animations: {
                        self.householdContainer.alpha = 0
                        self.householdContainer.alpha = 1
                        self.mentorsContainer.alpha = 1
                        self.mentorsContainer.alpha = 0
                    })
                }
            case 1:
                if self.mentorsContainer.alpha == 0 {
                    self.filterMentorButton.show()
                    UIView.animate(withDuration: 0.5, animations: {
                        self.mentorsContainer.alpha = 0
                        self.mentorsContainer.alpha = 1
                        self.householdContainer.alpha = 1
                        self.householdContainer.alpha = 0
                    })
                    self.mentorsController.getMentors()
                }
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        parentSubject.subscribe(onNext:{ message in
            guard let message = message else { return }
            print("Welcome Home, \(message["firstName"].string!)")
        }).disposed(by: disposeBag)
        
        parentPhoneVerifiedSubject.subscribe(onNext: { data in
            guard let data = data else { return }
            if data["verified"].stringValue == "false" {
                
                let okAction = UIAlertAction(title: "Ok",
                                             style: .default) { (action) in
                                                self.performSegue(withIdentifier: "VerifyPhoneSegue", sender: nil)
                }
                let cancelAction = UIAlertAction(title: "Cancel",
                                                 style: .cancel) { (action) in
                }
                
                let alert = UIAlertController(title: "Phone Number not verified",
                                              message: "Your phone number is not verified, do you want to verify it now?",
                                              preferredStyle: .alert)
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
        }).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        if self.mentorsController != nil {
            self.mentorsController.getMentors()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "parentHomeHouseholdSegue" {
            if let householdController = segue.destination as? ParentHomeHouseholdViewController {
                self.householdController = householdController
            }
        } else if segue.identifier == "parentHomeMentorsSegue" {
            if let mentorsController = segue.destination as? ParentHomeMentorsViewController {
                self.mentorsController = mentorsController
            }
        } else if segue.identifier == "FilterMentorsSegue" {
            if let controller = segue.destination as? FilterViewController {
                controller.delegate = self.mentorsController
            }
        } else if segue.identifier == "VerifyPhoneSegue" {
            if let controller = segue.destination as? VerifyPhoneNumberViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentHomeViewController: VerifyPhoneNumberViewControllerDelegate {
    func verifyPhoneNumberViewControllerDidCancel(_ controller: VerifyPhoneNumberViewController) {
        UIController.instance().showTabBar()
    }
    
    func verifyPhoneNumberViewControllerDidVerify(_ controller: VerifyPhoneNumberViewController) {
        DataHub.instance().publishData(for: "userPhoneVerified", with: JSON(dictionaryLiteral: ("verified", "true")))
        UIController.instance().showTabBar()
    }
    
    func verifyPhoneNumberViewControllerWillShow(_ controller: VerifyPhoneNumberViewController) {
        UIController.instance().hideTabBar()
    }
    
    func verifyPhoneNumberViewControllerShouldSendCode(_ controller: VerifyPhoneNumberViewController) -> Bool {
        return true
    }
}
