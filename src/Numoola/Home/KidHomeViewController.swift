//
//  KidHomeViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/24/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class KidHomeViewController: UIViewController {
    // controls
    @IBOutlet var allowanceViewContainer: UIView!
    @IBOutlet var avatarViewContainer: UIView!
    @IBOutlet var rewardsViewContainer: UIView!
    @IBOutlet var kidRocketImageView: UIImageView!
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var missionsView: UIView!
    @IBOutlet var missionProgressBar: UIProgressView!
    @IBOutlet var missionProgressLabel: UILabel!
    @IBOutlet var missionTitleLabel: UILabel!
    @IBOutlet var moolaViewContainer: UIView!
    // fields
    var disposeBag = DisposeBag()
    let kidSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "kid")
    let missionsData:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let allowance:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    let balances:BehaviorRelay<JSON?> = BehaviorRelay(value: nil)
    weak var rewardsView: RewardsView!
    weak var avatarView: AvatarView!
    weak var allowanceView: AllowanceView!
    weak var moolaView: MoolaView!
}

extension KidHomeViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
        }).disposed(by: disposeBag)
        
        missionsView.backgroundColor = .clear
        missionProgressLabel.textColor = ThemePrimaryColor
        missionTitleLabel.textColor = ThemePrimaryColor
        
        missionProgressBar.progressTintColor = ThemePrimaryColor
        
        let progressViewHeight: CGFloat = 2.0
        
        // Set progress view height
        let transformScale = CGAffineTransform(scaleX: 1.0, y: progressViewHeight)
        self.missionProgressBar.transform = transformScale
        
        // Set progress round corners
        self.missionProgressBar.layer.cornerRadius = progressViewHeight
        self.missionProgressBar.clipsToBounds = true
        
        UIView.animate(withDuration: 0.8, delay: 0, options: [.curveEaseIn], animations: {
            self.kidRocketImageView.center.y = self.kidRocketImageView.bounds.height * -2
            self.kidRocketImageView.layoutIfNeeded()
        })
        UIView.animate(withDuration: 0.5, delay: 0.8, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.9, options: [.curveEaseIn], animations: {
            self.kidRocketImageView.center.y = self.kidRocketImageView.center.y + 50
            self.kidRocketImageView.layoutIfNeeded()
        })
        
        missionsData
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] data in
            guard let missions = data else { return }
            
            self.missionProgressLabel.text = "\(missions["completedMissions"].int!)/\(missions["missions"].array!.count) Completed"
            let totalMissions = missions["missions"].array!.count
            let completedMissions = missions["completedMissions"].int!
            self.missionProgressBar.setProgress(totalMissions > 0 ? Float(Float(completedMissions) / Float(totalMissions)) : 0, animated: true)
        }).disposed(by: disposeBag)
        
        allowance
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] allowance in
            guard let allowance = allowance else { return }
            self.allowanceView.allowance.accept(allowance)
        }).disposed(by: disposeBag)
        
        balances
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] balances in
                guard let balances = balances else { return }
                let savingsBalance:Double = balances["saving"]["availableBalance"].double!
                let spendingBalance:Double = balances["spending"]["availableBalance"].double!
                let givingBalance:Double = balances["giving"]["availableBalance"].double!
                let spendingGoalsBalance: Double = balances["spendingGoals"]["availableBalance"].double!
                let givingGoalsBalance: Double = balances["givingGoals"]["availableBalance"].double!
                let totalBalance:Double = savingsBalance + spendingBalance + givingBalance + spendingGoalsBalance + givingGoalsBalance
                self.moolaView.moola.accept(totalBalance)
            }).disposed(by: disposeBag)
        
        kidSubject.subscribe(onNext: { [self] kid in
            guard let kid = kid else { return }
            self.navigationItem.title = "@\(kid["loginUsername"].stringValue)"
            if self.avatarView == nil {
                // create avatar view
                self.avatarView = UINib(nibName: "AvatarView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? AvatarView
                self.avatarView.frame.size.width = self.avatarViewContainer.frame.size.width
                self.avatarView.frame.size.height = self.avatarViewContainer.frame.size.height
                self.avatarView.kid = kid
                self.avatarView.avatarSelected.accept(true)
                self.avatarView.handleAvatarTap = self.handleAvatarTap
                self.avatarViewContainer.addSubview(self.avatarView)
            } else {
                self.avatarView.removeFromSuperview()
                // create avatar view
                self.avatarView = UINib(nibName: "AvatarView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? AvatarView
                self.avatarView.frame.size.width = self.avatarViewContainer.frame.size.width
                self.avatarView.frame.size.height = self.avatarViewContainer.frame.size.height
                self.avatarView.kid = kid
                self.avatarView.avatarSelected.accept(true)
                self.avatarView.handleAvatarTap = self.handleAvatarTap
                self.avatarViewContainer.addSubview(self.avatarView)
            }
            
            if self.allowanceView == nil {
                // create allowance view
                self.allowanceView = UINib(nibName: "AllowanceView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? AllowanceView
                self.allowanceView.frame.size.width = self.allowanceViewContainer.frame.size.width
                self.allowanceView.frame.size.height = self.allowanceViewContainer.frame.size.height
                self.allowanceView.handleAllowanceTap = self.handleAllowanceTap
                self.allowanceViewContainer.addSubview(self.allowanceView)
            }
            
            if self.rewardsView == nil {
                // create rewards view
                self.rewardsView = UINib(nibName: "RewardsView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? RewardsView
                self.rewardsView.frame.size.width = self.rewardsViewContainer.frame.size.width
                self.rewardsView.frame.size.height = self.rewardsViewContainer.frame.size.height
                self.rewardsView.handleRewardsTap = self.handleRewardsTap
                self.rewardsViewContainer.addSubview(self.rewardsView)
            }
            
            if self.moolaView == nil {
                // create moola view
                self.moolaView = UINib(nibName: "MoolaView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? MoolaView
                self.moolaView.frame.size.width = self.moolaViewContainer.frame.size.width
                self.moolaView.frame.size.height = self.moolaViewContainer.frame.size.height
                self.moolaView.handleMoolaTap = self.handleMoolaTap
                self.moolaViewContainer.addSubview(self.moolaView)
            }
            
            self.getMissionsProgress()
            self.getAllowance()
            self.getMoolaBalance()
            
            // set the points
            self.rewardsView.points.accept(kid["rewardsAccount"]["balance"].intValue)
            
        }).disposed(by: disposeBag)
        
    }
    
    private func handleMoolaTap() -> Void {
        print("Handle moola tap")
    }
    
    private func handleAvatarTap(_ kid: JSON?) -> Void {
        print("Handle avatar tap")
        // performSegue(withIdentifier: "KidProfileSegue", sender: nil)
    }
    
    private func handleAllowanceTap() -> Void {
        print("Handle allowance tap")
    }
    
    private func handleRewardsTap() -> Void {
        print("Handle rewards tap")
        performSegue(withIdentifier: "KidRewardsSegue", sender: nil)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.getMissionsProgress()
    }
    
    private func getMoolaBalance() {
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/balance",
            api: "numoola-q2")
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.balances.accept(json)
                    }
                case .failure(let error):
                    Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
                }
            }).disposed(by: disposeBag)
    }
    
    private func getAllowance() {
        // get allowance
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/allowance")
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.allowance.accept(json)
                    }
                case .failure(let error as NSError):
                    print(error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    
    private func getMissionsProgress() {
        let startDate = Date.getWeekDate(for: .Sunday)!.to(format: .Persistent)!
        let endDate = Date.getWeekDate(for: .Saturday)!.to(format: .Persistent)!
        Api.instance().rxInvoke(
            using: "Get",
            to: "userAccount/self/kidChores",
            with: RequestOptions(filter: "startDate ge \(startDate) and startDate le \(endDate)", orderBy: "startDate asc"))
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        Api.instance().rxInvoke(
                            using: "Get",
                            to: "userAccount/self/kidChores",
                            with: RequestOptions(filter: "status/value eq 'Completed' and startDate ge \(startDate) and startDate le \(endDate)", orderBy: "startDate asc"))
                            .subscribe(onNext: { [self] result2 in
                                switch result2 {
                                case .success(let data2):
                                    if let json2 = data2 {
                                        var info = JSON.init(parseJSON: "{}")
                                        info["completedMissions"].int = json2["value"].array!.count
                                        info["missions"].arrayObject = json["value"].array!
                                        self.missionsData.accept(info)
                                    }
                                case .failure(let error2 as NSError):
                                    print(error2.localizedDescription)
                                }
                            }).disposed(by: self.disposeBag)
                    }
                case .failure(let error as NSError):
                    print(error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "KidProfileSegue" {
            if let controller = segue.destination as? KidProfileViewController {
                controller.delegate = self
            }
        }
    }
}

extension KidHomeViewController: KidProfileViewControllerDelegate {
    func kidProfileViewControllerDidCancel(_ controller: KidProfileViewController) {
        UIController.instance().showTabBar()
    }
    
    func kidProfileViewController(_ controller: KidProfileViewController, didFinishUpdating kid: JSON) {
        UIController.instance().showTabBar()
    }
    
    func kidProfileViewControllerWillShow(_ controller: KidProfileViewController) {
        UIController.instance().hideTabBar()
    }
    
    
}
