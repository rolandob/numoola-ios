//
//  ParentHouseholdViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/30/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa
                                                            
class ParentHomeHouseholdViewController: UIViewController {
    // controls
    @IBOutlet var kidsView: UIView!
    @IBOutlet var parentSummaryView: UIView!
    @IBOutlet var kidSummaryView: UIView!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    private var parentSummaryController:ParentHomeSummaryViewController!
    private var kidSummaryController:ParentHomeKidSummaryViewController!
    var avatarListView: AvatarListView!
}

extension ParentHomeHouseholdViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        parentSubject
            .subscribe(onNext:{ [self] parent in
                guard let parent = parent else { return }
                if self.avatarListView != nil {
                    self.avatarListView.removeFromSuperview()
                }
                print("Welcome Home Household, \(parent["firstName"].string!)")
                self.avatarListView = UINib(nibName: "AvatarListView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? AvatarListView
                self.avatarListView.frame.size.width = self.kidsView.frame.size.width
                self.avatarListView.frame.size.height = self.kidsView.frame.size.height
                
                do {
                    var items:JSON = [parent.object]
                    try items.merge(with: parent["kids"])
                    self.avatarListView.handleAvatarTap = self.handleAvatarTap
                    self.avatarListView.kids = items
                    self.kidsView.addSubview(self.avatarListView)
                } catch let error as NSError{
                    print(error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    
    private func handleAvatarTap(user: JSON!) -> Void {
        if user!["$class"].string! == "Parent" {
            UIView.animate(withDuration: 0.5, animations: {
                self.parentSummaryView.alpha = 0
                self.parentSummaryView.alpha = 1
                self.kidSummaryView.alpha = 0
            })
        } else {
            // publish selected kid
            DataHub.instance().publishData(for: "summary-selected-kid", with: user)
            if self.kidSummaryView.alpha == 0 {
                UIView.animate(withDuration: 0.5, animations: {
                    self.kidSummaryView.alpha = 0
                    self.kidSummaryView.alpha = 1
                    self.parentSummaryView.alpha = 0
                })
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "parentHomeParentSummarySegue" {
            if let parentSummaryController = segue.destination as? ParentHomeSummaryViewController {
                self.parentSummaryController = parentSummaryController
            }
        } else if segue.identifier == "parentHomeKidSummarySegue" {
            if let kidSummaryController = segue.destination as? ParentHomeKidSummaryViewController {
                self.kidSummaryController = kidSummaryController
            }
        }
    }
}
