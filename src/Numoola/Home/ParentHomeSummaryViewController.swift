//
//  ParentHomeSummaryViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/1/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import FontAwesome_swift
import RxSwift
import RxCocoa

class ParentHomeSummaryViewController: UIViewController {
    // controls
    @IBOutlet var summaryView: UIView!
    @IBOutlet var pointsView: UIView!
    @IBOutlet var moolaView: UIView!
    @IBOutlet var icon: FontAwesomeImageView!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var pendingBalanceLabel: UILabel!
    @IBOutlet var pointsLabel: UILabel!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var summaryLabel: UILabel!
    @IBOutlet var usernameLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let havePhoto: BehaviorRelay<Bool> = BehaviorRelay(value: false)
}

extension ParentHomeSummaryViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        summaryView.backgroundColor = .white
        summaryView.layer.cornerRadius = 20
        
        pointsView.backgroundColor = .white
        pointsView.layer.cornerRadius = 20
        
        moolaView.backgroundColor = .white
        moolaView.layer.cornerRadius = 20
        
        icon.imageColor = ThemePrimaryColor
        icon.awakeFromNib()
        
        avatarImageView.backgroundColor = ThemePrimaryColor
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height/2
        
        parentSubject
            .subscribe(onNext:{ [self] parent in
                guard let parent = parent else { return }
                self.getBalance()
                self.getPointsBalance(for: parent)
                self.setAvatarImage(for: parent)
                self.summaryLabel.text = "\(parent["firstName"].stringValue)'s Overview"
                self.usernameLabel.text = "@\(parent["loginUsername"].stringValue)"
            }).disposed(by: disposeBag)
        
        havePhoto.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] havePhoto in
            self.avatarImageView.backgroundColor = havePhoto ? .white : ThemePrimaryColor
        }).disposed(by: disposeBag)
    }
    
    private func getBalance() {
        Api.instance().rxInvoke(
            using: "Get",
            to: "/userAccount/self/balance",
            api: "numoola-q2")
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let json = data {
                        self.balanceLabel.text = "$\(json["availableBalance"].number!) total"
                        self.pendingBalanceLabel.text = "$\(json["pendingBalance"].number!) total"
                    }
                case .failure(let error as NSError):
                    print(error.userInfo)
                }
            }).disposed(by: disposeBag)
    }
    
    private func setAvatarImage(for parent:JSON) {
        if parent["photo"].exists() {
            Api.instance().downloadPhoto(for: parentSubject.value!, withField: "photo", completionHandler: {
                data, error in
                if let error = error as NSError? {
                    print(error.localizedDescription)
                    self.havePhoto.accept(false)
                } else {
                    DispatchQueue.main.async {
                        if let base64Data = data {
                            let decodedData = Data(base64Encoded: base64Data)
                            self.avatarImageView.image = UIImage(data: decodedData!)
                            self.havePhoto.accept(true)
                        }
                    }
                }
            })
        } else {
            avatarImageView.image = UIImage(named: "Avatar")
            havePhoto.accept(false)
        }
    }
    
    private func getPointsBalance(for parent: JSON) {
        pointsLabel.text = "\(parent["rewardsAccount"]["balance"].number!)"
    }
    
    @IBAction func handleAddMoola(_ sender: Any) {
        print("Add Moola")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddMoolaSegue" {
            if let controller = segue.destination as? AddMoolaViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentHomeSummaryViewController: AddMoolaViewControllerDelegate {
    func addMoolaViewControllerDidCancel(_ controller: AddMoolaViewController) {
        UIController.instance().showTabBar()
    }
    
    func addMoolaViewController(_ controller: AddMoolaViewController, didFinishAdding amount: Double) {
        print("Account funded with \(amount)")
        UIController.instance().showTabBar()
    }
    
    func addMoolaViewControllerWillShow(_ controller: AddMoolaViewController) {
        UIController.instance().hideTabBar()
    }
}
