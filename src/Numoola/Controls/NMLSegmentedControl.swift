//
//  NMLSegmentedControl.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/31/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NMLSegmentedControl: UISegmentedControl {
    // fields
    var disposeBag = DisposeBag()
    private var _theme: String? = ""
}

extension NMLSegmentedControl {
    override func awakeFromNib() {
        super.awakeFromNib()
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.tintColor = theme.getColor(with: SegmentedControlStyles.TintColor, for: .normal, using: self._theme!)
            self.backgroundColor = theme.getColor(with: SegmentedControlStyles.BackgroundColor, for: .normal, using: self._theme! )
            self.setTitleTextAttributes([NSAttributedString.Key.font:theme.getFont(with: SegmentedControlStyles.TitleFont, for: .selected, using: self._theme!)!], for: .selected)
        }).disposed(by: disposeBag)
    }
    
    @IBInspectable
    var themePrefix: String {
        get {
            return _theme!
        }
        set {
            _theme = newValue
        }
    }
}
