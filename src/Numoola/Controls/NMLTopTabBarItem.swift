//
//  NMLTopTabBarItem.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/29/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit

class NMLTopTabBarItem: UITabBarItem {
    
    override func setTitleTextAttributes(_ attributes: [NSAttributedString.Key : Any]?, for state: UIControl.State) {
        if var attributes = attributes {
            attributes[NSAttributedString.Key.font] = UIFont.systemFont(ofSize: 18)
        }
        
    }
}
