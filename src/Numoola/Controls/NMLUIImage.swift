//
//  NMLUIImage.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/29/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    static func from(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}
