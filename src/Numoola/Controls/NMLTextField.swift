import Foundation
import UIKit
import FontAwesome_swift
import RxSwift
import RxCocoa
import InputMask

protocol FieldCheckComponent:class {
    func setHidden(_ hidden:Bool)
}

@IBDesignable
public class NMLTextField: UITextField {
    
    private var _icon: String?
    private var _theme: String? = ""
    private var _placeholderAsLabel = true
    private var _readOnly = false
    private var _required = false
    private var _name: String = "field"
    private var _pattern: String = ""
    private var _useMaskValidator = true
    // rx bag
    var disposeBag = DisposeBag()
    var validateDisposeBag: DisposeBag! = DisposeBag()
    // subjects
    let valid:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let errors:BehaviorRelay<[String]?> = BehaviorRelay(value:nil)
    
    @IBOutlet weak var fieldLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet var validators:[FieldValidator] = []

    var patternValid:Bool = true
    var value:BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    @IBInspectable
    var useMaskValidator: Bool {
        get {
            return _useMaskValidator
        }
        set {
            _useMaskValidator = newValue
        }
    }
    
    @IBInspectable
    var pattern: String {
        get {
            return _pattern
        }
        set {
            _pattern = newValue
        }
    }
    
    @IBInspectable
    var themePrefix: String {
        get {
            return _theme!
        }
        set {
            _theme = newValue
        }
    }
    
    @IBInspectable
    var icon: String {
        get {
            return _icon!
        }
        set {
            _icon = newValue
        }
    }
    
    @IBInspectable
    var usePlaceholderAsLabel: Bool {
        get {
            return _placeholderAsLabel
        }
        set {
            _placeholderAsLabel = newValue
        }
    }
    
    @IBInspectable
    var readOnly: Bool {
        get {
            return _readOnly
        }
        set {
            _readOnly = newValue
        }
    }
    
    @IBInspectable
    var required: Bool {
        get {
            return _required
        }
        set {
            _required = newValue
        }
    }
    
    @IBInspectable
    var name: String {
        get {
            return _name
        }
        set {
            _name = newValue
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        if let maskedDelegate = delegate as? MaskedTextFieldDelegate {
            maskedDelegate.delegate = self
        } else {
            delegate = self
        }
        
        borderStyle = .none
        clearButtonMode = .whileEditing
        leftViewMode = .always
        rightViewMode = .always
        
        if isSecureTextEntry == true {
            let rightView = UINib(nibName: "FieldSecureCheckView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? FieldSecureCheckView
            rightView?.showSecure.subscribe(onNext: { [self] value in
                self.isSecureTextEntry = !value
            }).disposed(by: disposeBag)
            self.rightView = rightView
        } else {
            let rightView = UINib(nibName: "FieldCheckView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? FieldCheckView
            self.rightView = rightView
        }
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            
            if let icon = self._icon {
                let iconColor:UIColor = theme.getColor(with: TextFieldStyles.IconColor, for: .normal, using: self._theme!)!
                let iconImage = UIImage.fontAwesomeIcon(code: icon, style: .solid, textColor: iconColor, size: ThemeTextFieldIconSize)
                let imageView = UIImageView(image: iconImage)
                self.leftView = imageView
            }
            
            self.font = theme.getFont(with: TextFieldStyles.TextFont, for: .normal)
            let placeHolderColor = theme.getColor(with: TextFieldStyles.PlaceholderTextColor, for: .normal, using: self._theme!)!
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: placeHolderColor])
            self.textColor = theme.getColor(with: TextFieldStyles.TextColor, for: .normal, using: self._theme!)!
        }).disposed(by: disposeBag)
        
        self.addTarget(self, action:#selector(nmlTextChanged(target:)), for: .editingChanged)
        
        // set the field label's text from placeholder if desired and font
        if fieldLabel != nil {
            fieldLabel.font = UIFont.systemFont(ofSize: 12, weight: .light)
            if _placeholderAsLabel == true {
                fieldLabel.text = placeholder
            }
            fieldLabel.numberOfLines = 0
        }
        // set the error label font
        if errorLabel != nil {
            errorLabel.font = UIFont.systemFont(ofSize: 12, weight: .light)
            errorLabel.textColor = ThemeErrorColor
            errorLabel.isHidden = true
            errorLabel.numberOfLines = 0
        }
        // set initial visibility of the field label
        updateLabelVisibility()
        
        valid.subscribe(onNext: { [self] value in
            if let view = self.rightView as? FieldCheckComponent {
                view.setHidden(!value)
            }
        }).disposed(by: disposeBag)
        
        errors.subscribe(onNext: { [self] items in
            guard let items = items else {
                if let view = self.rightView as? FieldCheckComponent {
                    view.setHidden(true)
                }
                if self.errorLabel != nil {
                    self.errorLabel.isHidden = true
                }
                return
            }
            if self.errorLabel != nil {
                if items.count > 0 {
                    self.errorLabel.text = items.joined(separator: "\n")
                    self.errorLabel.isHidden = false
                } else {
                    self.errorLabel.isHidden = true
                }
            }
            // set valid flag
            self.valid.accept(items.count == 0)
        }).disposed(by: disposeBag)
    }
    
    func validateTimer(_ inmediate: Bool) {
        validateDisposeBag = nil
        validateDisposeBag = DisposeBag()
        
        Observable<Int>.timer(inmediate == true ? 0.0 : 1.0, scheduler: ConcurrentMainScheduler.instance).subscribe({ [self] event in
            self.validate()
        }).disposed(by: validateDisposeBag)
    }
    
    func validate() {
        var errors:[String] = []
        if _required == true && text?.isEmpty == true {
            errors.append("The \(_name) is required")
        }
        if let _ = delegate as? MaskedTextFieldDelegate {
            if patternValid == false {
                errors.append("The \(_name) is invalid")
            }
        } else if _pattern.isEmpty == false && text?.isEmpty == false {
            let patternPredicate = NSPredicate(format:"SELF MATCHES %@", _pattern)
            if patternPredicate.evaluate(with: text) == false {
                errors.append("The \(_name) is invalid")
            }
        }
        
        for validator in validators {
            let result = validator.validate(_name, text ?? "")
            if result.valid == false {
                errors.append(result.message)
            }
        }
        // set errors
        self.errors.accept(errors)
    }
    
    func updateLabelVisibility(){
        if fieldLabel != nil {
            let hidden = self.text != nil ? self.text!.isEmpty : true
            UIView.animate(withDuration: 0.3, animations: {
                self.fieldLabel.isHidden = hidden
            }, completion: { _ in
                self.fieldLabel.isHidden = hidden
            })
        }
    }
    
    @objc func nmlTextChanged(target: Any?) {
        setValue(text)
        if text?.isEmpty == true {
            validateTimer(true)
        } else {
            validateTimer(false)
        }
    }

    func setValue(_ text:String?, _ updateText:Bool = true) {
        guard let text = text else { return }
        if updateText == true {
            if let delegate = self.delegate as? MaskedTextFieldDelegate {
                delegate.put(text: text, into: self)
            } else {
                self.text = text
            }
            validate()
        }
        self.value.accept(text)
        updateLabelVisibility()
    }
    
    func reset() {
        setValue("")
        self.errors.accept(nil)
    }
}

extension NMLTextField: MaskedTextFieldDelegateListener {
    
    public func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        patternValid = complete
        if _useMaskValidator == false {
            patternValid = true
        }
        setValue(value, false)
        if complete == true || value == "" {
            validateTimer(true)
        } else {
            validateTimer(false)
        }
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return _readOnly == true ? false : true
    }
}
