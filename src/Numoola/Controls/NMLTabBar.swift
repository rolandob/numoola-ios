//
//  NMLTabBar.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/31/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit

class NMLTabBar: UITabBar {

    override func awakeFromNib() {
        super.awakeFromNib()
        tintColor = ThemePrimaryColor
    }

}
