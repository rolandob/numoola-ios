//
//  NMLSeparatorView.swift
//  Numoola
//
//  Created by Rolando Bermudez on 6/2/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import UIKit

class NMLSeparatorView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = ThemePrimaryColor
    }
}
