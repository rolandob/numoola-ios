//
//  NMLBarButtonItem.swift
//  Numoola
//
//  Created by Rolando Bermudez on 5/26/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome_swift
import RxSwift
import RxCocoa

@IBDesignable public class NMLBarButtonItem: UIBarButtonItem {
    private var _icon: String?
    private var _theme: String? = ""
    var disposeBag = DisposeBag()
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.tintColor = theme.getColor(with: BarButtonItemStyles.TintColor, for: .normal)
            // set icon
            if let icon = self._icon {
                let size = theme.getSize(with: BarButtonItemStyles.Size, for: .normal, using: self._theme!)
                let iconImage = UIImage.fontAwesomeIcon(code: icon, style: .solid, textColor: theme.getColor(with: BarButtonItemStyles.TintColor, for: .normal, using: self._theme!)!, size: CGSize(width: Double(size!), height: Double(size!)))
                self.image = iconImage
            }
        }).disposed(by: disposeBag)
    }
    
    @IBInspectable
    var icon: String {
        get {
            return _icon!
        }
        set {
            _icon = newValue
        }
    }
    
    @IBInspectable
    var themePrefix: String {
        get {
            return _theme!
        }
        set {
            _theme = newValue
        }
    }
    
    func hide() {
        self.isEnabled = false
        self.tintColor = .clear
    }
    
    func show() {
        self.isEnabled = true
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.tintColor = theme.getColor(with: BarButtonItemStyles.TintColor, for: .normal)
        }).disposed(by: disposeBag)
    }
}
