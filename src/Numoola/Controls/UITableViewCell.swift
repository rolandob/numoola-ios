//
//  UITableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/8/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit

extension UITableViewCell {
    func prepareDisclosureIndicator() {
        for case let button as UIButton in subviews {
            let image = button.backgroundImage(for: .normal)?.withRenderingMode(.
                alwaysTemplate)
            button.setBackgroundImage(image, for: .normal)
        }
    }
}
