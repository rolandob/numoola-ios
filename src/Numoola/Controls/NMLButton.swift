import UIKit
import RxSwift
import RxCocoa

public class NMLButton: UIButton {
    // fields
    var disposeBag = DisposeBag()
    private var _theme: String? = ""
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable
    var themePrefix: String {
        get {
            return _theme!
        }
        set {
            _theme = newValue
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setTitleColor(ThemeDisabledColor, for: .disabled)
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.titleLabel?.font = theme.getFont(with: ButtonStyles.Font, for: .normal, using: self._theme!)
            self.setTitleColor(theme.getColor(with: ButtonStyles.FontColor, for: .normal, using: self._theme!), for: .normal)
            self.backgroundColor = theme.getColor(with: ButtonStyles.BackgroundColor, for: .normal, using: self._theme! )
            self.layer.cornerRadius = CGFloat(theme.getSize(with: ButtonStyles.CornerRadius, for: .normal, using: self._theme!)!)
            self.layer.borderColor = theme.getColor(with: ButtonStyles.BorderColor, for: .normal, using: self._theme!)!.cgColor
            self.layer.borderWidth = CGFloat(theme.getSize(with: ButtonStyles.BorderWith, for: .normal, using: self._theme!)!)
        }).disposed(by: disposeBag)
    }
}
