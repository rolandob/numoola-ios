//
//  TouchIDTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/29/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AWSMobileClient

class TouchIDTableViewCell: UITableViewCell {
    // controls
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var touchSwitch: UISwitch!
    // fields
    var disposeBag = DisposeBag()
    let item:BehaviorRelay<SettingItem?> = BehaviorRelay(value: nil)
}

extension TouchIDTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        item.subscribe(onNext: { [self] item in
            guard let item = item else { return }
            self.titleLabel.text = item.title
            self.descriptionLabel.text = item.description
        }).disposed(by: disposeBag)
        
        touchSwitch.onTintColor = ThemeSecondaryColor
        touchSwitch.rx.controlEvent(.valueChanged).subscribe(onNext: {
            if self.touchSwitch.isOn == true {
                UserDefaults.standard.set(true, forKey: TouchIDEnabled)
                UserDefaults.standard.set(AWSMobileClient.sharedInstance().username, forKey: TouchIDUserName)
            } else {
                UserDefaults.standard.set(false, forKey: TouchIDEnabled)
                UserDefaults.standard.removeObject(forKey: TouchIDUserName)
            }
        }).disposed(by: disposeBag)
        
        if UserDefaults.standard.bool(forKey: TouchIDEnabled) == true {
            if let username = UserDefaults.standard.object(forKey: TouchIDUserName) as? String {
                touchSwitch.isOn = (AWSMobileClient.sharedInstance().username)! == username
            }
        }
    }
}
