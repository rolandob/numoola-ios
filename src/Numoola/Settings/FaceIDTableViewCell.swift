//
//  FaceIDTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/19/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AWSMobileClient

class FaceIDTableViewCell: UITableViewCell {
    // controls
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var faceSwitch: UISwitch!
    // fields
    var disposeBag = DisposeBag()
    let item:BehaviorRelay<SettingItem?> = BehaviorRelay(value: nil)
}

extension FaceIDTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        item.subscribe(onNext: { [self] item in
            guard let item = item else { return }
            self.titleLabel.text = item.title
            self.descriptionLabel.text = item.description
        }).disposed(by: disposeBag)
        
        faceSwitch.onTintColor = ThemeSecondaryColor
        faceSwitch.rx.controlEvent(.valueChanged).subscribe(onNext: {
            if self.faceSwitch.isOn == true {
                UserDefaults.standard.set(true, forKey: FaceIDEnabled)
                UserDefaults.standard.set(AWSMobileClient.sharedInstance().username, forKey: FaceIDUserName)
            } else {
                UserDefaults.standard.set(false, forKey: FaceIDEnabled)
                UserDefaults.standard.removeObject(forKey: FaceIDUserName)
            }
        }).disposed(by: disposeBag)
        
        if UserDefaults.standard.bool(forKey: FaceIDEnabled) == true {
            if let username = UserDefaults.standard.object(forKey: FaceIDUserName) as? String {
                faceSwitch.isOn = (AWSMobileClient.sharedInstance().username)! == username
            }
        }
    }
}
