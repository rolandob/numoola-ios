//
//  AchTermsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/6/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import WebKit

protocol AchTermsViewControllerDelegate: class{
    func achTermsViewControllerDidCancel(_ controller: AchTermsViewController)
    func achTermsViewControllerDidAccept(_ controller: AchTermsViewController)
}

class AchTermsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var acceptSwitch: UISwitch!
    @IBOutlet var webView: WKWebView!
    // delegate
    weak var delegate: AchTermsViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
}

extension AchTermsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: {[self] _ in
            self.dismiss(animated: true, completion: nil)
            self.delegate?.achTermsViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: {[self] _ in
            self.dismiss(animated: true, completion: nil)
            self.delegate?.achTermsViewControllerDidAccept(self)
        }).disposed(by: disposeBag)
        
        acceptSwitch.rx.isOn.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
        if let url = URL(string: "http://www.numoola.com/ach-transaction-terms-use/") {
            print(url)
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
}
