//
//  KidSecurityViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/8/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import LocalAuthentication

class KidSecurityViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var backButton: NMLBarButtonItem!
    // fields
    var disposeBag = DisposeBag()
    let items:BehaviorRelay<[SettingItem]> = BehaviorRelay(value: [])
}

extension KidSecurityViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let context = LAContext()
        var settingItems:[SettingItem] = [SettingItem("Change Password", "You can change your account password", "ChangePassword")]
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            switch context.biometryType {
            case .faceID:
                settingItems.insert(SettingItem("Face ID", "Enable Face ID Login for your account", "FaceID"), at: 0)
            case .touchID:
                settingItems.insert(SettingItem("Touch ID", "Enable Touch ID Login for your account", "TouchID"), at: 0)
            case .none:
                print("none")
            @unknown default:
                print("default")
            }
        }
        items.accept(settingItems)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        items.bind(to: tableView.rx.items) { (tv, row, item) in
            if item.key == "FaceID" {
                print("is face id")
                let cell = tv.dequeueReusableCell(withIdentifier: "FaceIDCell", for: IndexPath.init(row: row, section: 0)) as? FaceIDTableViewCell
                cell!.item.accept(item)
                return cell!
            } else if item.key == "TouchID" {
                let cell = tv.dequeueReusableCell(withIdentifier: "TouchIDCell", for: IndexPath.init(row: row, section: 0)) as? TouchIDTableViewCell
                cell!.item.accept(item)
                return cell!
            } else {
                let cell = tv.dequeueReusableCell(withIdentifier: "SettingCell", for: IndexPath.init(row: row, section: 0)) as? SettingTableViewCell
                cell!.item.accept(item)
                return cell!
            }
            }.disposed(by: disposeBag)
        // on name selected fill up the text field
        Observable.zip(tableView.rx.itemSelected, tableView.rx.modelSelected(SettingItem.self)).bind { [unowned self] indexPath, item in
            if let cell = self.tableView.cellForRow(at: indexPath) as? SettingTableViewCell {
                self.tableView.deselectRow(at: indexPath, animated: true)
                self.openSetting(with: cell.item.value!.key)
            }
            }.disposed(by: disposeBag)
        
        backButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    private func openSetting(with key: String) {
        print("Opening setting with key \(key)")
        switch key {
        case "ChangePassword":
            self.performSegue(withIdentifier: "ChangePasswordSegue", sender: nil)
        case "FaceID":
            self.performSegue(withIdentifier: "FaceIDSegue", sender: nil)
        default:
            print("Unimplemented option \(key)")
        }
    }
}

extension KidSecurityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.prepareDisclosureIndicator()
        cell.tintColor = ThemeSecondaryColor
    }
}
