//
//  ParentSettingsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/2/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AWSMobileClient
import SwiftyJSON

class ParentSettingsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var tableView: UITableView!
    // fields
    var disposeBag = DisposeBag()
    let items:BehaviorRelay<[SettingItem]> = BehaviorRelay(value: [
        SettingItem("My Profile", "Personal & Contact Info", "Profile"),
        SettingItem("Security Settings", "Sign In Info", "Security"),
        SettingItem("Membership", "Membership information", "Membership"),
        SettingItem("Moola", "Bank Accounts & Funding", "Moola"),
        SettingItem("Notifications", "Configure NuMoola Notifications", "Notification"),
        SettingItem("Help", "NuMoola FAQs", "Help"),
        SettingItem("Feedback", "Help us build a better experience", "Feedback")
        ])
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
}

extension ParentSettingsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // bind items to table view
        items.bind(to: tableView.rx.items(cellIdentifier: "SettingCell")) { (row, item, cell) in
            if let cell = cell as? SettingTableViewCell {
                cell.item.accept(item)
            }
            }.disposed(by: disposeBag)
        // on name selected fill up the text field
        Observable.zip(tableView.rx.itemSelected, tableView.rx.modelSelected(SettingItem.self)).bind { [unowned self] indexPath, item in
            if let cell = self.tableView.cellForRow(at: indexPath) as? SettingTableViewCell {
                self.tableView.deselectRow(at: indexPath, animated: true)
                self.openSetting(with: cell.item.value!.key)
            }
        }.disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signOutAction(_ sender: Any) {
        // sign out
        AWSMobileClient.sharedInstance().signOut()
        // go to login page
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
            UITheme.instance().setCurrent("default-theme")
            DataHub.instance().getBehaviorRelay(for: "userLoggedIn").accept(nil)
        }
    }
    
    private func openSetting(with key: String) {
        print("Opening setting with key \(key)")
        switch key {
        case "Profile":
            self.performSegue(withIdentifier: "ProfileSegue", sender: nil)
        case "Security":
            self.performSegue(withIdentifier: "SecuritySegue", sender: nil)
        case "Membership":
            self.performSegue(withIdentifier: "MembershipSegue", sender: nil)
        case "Moola":
            self.performSegue(withIdentifier: "MoolaSegue", sender: nil)
        case "Help":
            if let url = URL(string: "http://www.numoola.com/faq/") {
                UIApplication.shared.open(url)
            }
        case "Notification":
            self.performSegue(withIdentifier: "NotificationsSegue", sender: nil)
        case "Feedback":
            self.performSegue(withIdentifier: "FeedbackSegue", sender: nil)
        default:
            print("Unimplemented option \(key)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NotificationsSegue" {
            if let controller = segue.destination as? NotificationsViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentSettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.prepareDisclosureIndicator()
        cell.tintColor = ThemeSecondaryColor
    }
}

extension ParentSettingsViewController: NotificationsViewControllerDelegate {
    func notificationsViewControllerDidCancel(_ controller: NotificationsViewController) {}
    
    func notificationsViewController(_ controller: NotificationsViewController, didFinishSelecting items: [SelectableItem]) {
        ModalLogo.showModal(with: "Updating...", in: { () -> Observable<Result<JSON?, Error>> in
            do {
                var instance = parentSubject.value!["appSettings"]
                items.forEach({ item in
                    instance[item.item["key"].stringValue].bool = item.selected
                })
                let data = try instance.rawData()
                return Api.instance().rxInvoke(using: "Put", to: "userAccount/self/appSettings", payload: data, with: RequestOptions(fetch: UserFetchOptions))
            } catch {
                return Observable.just(.failure(NSError(domain: "", code: 0, userInfo: ["message": "An error has occurred when updating the notification settings, please try again"])))
            }
        }()).observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { result in
            switch result {
            case .success(let data):
                if let parent = data {
                    self.parentSubject.accept(parent)
                    Notification.show(title: "Notification Settings Updated", message: "The Notification settings were updated successfully", type: .Success)
                    // request permissions for push notifications
                    Notification.isPushNotificationsEnabled().observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] enabled in
                        if enabled == false {
                            print("Push notifications not enabled")
                            Notification.requestPushNotificationsPermission(in: self).observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { granted in
                                guard granted else { return }
                                Notification.registerForRemoteNotifications()
                            }).disposed(by: self.disposeBag)
                        }
                    }).disposed(by: self.disposeBag)
                    
                }
            case .failure(let error):
                Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
            }
        }).disposed(by: disposeBag)
    }
    
    func notificationsViewControllerItems(_ controller: NotificationsViewController) -> [SelectableItem] {
    
        guard let parent = parentSubject.value  else {
            return []
        }
        var items:[SelectableItem] = []
        
        items.append(SelectableItem(with: JSON(dictionaryLiteral: ("description", "Email Notifications"), ("key", "emailNotification")), isSelected: parent["appSettings"]["emailNotification"].boolValue))
        items.append(SelectableItem(with: JSON(dictionaryLiteral: ("description", "Push Notifications"), ("key", "pushNotification")), isSelected: parent["appSettings"]["pushNotification"].boolValue))
        items.append(SelectableItem(with: JSON(dictionaryLiteral: ("description", "In App Notifications"), ("key", "inAppNotification")), isSelected: parent["appSettings"]["inAppNotification"].boolValue))
        return items
    }
}
