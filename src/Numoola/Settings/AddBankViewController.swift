//
//  AddBankViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/6/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FontAwesome_swift
import BetterSegmentedControl

class BankModel {
    var alias:String
    var firstName: String
    var lastName: String
    var accountNumber: String
    var routingNumber: String
    var type: String
    
    init(_ alias: String, _ firstName: String, _ lastName: String, _ accountNumber: String, _ routingNumber: String ,_ type: String) {
        self.alias = alias
        self.firstName = firstName
        self.lastName = lastName
        self.accountNumber = accountNumber
        self.routingNumber = routingNumber
        self.type = type
    }
}

protocol AddBankViewControllerDelegate:class {
    func addBankViewControllerDidCancel(_ controller: AddBankViewController)
    func addBankViewController(_ controller: AddBankViewController, didFinishAdding bank: BankModel)
    func addBankViewControllerWillShow(_ controller: AddBankViewController)
}

class AddBankViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var aliasTextField: NMLTextField!
    @IBOutlet var firstNameTextField: NMLTextField!
    @IBOutlet var lastNameTextField: NMLTextField!
    @IBOutlet var accountNumberTextField: NMLTextField!
    @IBOutlet var routingNumberTextField: NMLTextField!
    @IBOutlet var typeControl: BetterSegmentedControl!
    @IBOutlet var bankIcon: FontAwesomeImageView!
    // delegate
    weak var delegate: AddBankViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let type:BehaviorRelay<String?> = BehaviorRelay(value: "Checking")
}

extension AddBankViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        bankIcon.imageColor = ThemeSecondaryColor
        bankIcon.awakeFromNib()
        
        typeControl.segments = LabelSegment.segments(withTitles: ["Spending", "Charity"], numberOfLines: 1, normalBackgroundColor: .clear, normalFont: UIFont.systemFont(ofSize: 15, weight: .light), normalTextColor: ThemeSecondaryColor, selectedBackgroundColor: ThemeSecondaryColor, selectedFont: UIFont.systemFont(ofSize: 16, weight: .bold), selectedTextColor: .white)
        
        typeControl.options = [.cornerRadius(20),.bouncesOnChange(true), .alwaysAnnouncesValue(true), .backgroundColor(.clear),.indicatorViewBorderWidth(CGFloat(5)),.indicatorViewBackgroundColor(ThemeSecondaryColor)]
        
        typeControl.rx.controlEvent(.valueChanged).observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] _ in
            switch self.typeControl.index {
            case 0:
                self.type.accept("Checking")
            case 1:
                self.type.accept("Savings")
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: {[self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.addBankViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: {[self] _ in
            self.performSegue(withIdentifier: "AchTermsSegue", sender: nil)
        }).disposed(by: disposeBag)
        
        let typeValid: Observable<Bool> = type.map{ text -> Bool in text != nil && text!.isEmpty == false }.share(replay: 1)
        let allValid: Observable<Bool> = Observable.combineLatest(aliasTextField.valid, firstNameTextField.valid, lastNameTextField.valid, accountNumberTextField.valid, routingNumberTextField.valid, typeValid) { $0 && $1 && $2 && $3 && $4 && $5 }
        // bind done isEnabled
        allValid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AchTermsSegue" {
            if let controller = segue.destination as? UINavigationController {
                if let childController = controller.viewControllers[0] as? AchTermsViewController {
                    childController.delegate = self
                }
            }
        }
    }
}

extension AddBankViewController: AchTermsViewControllerDelegate {
    func achTermsViewControllerDidCancel(_ controller: AchTermsViewController) {}
    
    func achTermsViewControllerDidAccept(_ controller: AchTermsViewController) {
        self.navigationController?.popViewController(animated: true)
        self.delegate?.addBankViewController(self, didFinishAdding: BankModel(self.aliasTextField.value.value!, self.firstNameTextField.value.value!, self.lastNameTextField.value.value!, self.accountNumberTextField.value.value!, self.routingNumberTextField.value.value!, self.type.value!))
    }
}
