//
//  NotificationTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/7/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NotificationTableViewCell: UITableViewCell {
    // controls
    @IBOutlet var descriptionLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let item:BehaviorRelay<String?> = BehaviorRelay(value: "")
}

extension NotificationTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // bindings
        item.subscribe(onNext: { [self] value in
            self.descriptionLabel.text = value
        }).disposed(by: disposeBag)
    }
}
