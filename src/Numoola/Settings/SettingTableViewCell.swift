//
//  SettingTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/2/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SettingItem {
    var title: String
    var description: String
    var key: String
    
    init(_ title:String, _ description:String, _ key:String) {
        self.title = title
        self.description = description
        self.key = key
    }
}

class SettingTableViewCell: UITableViewCell {
    // controls
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    // fields
    var disposeBag = DisposeBag()
    let item:BehaviorRelay<SettingItem?> = BehaviorRelay(value: nil)
}

extension SettingTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        item.subscribe(onNext: { [self] item in
            guard let item = item else { return }
            self.titleLabel.text = item.title
            self.descriptionLabel.text = item.description
        }).disposed(by: disposeBag)
    }
}
