//
//  FeedbackViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/7/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import StoreKit

class FeedbackViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var backButton: NMLBarButtonItem!
    @IBOutlet var rateButton: NMLButton!
    @IBOutlet var emailButton: NMLButton!
    // fields
    var disposeBag = DisposeBag()
}

extension FeedbackViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        backButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        rateButton.rx.tap.subscribe(onNext: { _ in
            SKStoreReviewController.requestReview()
        }).disposed(by: disposeBag)
        
        emailButton.rx.tap.subscribe(onNext: { _ in
            let email = "feedback@numoola.com"
            if let url = URL(string: "mailto:\(email)") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }).disposed(by: disposeBag)
    }
}
