//
//  NotificationsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/7/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

protocol NotificationsViewControllerDelegate:class {
    func notificationsViewControllerDidCancel(_ controller: NotificationsViewController)
    func notificationsViewController(_ controller: NotificationsViewController, didFinishSelecting items: [SelectableItem])
    func notificationsViewControllerItems(_ controller: NotificationsViewController) -> [SelectableItem]
}

class NotificationsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    // delegate
    weak var delegate: NotificationsViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let items:BehaviorRelay<[SelectableItem]> = BehaviorRelay(value: [])
}

extension NotificationsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        if let delegate = delegate {
            items.accept(delegate.notificationsViewControllerItems(self))
        }
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // bind items to table view
        items.bind(to: tableView.rx.items(cellIdentifier: "NotificationCell")) { (row, item, cell) in
            if let cell = cell as? NotificationTableViewCell {
                cell.item.accept(item.item["description"].stringValue)
                if item.selected == true {
                    cell.accessoryType = .checkmark
                } else {
                    cell.accessoryType = .none
                }
            }
            }.disposed(by: disposeBag)
        // on name selected fill up the text field
        Observable.zip(tableView.rx.itemSelected, tableView.rx.modelSelected(SelectableItem.self)).bind { [unowned self] indexPath, item in
            if let cell = self.tableView.cellForRow(at: indexPath) {
                item.selected = !item.selected
                cell.accessoryType = item.selected == true ? .checkmark : .none
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
            }.disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.notificationsViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.notificationsViewController(self, didFinishSelecting: self.items.value)
        }).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
}

extension NotificationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.prepareDisclosureIndicator()
        cell.tintColor = ThemeSecondaryColor
    }
}
