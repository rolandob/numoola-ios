//
//  ParentChangePasswordViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/3/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AWSMobileClient

class ChangePasswordViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var currentPasswordTextField: NMLTextField!
    @IBOutlet var newPasswordTextField: NMLTextField!
    @IBOutlet var repeatPasswordTextField: NMLTextField!
    // fields
    var disposeBag = DisposeBag()
}

extension ChangePasswordViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            guard let current = self.currentPasswordTextField.value.value, let newPass = self.newPasswordTextField.value.value else { return }
            ModalLogo.showModal(with: "Processing...", in: Observable<Bool>.create({ observer in
                AWSMobileClient.sharedInstance().changePassword(currentPassword: current, proposedPassword: newPass, completionHandler: { error in
                    observer.on(.completed)
                    if let error = error {
                        DispatchQueue.main.async {
                            Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
                        }
                    } else {
                        // update password in keychain
                        KeyChain.savePassword(service: "www.numoola.com", account: AWSMobileClient.sharedInstance().username!, data: newPass)
                        DispatchQueue.main.async {
                            Notification.show(title: "Password Changed", message: "Your account password was changed successfully", type: .Success)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                })
                return Disposables.create()
            })).subscribe(onNext:{ _ in }).disposed(by: self.disposeBag)
        }).disposed(by: disposeBag)
        
        let allValid: Observable<Bool> = Observable.combineLatest(currentPasswordTextField.valid, newPasswordTextField.valid, repeatPasswordTextField.valid) { $0 && $1 && $2 }
        
        // bind done isEnabled
        allValid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
    }
}
