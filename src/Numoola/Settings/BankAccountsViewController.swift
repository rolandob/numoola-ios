//
//  BankAccountsViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/5/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class BankAccountsViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var backButton: NMLBarButtonItem!
    @IBOutlet var addBankButton: NMLBarButtonItem!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let banks:BehaviorRelay<[JSON]> = BehaviorRelay(value: [])
    let accountLimit:BehaviorRelay<Int> = BehaviorRelay(value: 0)
}

extension BankAccountsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        // bind items to table view
        banks.bind(to: tableView.rx.items(cellIdentifier: "BankAccountCell")) { (row, bank, cell) in
            if let cell = cell as? BankAccountTableViewCell {
                cell.bankView.bank.accept(bank)
            }
            }.disposed(by: disposeBag)
        
        backButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        parentSubject.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] parent in
            guard let parent = parent else { return }
            self.accountLimit.accept(parent["bankPlatform"]["externalAccountLimit"].intValue)
        }).disposed(by: disposeBag)
        
        getBankAccounts()
        
        let isLimitValid: Observable<Bool> = Observable.combineLatest(banks, accountLimit).map({ result -> Bool in result.0.count < result.1 }).share(replay: 1)
        isLimitValid.bind(to: addBankButton.rx.isEnabled).disposed(by: disposeBag)
        
        // set default background when no bank accounts are available
        banks.asObservable()
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [unowned self] missions in
                if missions.count == 0 {
                    let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
                    noDataLabel.text = "No bank accounts available"
                    noDataLabel.textColor = ThemePrimaryColor
                    noDataLabel.textAlignment = NSTextAlignment.center
                    self.tableView.backgroundView = noDataLabel
                    self.tableView.separatorStyle = .none
                } else {
                    self.tableView.backgroundView = nil
                    self.tableView.separatorStyle = .singleLine
                }
            }).disposed(by: disposeBag)
    }
    
    private func getBankAccounts() {
        Api.instance().rxInvoke(using: "Get", to: "userAccount/self/bankAccount", with: RequestOptions(select: "id,alias,firstName, lastName, accountNumber, type, status", filter: "status eq 'Verified' or status eq 'Unverified' or status eq 'Locked'"))
            .observeOn(ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let banks = data {
                        self.banks.accept(banks["value"].arrayValue)
                    }
                case .failure(let error):
                    Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
                }
            }).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddBankSegue" {
            if let controller = segue.destination as? AddBankViewController {
                controller.delegate = self
            }
        }
    }
}

extension BankAccountsViewController: UITableViewDelegate {
    
    func contextualVerifyAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Verify", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let bank = self.banks.value[indexPath.row]
            print("Verify bank id: \(bank["id"].stringValue)")
            completionHandler(true)
        })
        action.backgroundColor = ThemeAccentColor1
        return action
    }
    
    func contextualFundAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal,title: "Fund", handler: {(contextAction: UIContextualAction, sourceView: UIView, completionHandler: @escaping (Bool) -> Void) in
            let bank = self.banks.value[indexPath.row]
            print("Fund from bank id: \(bank["id"].stringValue)")
            completionHandler(true)
        })
        action.backgroundColor = ThemeAccentColor1
        return action
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actions:[UIContextualAction] = []
        let bank = self.banks.value[indexPath.row]
        let status = bank["status"].stringValue
        switch status {
        case "Unverified":
            actions.append(self.contextualVerifyAction(forRowAtIndexPath: indexPath))
        case "Verified":
            actions.append(self.contextualFundAction(forRowAtIndexPath: indexPath))
        default:
            print("Unknow status")
        }
        let swipeConfig = UISwipeActionsConfiguration(actions: actions)
        swipeConfig.performsFirstActionWithFullSwipe = false
        return swipeConfig
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension BankAccountsViewController: AddBankViewControllerDelegate {
    func addBankViewControllerDidCancel(_ controller: AddBankViewController) {}
    func addBankViewControllerWillShow(_ controller: AddBankViewController) {}
    
    func addBankViewController(_ controller: AddBankViewController, didFinishAdding bank: BankModel) {
        ModalLogo.showModal(with: "Adding...", in: { () -> Observable<Result<JSON?, Error>> in
            do {
                let instance = JSON(dictionaryLiteral:
                    ("$class", "BankAccount"),
                    ("alias", bank.alias),
                    ("routingNumber", bank.routingNumber),
                    ("accountNumber", bank.accountNumber),
                    ("firstName", bank.firstName),
                    ("lastName", bank.lastName),
                    ("type", bank.type),
                    ("loaded", true)
                )
                let data = try instance.rawData()
                return Api.instance().rxInvoke(using: "Post", to: "userAccount/self/bankAccount", payload: data)
            } catch {
                return Observable.just(.failure(NSError(domain: "", code: 0, userInfo: ["message": "An error has occurred when adding the Bank, please try again"])))
            }
        }()).subscribe(onNext: { result in
            switch result {
            case .success(let data):
                if let _ = data {
                    self.getBankAccounts()
                    Notification.show(title: "Bank Account Added", message: "Congratulations! The Bank Account was added successfully", type: .Success)
                }
            case .failure(let error):
                Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
            }
        }).disposed(by: disposeBag)
    }
}
