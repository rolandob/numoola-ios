//
//  ParentProfileViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/2/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FontAwesome_swift
import SwiftyJSON

class ParentProfileViewController: UIViewController {
    // controllers
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var firstNameTextField: NMLTextField!
    @IBOutlet var lastNameTextField: NMLTextField!
    @IBOutlet var emailTextField: NMLTextField!
    @IBOutlet var dobTextField: NMLTextField!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
    let photo:BehaviorRelay<UIImage?> = BehaviorRelay(value: UIImage(named: "Avatar"))
    private let pickerController: UIImagePickerController = UIImagePickerController()
    var photoChanged = false
}

extension ParentProfileViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.mediaTypes = ["public.image"]
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.subscribe(onNext: { [self] _ in
            
            ModalLogo.showModal(with: "Updating...", in: { () -> Observable<Result<JSON?, Error>> in
                if self.photoChanged == true {
                    return Api.instance().rxUploadFile(content: self.photo.value!.pngData()?.base64EncodedString().data(using: .utf8), type: "text/plain")
                        .flatMap({ result -> Observable<Result<JSON?, Error>> in
                            switch result {
                            case .success(let data):
                                if let content = data {
                                    
                                    var documentInstance = JSON(parseJSON: "{}")
                                    documentInstance["$class"].string = "Document"
                                    documentInstance["name"].string = "ProfilePhoto"
                                    documentInstance["type"].string = "Resource"
                                    documentInstance["status"].string = "Completed"
                                    documentInstance["content"].object = content.object
                                    
                                    return self.updateProfile(withPhoto: documentInstance)
                                }
                            case .failure(let error as NSError):
                                print(error.localizedDescription)
                            }
                            return Observable.just(.failure(NSError(domain: "", code: 0, userInfo: ["message": "An error has occurred when updating your profile, please try again"])))
                        })
                } else {
                    return self.updateProfile(withPhoto: nil)
                }
            }()).observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] result in
                switch result {
                case .success(let data):
                    if let parentUpdated = data {
                        DataHub.instance().publishData(for: "parent", with: parentUpdated)
                        Notification.show(title: "Profile Updated", message: "Your profile was updated successfully", type: .Success)
                        self.navigationController?.popViewController(animated: true)
                    }
                case .failure(let error):
                    Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
                }
            }).disposed(by: self.disposeBag)
            
        }).disposed(by: self.disposeBag)
        
        parentSubject.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] parent in
            guard let parent = parent else { return }
            self.firstNameTextField.setValue(parent["firstName"].stringValue)
            self.lastNameTextField.setValue(parent["lastName"].stringValue)
            self.emailTextField.setValue(parent["emailAddresses"].arrayValue[0]["value"].stringValue)
            self.dobTextField.setValue(Date.from(string: parent["dateOfBirth"].stringValue, format: .Persistent)?.to(format: .DisplayLong))
            self.loadProfilePhoto(for: parent)
        }).disposed(by: disposeBag)
        
        photo.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] photo in
            guard let photo = photo else { return }
            self.avatarImageView.image = photo
            self.avatarImageView.layoutIfNeeded()
        }).disposed(by: disposeBag)
        
        let isParentValid: Observable<Bool> = parentSubject.map{ parent -> Bool in parent != nil }.share(replay: 1)
        let allValid: Observable<Bool> = Observable.combineLatest(firstNameTextField.valid, lastNameTextField.valid, emailTextField.valid, isParentValid) { $0 && $1 && $2 && $3}
        
        // bind done isEnabled
        allValid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    private func loadProfilePhoto(for parent: JSON) {
        if parent["photo"].exists() {
            self.avatarImageView.backgroundColor = .clear
            Api.instance().downloadPhoto(for: parent, withField: "photo", completionHandler: { [self] data, error in
                if let error = error as NSError? {
                    print(error.localizedDescription)
                } else {
                    if let base64Data = data {
                        let decodedData = Data(base64Encoded: base64Data)
                        self.photo.accept(UIImage(data: decodedData!))
                    }
                }
            })
        } else {
            self.photo.accept(UIImage(named: "Avatar"))
            self.avatarImageView.backgroundColor = ThemeSecondaryColor
        }
    }
    
    private func updateProfile(withPhoto photo: JSON?) -> Observable<Result<JSON?, Error>> {
        let parent = self.parentSubject.value!
        do {
            let emailAddress = JSON(dictionaryLiteral:
                ("id", parent["emailAddresses"].arrayValue[0]["id"].stringValue),
                ("value", self.emailTextField.value.value!)
            )
            var json = JSON(dictionaryLiteral:
                            ("firstName", self.firstNameTextField.value.value!),
                            ("lastName", self.lastNameTextField.value.value!),
                            ("dateOfBirth", Date.from(string: self.dobTextField.value.value!, format: .DisplayLong)!.to(format: .Persistent)!),
                            ("emailAddress",emailAddress.object))
            if photo != nil {
                json["photo"].object = photo!.object
            }
            let data = try json.rawData()
            return Api.instance().rxInvoke(using: "Put", to: "userAccount/self/profile", payload: data,with: RequestOptions(fetch: UserFetchOptions))
        } catch {
            return Observable.just(.failure(NSError(domain: "", code: 0, userInfo: ["message": "An error has occurred when updating your profile, please try again"])))
        }
    }
    
    @IBAction func handlePhotoTap(_ sender: Any?) {
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Set Profile Photo", preferredStyle: .actionSheet)
        
        // 2
        let pickAction = UIAlertAction(title: "Photo Library", style: .default, handler: { [self] _ in
            self.pickerController.sourceType = .photoLibrary
            self.present(self.pickerController, animated: true)
        })
        let cameraAction = UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.pickerController.sourceType = .camera
            self.present(self.pickerController, animated: true)
        })
        
        // 3
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        // 4
        optionMenu.addAction(pickAction)
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DateOfBirthSegue" {
            if let controller = segue.destination as? SelectDateViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }
        let resizedImage = image.resized(to: CGSize(width: 100, height: 100))
        self.photo.accept(resizedImage)
        self.avatarImageView.backgroundColor = .clear
        self.pickerController.dismiss(animated: true)
        self.photoChanged = true
    }
}

extension ParentProfileViewController: UINavigationControllerDelegate {
    
}

extension ParentProfileViewController: SelectDateViewControllerDelegate {
    func selectDateViewControllerWillShow(_ controller: SelectDateViewController) {}
    
    func selectDateViewControllerDidCancel(_ controller: SelectDateViewController) {}
    
    func selectDateViewController(_ controller: SelectDateViewController, didFinishEditing date: DateModel) {
        self.dobTextField.setValue(date.value.to(format: .DisplayLong))
    }
    
    func selectDateViewControllerModelForEdition(_ controller: SelectDateViewController) -> DateModel {
        return DateModel(.date, Date.from(string: self.dobTextField.value.value!, format: .DisplayLong)!, "Select Date of Birth")
    }
}
