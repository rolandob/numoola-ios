//
//  VerifyPhoneNumberViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 8/6/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AWSMobileClient
import SwiftyJSON

protocol VerifyPhoneNumberViewControllerDelegate:class {
    func verifyPhoneNumberViewControllerDidCancel(_ controller: VerifyPhoneNumberViewController)
    func verifyPhoneNumberViewControllerDidVerify(_ controller: VerifyPhoneNumberViewController)
    func verifyPhoneNumberViewControllerShouldSendCode(_ controller: VerifyPhoneNumberViewController) -> Bool
    func verifyPhoneNumberViewControllerWillShow(_ controller: VerifyPhoneNumberViewController)
}

class VerifyPhoneNumberViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var codeTextField: NMLTextField!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var codeButton: NMLButton!
    // delegate
    weak var delegate: VerifyPhoneNumberViewControllerDelegate!
    // fields
    var disposeBag = DisposeBag()
    let parentSubject: BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
}

extension VerifyPhoneNumberViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        parentSubject.subscribe(onNext: { [self] parent in
            guard let parent = parent else { return }
            let phoneNumber = parent["phone"]["number"].stringValue as NSString?
            let range = NSMakeRange(0, phoneNumber!.length - 4)
            let maskedPhone = phoneNumber!.replacingCharacters(in: range, with: String(repeating: "*", count: phoneNumber!.length - 4))
            self.titleLabel.text = "Your current phone number is \(maskedPhone), a verification code has been sent, please enter it below to verify it."
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.verifyPhoneNumberViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] _ in
            guard let code = self.codeTextField.value.value else { return }
            AWSMobileClient.sharedInstance().confirmVerifyUserAttribute(attributeName: "phone_number", code: code, completionHandler: { error in
                if let error = error {
                    DispatchQueue.main.async {
                        Notification.showError(using: error)
                    }
                } else {
                    DispatchQueue.main.async {
                        Notification.show(title: "Phone Number Verified", message: "Your phone number was successfully verified", type: .Success)
                        self.navigationController?.popViewController(animated: true)
                        self.delegate?.verifyPhoneNumberViewControllerDidVerify(self)
                    }
                }
            })
        }).disposed(by: disposeBag)
        
        codeButton.rx.tap.subscribe(onNext: { [self] _ in
            self.verifyPhone()
        }).disposed(by: disposeBag)
        
        codeTextField.valid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
        
        if delegate?.verifyPhoneNumberViewControllerShouldSendCode(self) == true {
            self.verifyPhone()
        }
    }
    
    private func verifyPhone() {
        AWSMobileClient.sharedInstance().verifyUserAttribute(attributeName: "phone_number", completionHandler: { (details, error) in
            if let error = error {
                Notification.showError(using: error)
            }
        })
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.delegate?.verifyPhoneNumberViewControllerWillShow(self)
    }
}
