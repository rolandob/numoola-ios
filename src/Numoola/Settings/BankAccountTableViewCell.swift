//
//  BankAccountTableViewCell.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/5/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit

class BankAccountTableViewCell: UITableViewCell {
    // fields
    var bankView: BankView!
}

extension BankAccountTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tintColor = ThemePrimaryColor
        // create avatar view
        self.bankView = UINib(nibName: "BankView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? BankView
        self.bankView.frame.size.width = self.contentView.frame.size.width
        self.bankView.frame.size.height = self.contentView.frame.size.height
        self.contentView.addSubview(self.bankView)
    }
}
