//
//  ParentMoolaViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/5/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ParentMoolaViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var backButton: NMLBarButtonItem!
    // fields
    var disposeBag = DisposeBag()
    let items:BehaviorRelay<[SettingItem]> = BehaviorRelay(value: [
        SettingItem("Bank Accounts", "Link & Verify bank accounts", "BankAccounts"),
        SettingItem("Bank Statements", "Bank statements of your NuMoola account", "BankStatements")
        ])
}

extension ParentMoolaViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // bind items to table view
        items.bind(to: tableView.rx.items(cellIdentifier: "SettingCell")) { (row, item, cell) in
            if let cell = cell as? SettingTableViewCell {
                cell.item.accept(item)
            }
            }.disposed(by: disposeBag)
        // on name selected fill up the text field
        Observable.zip(tableView.rx.itemSelected, tableView.rx.modelSelected(SettingItem.self)).bind { [unowned self] indexPath, item in
            if let cell = self.tableView.cellForRow(at: indexPath) as? SettingTableViewCell {
                self.tableView.deselectRow(at: indexPath, animated: true)
                self.openSetting(with: cell.item.value!.key)
            }
            }.disposed(by: disposeBag)
        
        backButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    private func openSetting(with key: String) {
        print("Opening setting with key \(key)")
        switch key {
        case "BankAccounts":
            self.performSegue(withIdentifier: "BankAccountsSegue", sender: nil)
//        case "BankStatements":
//            self.performSegue(withIdentifier: "BankStatementsSegue", sender: nil)
        default:
            print("Unimplemented option \(key)")
        }
    }
}

extension ParentMoolaViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.prepareDisclosureIndicator()
        cell.tintColor = ThemeSecondaryColor
    }
}
