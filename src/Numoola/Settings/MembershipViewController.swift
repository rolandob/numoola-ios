//
//  MembershipViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/3/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class MembershipViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var backButton: NMLBarButtonItem!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var actionButton: NMLButton!
    // fields
    var disposeBag = DisposeBag()
    let membershipSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "membership")
    let membershipConfigSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "membership-config")
}

extension MembershipViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        backButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        membershipSubject.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] membership in
            guard let membership = membership, let config = self.membershipConfigSubject.value else { return }
            self.messageLabel.text = self.getMessage(for: membership, with: config)
            self.actionButton.setTitle(self.getButtonLabel(for: membership), for: .normal)
        }).disposed(by: disposeBag)
        
        actionButton.rx.tap.subscribe(onNext: { [self] _ in
            guard let membership = self.membershipSubject.value else { return }
            if membership["id"].exists() {
                let status = membership["status"]["value"].stringValue
                switch status {
                case "Active":
                    self.processMembershipAction(Api.instance().rxInvoke(using: "Put", to: "userAccount/self/membership/pause", payload: "{}".data(using: .utf8)))
                case "Paused":
                    self.processMembershipAction(Api.instance().rxInvoke(using: "Put", to: "userAccount/self/membership/resume", payload: "{}".data(using: .utf8)))
                case "Expired":
                    self.processMembershipAction(Api.instance().rxInvoke(using: "Put", to: "userAccount/self/membership/renew", payload: "{}".data(using: .utf8)))
                default:
                    print("Unimplemented status \(status)")
                }
            } else {
                self.processMembershipAction(Api.instance().rxInvoke(using: "Post", to: "userAccount/self/membership/start", payload: "{}".data(using: .utf8)))
            }
        }).disposed(by: disposeBag)
    }
    
    private func processMembershipAction(_ observable: Observable<Result<JSON?, Error>>) {
        observable.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] result in
            switch result {
            case .success(let data):
                if let updatedMembership = data {
                    self.membershipSubject.accept(updatedMembership)
                    Notification.show(title: "Membership updated", message: "Membership updated successfully", type: .Success)
                }
            case .failure(let error):
                Notification.show(title: "Something went wrong", message: Errors.getErrorMessage(for: error), type: .Error)
            }
        }).disposed(by: disposeBag)
    }
    
    private func getMessage(for membership: JSON, with config: JSON) -> String {
        if membership["id"].exists() {
            let status = membership["status"]["value"].stringValue
            
            switch status {
            case "Active":
                let renewDate = Date.from(string: membership["renewDate"].stringValue, format: .DateTime)!.to(format: .DisplayLong)!
                let amount = config["membershipMonthlyAmount"].doubleValue
                let daysLeft = getDaysLeft(for: membership)
                return "Your membership is active and will renew in \(daysLeft) day(s) on \(renewDate). On your renewal date, we will deduct $\(amount) from your NuMoola account. If you do not want to be charged, please pause your membership before \(renewDate)"
            case "Expired":
                let renewDate = Date.from(string: membership["renewDate"].stringValue, format: .DateTime)!.to(format: .DisplayLong)!
                return "Your membership expired on \(renewDate). To access all the NuMoola features you need an active membership."
            case "Paused":
                let amount = config["membershipMonthlyAmount"].doubleValue
                return "Your membership is currently paused. To unlock the full potential of NuMoola, please activate your membership now. You will be charged $\(amount) per month on your renewal date after your free trial period expires. To access all the NuMoola features you need an active membership."
            default:
                return "To access all the NuMoola features you need an active membership."
            }
        } else {
            let amount = config["membershipMonthlyAmount"].doubleValue
            let freeTrialDays = config["membershipTrialDuration"].stringValue
            return "To unlock the full potential of NuMoola we charge a small membership fee of $\(amount) per month. This allows us to provide a full range of tools to empower your family to learn about money management and create a positive experience with finances. Start your free \(freeTrialDays) day trial period today. Join us now to create a brighter financial future for your family for pennies per day."
        }
    }
    
    private func getButtonLabel(for membership: JSON) -> String {
        if membership["id"].exists() {
            let status = membership["status"]["value"].stringValue
            
            switch status {
            case "Active":
                return "Pause Membership"
            case "Expired":
                return "Renew Membership"
            case "Paused":
                return "Resume Membership"
            default:
            return "Start free trial"
            }
        } else {
            return "Start free trial"
        }
    }
    
    private func getDaysLeft(for membership: JSON) -> String {
        if membership["id"].exists() {
            let renewDate = Date.from(string: membership["renewDate"].stringValue, format: .DateTime)!
            let todayDate = Date()
            let components = Calendar.current.dateComponents([.day], from: todayDate, to: renewDate)
            return "\(components.day ?? 0)"
        }
        return ""
    }
    
    
}
