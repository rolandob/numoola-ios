//
//  ParentSecurityViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 7/2/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import LocalAuthentication
import AWSMobileClient
import SwiftyJSON

class ParentSecurityViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var backButton: NMLBarButtonItem!
    // fields
    var disposeBag = DisposeBag()
    let items:BehaviorRelay<[SettingItem]> = BehaviorRelay(value: [])
    let parentPhoneVerifiedSubject:BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "userPhoneVerified")
}

extension ParentSecurityViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createOptions()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        items.bind(to: tableView.rx.items) { (tv, row, item) in
            if item.key == "FaceID" {
                let cell = tv.dequeueReusableCell(withIdentifier: "FaceIDCell", for: IndexPath.init(row: row, section: 0)) as? FaceIDTableViewCell
                 cell!.item.accept(item)
                return cell!
            } else if item.key == "TouchID" {
                let cell = tv.dequeueReusableCell(withIdentifier: "TouchIDCell", for: IndexPath.init(row: row, section: 0)) as? TouchIDTableViewCell
                cell!.item.accept(item)
                return cell!
            } else {
                let cell = tv.dequeueReusableCell(withIdentifier: "SettingCell", for: IndexPath.init(row: row, section: 0)) as? SettingTableViewCell
                cell!.item.accept(item)
                return cell!
            }
            }.disposed(by: disposeBag)
        // on name selected fill up the text field
        Observable.zip(tableView.rx.itemSelected, tableView.rx.modelSelected(SettingItem.self)).bind { [unowned self] indexPath, item in
            if let cell = self.tableView.cellForRow(at: indexPath) as? SettingTableViewCell {
                self.tableView.deselectRow(at: indexPath, animated: true)
                self.openSetting(with: cell.item.value!.key)
            }
            }.disposed(by: disposeBag)
        
        backButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    private func createOptions() {
        let context = LAContext()
        var settingItems:[SettingItem] = [SettingItem("Change Password", "You can change your account password", "ChangePassword"), SettingItem("Change Phone Number", "You can change your phone number", "ChangePhone")]
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            switch context.biometryType {
            case .faceID:
                settingItems.insert(SettingItem("Face ID", "Enable Face ID Login for your account", "FaceID"), at: 0)
            case .touchID:
                settingItems.insert(SettingItem("Touch ID", "Enable Touch ID Login for your account", "TouchID"), at: 0)
            case .none:
                print("none")
            @unknown default:
                print("default")
            }
        }
        
        items.accept(settingItems)
        
        parentPhoneVerifiedSubject.subscribe(onNext: { data in
            guard let data = data else { return }
            if data["verified"].stringValue == "false" {
                settingItems.append(SettingItem("Verify Phone Number", "You can verify your phone number", "VerifyPhone"))
                self.items.accept(settingItems)
            }
        }).disposed(by: disposeBag)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        createOptions()
    }
    
    private func openSetting(with key: String) {
        print("Opening setting with key \(key)")
        switch key {
        case "ChangePassword":
            self.performSegue(withIdentifier: "ChangePasswordSegue", sender: nil)
        case "ChangePhone":
            self.performSegue(withIdentifier: "ChangePhoneSegue", sender: nil)
        case "VerifyPhone":
            self.performSegue(withIdentifier: "VerifyPhoneSegue", sender: nil)
        case "FaceID":
            self.performSegue(withIdentifier: "FaceIDSegue", sender: nil)
        default:
            print("Unimplemented option \(key)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ChangePhoneSegue" {
            if let controller = segue.destination as? ChangePhoneNumberViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "VerifyPhoneSegue" {
            if let controller = segue.destination as? VerifyPhoneNumberViewController {
                controller.delegate = self
            }
        }
    }
}

extension ParentSecurityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.prepareDisclosureIndicator()
        cell.tintColor = ThemeSecondaryColor
    }
}

extension ParentSecurityViewController: ChangePhoneNumberViewControllerDelegate {
    func changePhoneNumberViewControllerDidCancel(_ controller: ChangePhoneNumberViewController) {}
    
    func changePhoneNumberViewController(_ controller: ChangePhoneNumberViewController, didChangePhoneNumberWith phoneNumber: String) {
        DataHub.instance().publishData(for: "userPhoneVerified", with: JSON(dictionaryLiteral: ("verified", "false")))
        self.openSetting(with: "VerifyPhone")
    }
    
    func changePhoneNumberViewControllerWillShow(_ controller: ChangePhoneNumberViewController) {}
}

extension ParentSecurityViewController: VerifyPhoneNumberViewControllerDelegate {
    func verifyPhoneNumberViewControllerShouldSendCode(_ controller: VerifyPhoneNumberViewController) -> Bool {
        return false
    }
    
    func verifyPhoneNumberViewControllerDidCancel(_ controller: VerifyPhoneNumberViewController) {}
    
    func verifyPhoneNumberViewControllerDidVerify(_ controller: VerifyPhoneNumberViewController) {
        DataHub.instance().publishData(for: "userPhoneVerified", with: JSON(dictionaryLiteral: ("verified", "true")))
    }
    
    func verifyPhoneNumberViewControllerWillShow(_ controller: VerifyPhoneNumberViewController) {}
}
