//
//  ChangePhoneNumberViewController.swift
//  Numoola
//
//  Created by Rolando Bermudez on 8/2/19.
//  Copyright © 2019 Numoola LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AWSMobileClient
import SwiftyJSON

protocol ChangePhoneNumberViewControllerDelegate:class {
    func changePhoneNumberViewControllerDidCancel(_ controller: ChangePhoneNumberViewController)
    func changePhoneNumberViewController(_ controller: ChangePhoneNumberViewController, didChangePhoneNumberWith phoneNumber: String)
    func changePhoneNumberViewControllerWillShow(_ controller: ChangePhoneNumberViewController)
}

class ChangePhoneNumberViewController: UIViewController {
    // controls
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var cancelButton: NMLBarButtonItem!
    @IBOutlet var doneButton: NMLBarButtonItem!
    @IBOutlet var phoneNumberTextField: NMLTextField!
    @IBOutlet var currentPhoneLabel: UILabel!
    // delegate
    weak var delegate: ChangePhoneNumberViewControllerDelegate?
    // fields
    var disposeBag = DisposeBag()
    let parentSubject: BehaviorRelay<JSON?> = DataHub.instance().getBehaviorRelay(for: "parent")
}

extension ChangePhoneNumberViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        UITheme.instance().current.subscribe(onNext: { [self] theme in
            guard let theme = theme else { return }
            self.backgroundImageView.image = UIImage(named: theme.getString(with: ViewStyles.BackgroundImageName, for: .normal)!)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.getColor(with: ViewStyles.NavigationBarForegroundColor, for: .normal)!]
            self.mainView.layer.cornerRadius = CGFloat(theme.getSize(with: ViewStyles.MainViewCornerRadius, for: .normal)!)
        }).disposed(by: disposeBag)
        
        parentSubject.subscribe(onNext: { [self] parent in
            guard let parent = parent else { return }
            let phoneNumber = parent["phone"]["number"].stringValue as NSString?
            let range = NSMakeRange(0, phoneNumber!.length - 4)
            let maskedPhone = phoneNumber!.replacingCharacters(in: range, with: String(repeating: "*", count: phoneNumber!.length - 4))
            self.currentPhoneLabel.text = "Your current phone number is \(maskedPhone)"
        }).disposed(by: disposeBag)
        
        cancelButton.rx.tap.subscribe(onNext: { [self] _ in
            self.navigationController?.popViewController(animated: true)
            self.delegate?.changePhoneNumberViewControllerDidCancel(self)
        }).disposed(by: disposeBag)
        
        doneButton.rx.tap.observeOn(ConcurrentMainScheduler.instance).subscribe(onNext: { [self] _ in
            guard let phoneNumber = self.phoneNumberTextField.value.value else { return }
            do {
                let data = try JSON(dictionaryLiteral: ("phoneNumber", phoneNumber)).rawData()
                Api.instance().rxInvoke(using: "Put", to: "userAccount/self/phone", payload: data, with: RequestOptions(fetch: UserFetchOptions))
                    .observeOn(ConcurrentMainScheduler.instance)
                    .subscribe(onNext: { [self] result in
                        switch result {
                        case .success(let data):
                            if let json = data {
                                if json["id"].exists() {
                                    DataHub.instance().publishData(for: "user", with: json)
                                    DataHub.instance().publishData(for: "parent", with: json)
                                    Notification.show(title: "Phone Number Changed", message: "Your phone number was successfully changed, now you will need to confirm it", type: .Success)
                                    DispatchQueue.main.async {
                                        self.navigationController?.popViewController(animated: false)
                                        self.delegate?.changePhoneNumberViewController(self, didChangePhoneNumberWith: phoneNumber)
                                    }
                                } else {
                                    Notification.show(title: "Something went wrong", message: "The phone number entered is the same as the current one, please enter a different phone number", type: .Warning)
                                }
                            } else {
                                Notification.show(title: "Something went wrong", message: "The phone number entered is the same as the current one, please enter a different phone number", type: .Warning)
                            }
                        case .failure(let error):
                            Notification.showError(using: error)
                        }
                    }).disposed(by: self.disposeBag)
            } catch (let error) {
                Notification.showError(using: error)
            }
        }).disposed(by: disposeBag)
        
        phoneNumberTextField.valid.bind(to: doneButton.rx.isEnabled).disposed(by: disposeBag)
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.delegate?.changePhoneNumberViewControllerWillShow(self)
    }
}
