
const execSync = require('child_process').execSync;

var udid = null;
var args = process.argv.slice(2);
var deviceName = args[0];
if (deviceName != null) {
    var content = execSync('xcrun simctl list -j').toString('utf-8');
    var data = JSON.parse(content)
    for (var key in data["devices"]["com.apple.CoreSimulator.SimRuntime.iOS-12-4"]) {
        let device = data["devices"]["com.apple.CoreSimulator.SimRuntime.iOS-12-4"][key];
        if (device.name === deviceName) {
            udid = device.udid;
        }
    }
}

if (udid != null) {
    console.log(udid);
} else {
    console.log("NA");
}
